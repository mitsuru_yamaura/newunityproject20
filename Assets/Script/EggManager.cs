﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggManager : MonoBehaviour {

    int breakPoint;                        //  破壊したオブジェクトのトータルXP加算用（ステージ用）
    int count;                             //  破壊したオブジェクトのトータル数
    int collectPoint;                      //  獲得したアイテムの総数
    int lastLife;                          //  クリア時のライフの残り値
    public int getCount;                   //  オーブを叩ける回数

    bool isChecked;                        //  複数取れてしまう場合の回避フラグ

    [SerializeField]
    Life life;                             //  ライフの現在値取得用

    [SerializeField]
    ClearBonus cBonus;                     //  決定したエッグガチャ回数を渡す先のscript

    [SerializeField]
    MeshRenderer circleRend;               //  チェックが入るとcircleを表示

    [SerializeField]
    ParticleSystem circleEffect;           //  チェックが入るとparticleを再生

    [SerializeField]
    StageChangeBox stageChangeBox;         //  現在のステージ数を取得

    [SerializeField]
    GameObject stageTextBack;              //  背景表示用のオブジェクトをアサイン。iTween用

    [SerializeField]
    SpriteRenderer sRen;                   //  背景画表示用のコンポーネントをアサイン

    [SerializeField]
    TextMesh tMesh;                        //  Text

    Vector3 scalePos;　　　　　　　        //  元の大きさの保存用

    public int nowClearCount;

    private void Start()
    {
        //  現在のステージに合わせてクリアカウントを取得する
        //  GateSwitch.csでクリア状態の管理に利用する
        switch (stageChangeBox.nowSceneName)
        {
            case "GameScene1":
                                
                //  現在の数値を代入する
                nowClearCount = SaveData.firstStageClearCount;
                Debug.Log(nowClearCount);
                break;

            case "GameScene3":
                
                nowClearCount = SaveData.secondStageClearCount;
                Debug.Log(nowClearCount);
                break;

            default:
                break;
        }       
    }

    //  当たり判定でメソッド呼び出し
    private void OnTriggerEnter(Collider col)
    {
        //  チェックしていないなら
        if (!isChecked)
        {
            if (col.gameObject.tag == "Player")
            {
                //  拡大・縮小表示する描画用オブジェクトのScale値を取得する
                scalePos = stageTextBack.transform.localScale; 

                //  チェック済のフラグを立てる
                isChecked = true;

                //  ステージ内の各獲得数を取得するメソッド
                BonusCheck();
            }
        }
    }

    //  BreakObjectとXpGetから呼ばれるメソッド
    public void Breaker(int addBreakXp)
    {
        //  BreakPointを加算。Xpを加算するようにする＝リザルトで今回分のトータルXPを表示できる
        breakPoint += addBreakXp;
        Debug.Log(breakPoint);
    }

    //  BreakObject.csから呼ばれるメソッド
    public void Counter(int addCountObject)
    {
        count += addCountObject;
        Debug.Log(count);
    }

    //  ItemRotate.csとXpGet.csから呼ばれるメソッド
    public void Collector(int addCollect)
    {
        //  ハートと巻物を獲得した数をカウントする
        collectPoint += addCollect;
        Debug.Log(collectPoint);
    }

    //  最終的に何回オーブを叩けるのかを決めるメソッド
    //  チェック用オブジェクトを叩いたときに呼ばれる
    private void BonusCheck()
    {
        //  BreakPointを獲得しているなら
        if (breakPoint > 0)
        {
            if (0 < breakPoint && breakPoint <= 500)
            {
                getCount += 0;
            }
            else if (500 < breakPoint && breakPoint <= 1500)
            {
                getCount += 1;
            }
            else
            {
                getCount += 2;
            }
            Debug.Log(breakPoint);
            Debug.Log(getCount);
        }

        //  Itemを獲得しているなら
        if (collectPoint > 0)
        {
            if (0 < collectPoint && collectPoint <= 5)
            {
                getCount += 0;
            }
            else if (5 < collectPoint && collectPoint <= 10)
            {
                getCount += 1;
            }
            else
            {
                getCount += 2;
            }
            Debug.Log(collectPoint);
            Debug.Log(getCount);
        }

        //  ハートの残り数を取得する
        lastLife = life.currentLife;
        Debug.Log(lastLife);

        if (lastLife > 0)
        {
            if (0 < lastLife && lastLife <= 3)
            {
                getCount += 0;
            }
            else if (3 < lastLife && lastLife <= 6)
            {
                getCount += 1;
            }
            else if (6 < lastLife && lastLife <= 9)
            {
                getCount += 2;
            }
            else
            {
                getCount += 3;
            }
            Debug.Log(getCount);
        }
        //  すべてチェックしたら、Eggガチャ用のメソッドに引数を渡す
        cBonus.GetBonusPoint(getCount);

        //  ステージクリア回数を現在のステージ分を見つけて加算
        switch (stageChangeBox.nowSceneName)
        {
            case "GameScene1":
                SaveData.firstStageClearCount++;
                string STAGE1_CLEAR_KER = "stage1ClearKey";  //  クリア情報の保存用
                SaveData.Save(STAGE1_CLEAR_KER, SaveData.firstStageClearCount);
                break;

            case "GameScene3":
                SaveData.secondStageClearCount++;
                string STAGE2_CLEAR_KER = "stage2ClearKey";  //  クリア情報の保存用
                SaveData.Save(STAGE2_CLEAR_KER, SaveData.secondStageClearCount);
                break;

            default:
                break;
        }
        //  circleを表示し、particleを再生する
        circleRend.enabled = true;
        circleEffect.Stop();

        //  textと画像を表示する
        sRen.enabled = true;

        //  拡大・縮小表示する描画用オブジェクトのScale値を最小値にする
        stageTextBack.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

        //  元の大きさまでアニメで表示
        iTween.ScaleTo(stageTextBack, iTween.Hash("x", scalePos.x, "y", scalePos.y, "z", scalePos.z, "time", 1.0f));

        tMesh.text = "今回の成績\n-Result-\n\n\n◇ 獲得した合計XP : " + breakPoint.ToString() + " pt\n\n◇ 破壊したオブジェクトの数 : "+ count.ToString() + " 個\n\n◇ 獲得したハートと巻物の合計数 : " + collectPoint.ToString() + " 個\n\n\n(右端の柱を叩くとステージ選択画面に戻ります。)";
    }
}

