﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeIn : MonoBehaviour {

    [SerializeField]
    private float fadeInTime;            //  フェードインのおおよその秒数

    private Image image;                 //  フェイドインで消す背景イメージ
    
	// Use this for initialization
	void Start () {
        image = GameObject.Find("Panel").GetComponent<Image>();
       
        //  fadeInTimeの値を書き換え、コルーチンで使用する待ち時間を計測
        fadeInTime = 1f * fadeInTime / 10f;

        StartCoroutine("FadeInTime");
	}
	
	IEnumerator FadeInTime()
    {
        //  Colorのアルファ(透明度)を0.1ずつ下げていく
        for(var i = 1f; i >= 0; i -= 0.1f)
        {
            image.color = new Color(0f, 0f, 0f, i);

            //  指定秒数待つ
            yield return new WaitForSeconds(fadeInTime);
        }

        //  スタートボタンを有効にするようにこの背景を消す
        this.gameObject.SetActive(false);
    }
}
