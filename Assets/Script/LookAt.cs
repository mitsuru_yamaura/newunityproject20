﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour {

    private GameObject target;

	// Use this for initialization
	void Start ()
    {
        //  プレイヤーに狙いを定める
        target = GameObject.Find("Kohaku");	

	}
	
	// Update is called once per frame
	void Update ()
    {
        this.gameObject.transform.LookAt(target.transform.position);	
	}
}
