﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HummerAttack : MonoBehaviour {

    //GameObject kohaku;
    //Rigidbody rb;

    public float hummerSpeed;
    public float hummerPower;

	// Use this for initialization
	void Start () {

        //kohaku = GameObject.Find("Kohaku");
        //rb = GetComponent<Rigidbody>();
        //rb.velocity = new Vector3(hummerSpeed * kohaku.transform.localScale.x, rb.velocity.y, 0);

        Destroy(this.gameObject, 0.8f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Chest")
        {
            Destroy(gameObject);
        }
    }
}
