﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Chest : MonoBehaviour {

　　public bool isOpen;           //  宝箱を開けたか管理するフラグ。これがFlagManagerにも記録される
    public int nowChestNum;       //  宝箱の番号を元となるオブジェクトから紐付けする宝箱の番号
    int nowStageNum = 1;          //  ステージ番号。ステージが増えたらpublicできるようにしておく
    int runePoint = 1;            //  獲得できるルーンの数。これもボスなどで複数獲得できる場合用に変数にしておく

    //  private
    //TextMesh getText;           //  得点表示用
    //SpriteRenderer sr;          //  ハート表示用

    //AudioSource sound1;         //  錠の開くSE（宝箱のAudioSource） ChestTopで鳴らすように変更
    AudioSource sound2;           //  ハンマーで叩くSE（ハンマーのオブジェクトから取得）

    //GameObject itemMgr;           //  ItemManagerスクリプト格納用
    //ItemManager itemMgrScript;    //　シングルトン化したので取得は必要ない

    ChestStatus dict_chest;        //  Dictionary作成用のクラス

    ZoomOut zoomOutScript;                //  各スクリプトの格納用
    ChestTopRotate chestRotateScript;
    RuneCounter runeCounterScript;


    // Use this for initialization
    void Start () {

        //  List(Dictionary)にこの宝箱の情報があるか確認する
        //  ないなら作るメソッドを実行する
        //  ItemManager.csがシングルトンなので、ScriptとInstanceで指定しないと探せない
        //  まずはDictionaryからdict_chestにこの宝箱の情報をnowChestNumをIDに使って取得する
        if (!ItemManager.Instance.rune_dict.ContainsKey("chest_" + nowChestNum.ToString().PadLeft(2, '0')))
        {
            ItemManager.Instance.ChestCreate(nowChestNum, nowStageNum);
            Debug.Log(nowChestNum);
        }

        dict_chest = ItemManager.Instance.rune_dict["chest_" + nowChestNum.ToString().PadLeft(2, '0')];
        Debug.Log("chest_" + nowChestNum.ToString().PadLeft(2, '0'));
        Debug.Log(dict_chest.m_isOpen);

        //  開封していないなら
        if (dict_chest.m_isOpen == false)
        {
            //  宝箱を配置・生成可能にする
            this.gameObject.SetActive(true);
        }
        else
        {
            //  宝箱を配置せず、生成不可にする
            this.gameObject.SetActive(false);            
        }     
    }
	
	// Update is called once per frame
	void Update () { 
		
	}

    //  インスタンス元になるオブジェクトから呼ばれるゲッターメソッド
    public void GetChestNum(int chestNum)
    {
        //  元となるオブジェクトの持っていた宝箱の番号を受け取る
        nowChestNum = chestNum;
    }

    public void OnTriggerEnter(Collider col)
    {
        //  ズームイン・ズームアウト用のscriptを格納
        zoomOutScript = GameObject.Find("MainCamera").GetComponent<ZoomOut>();

        //  宝箱の上フタを開ける処理用のscriptを格納
        chestRotateScript = transform.GetChild(0).GetComponent<ChestTopRotate>();

        //  獲得したルーンをカウントするscriptを格納
        runeCounterScript = GameObject.Find("NowRuneCountText").GetComponent<RuneCounter>();

        //  宝箱の錠が開くSEを格納
        //AudioSource[] audioSourcesChest = GetComponents<AudioSource>();
        //sound1 = audioSourcesChest[0];  

        //  HeartScoreオブジェクトからLifeスクリプトを格納
        //lifeScript = GameObject.Find("HeartScore").GetComponent<Life>();
        //itemScript = transform.GetChild(0).gameObject.GetComponent<ItemRotate>();

        //  Textと背景のImageを取得
        //getText = transform.GetChild(0).gameObject.GetComponent<TextMesh>();

        //  子のオブジェクトの描画を取得
        //sr = transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>();

        //getText.text = scorePoint.ToString();

        //  通常のハンマー攻撃をしたら
        if (col.gameObject.tag == "Weapon")
        {
            //  別の配列にハンマーで叩かれた時のSEを取得して格納
            AudioSource[] audioSources = GameObject.Find("HammerEffect").GetComponents<AudioSource>();
            sound2 = audioSources[0];

            //  宝箱を開けていないなら
            if (dict_chest.isOpen == false)
            {
                //  宝箱を開けたフラグをオンにする
                dict_chest.m_isOpen = true;
                Debug.Log(dict_chest.m_isOpen);

                //  宝箱の管理フラグをオンにする
                FlagManager.Instance.flags[nowChestNum] = true;

                //  Dictionary管理のスクリプトのメソッドを呼び出す
                ItemManager.Instance.Save(nowChestNum,dict_chest);

                //  ルーンの数を増やす
                runeCounterScript.GetRune(runePoint);

                //Quaternion chestPos = transform.rotation;

                //  アニメ付きでオブジェクトを揺らす
                iTween.ShakePosition(this.gameObject, iTween.Hash("z", 0.5f, "time", 1.0f));

                //  宝箱のフタを開くメソッドを呼び出し
                chestRotateScript.ChestOpen();

                //  宝箱の位置情報をposで取得し
                //  zoomOutScript内のChestZoomInメソッドに引数として渡す
                Vector3 pos = this.transform.position;
                zoomOutScript.ZoomIn(pos, "chest");

                //  ハンマーの叩く音のSE再生
                sound2.Play();

                //lifeScript.RecoveryLife(repair);
                //Debug.Log(repair);

                //  このオブジェクトを破壊する
                Destroy(this.gameObject, 2.5f);
            }
        }
    }
}
