﻿using System;
using UnityEngine;

public class RotateCamera : MonoBehaviour {
    
    [SerializeField,HeaderAttribute("追従するオブジェクト")] GameObject targetObj;
    [SerializeField] Camera cam;
    UnitychanController uniCon;       
    Tutorial tutorial;
    Vector3 targetPos;
    public bool fixCamera;

	void Start () {
        targetPos = targetObj.transform.position;      // 取得した追従オブジェクトのtransform.positionを取得
        uniCon = targetObj.GetComponent<UnitychanController>();
        if (uniCon.isDebugFlg) {
            return;
        } else if (Tutorial.tutorialClear == 0) {             // チュートリアルが終わっていないなら
            tutorial = GameObject.Find("TutorialCanvas").GetComponent<Tutorial>();           
        }
    }
	
	void Update () {
        transform.position += targetObj.transform.position - targetPos;    // targetの移動量分、自分（カメラ）も定位置離れて追従移動する
        targetPos = targetObj.transform.position;
        if (targetObj == null) {　                                         // キャラクターを失った場合、再取得
            targetObj = GameObject.Find("Kohaku");                         //  追従するオブジェクトを取得
        }
        if (!fixCamera) {
            targetPos = targetObj.transform.position;
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");                 //  マウスホイールの操作分だけFieldOfViewを変更する
        float view = cam.fieldOfView - scroll;
        cam.fieldOfView = Mathf.Clamp(value: view, min: 30.0f, max: 50.0f);  //  第１引数で受け取った値が第２引数と第３引数の範囲を超えないように値を返すようにする
      
        if (Input.GetKeyDown("o")) {                                       // default値に戻す操作
            cam.fieldOfView = 40.0f;
            if (uniCon.isDebugFlg) {
                return;
            } else if (Tutorial.tutorialClear == 0) {   // チュートリアルが終わっていないなら
                if (!Tutorial.isMouseZoom)
                {
                    Tutorial.isMouseZoom = true;
                    int value = 7;
                    tutorial.Checker(value);
                }
            }
        }
	}
}
