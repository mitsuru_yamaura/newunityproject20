﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageChoice : MonoBehaviour {

    public static int languageNum;                 //  選択言語。0は日本語、1は英語
    string LANGUAGE_KEY = "languageKey";           //  言語を保存/読みだし
    bool isChoice;

    [SerializeField] Canvas dialogCanvas;          //  終了ダイアログのキャンバス。開いているときは操作無効にする
    [SerializeField] Canvas languageCanvas;        //  言語のキャンバス
    [SerializeField] Loading loading;

    Text selectLanguage;                           //  現在の仕様言語の表示
    Text explainText;                              //  言語の説明文

    // Use this for initialization
    void Start () {
        languageNum = PlayerPrefs.GetInt("languageKey", 0);   //  言語を取得。保存されていなければ日本語
        languageCanvas.enabled = false;                       //  言語のキャンバスを非表示にする
        explainText = transform.GetChild(2).gameObject.GetComponent<Text>();
        selectLanguage = transform.GetChild(3).gameObject.GetComponent<Text>();   
    }

    private void Update(){
        if (languageNum == 0){                       //  現在選択中の言語名を表示
            explainText.text = "現在の選択言語\n\nクリックかCボタンで\n言語選択できます";
            selectLanguage.text = "日本語";
        } else {
            explainText.text = "Currently selected languages.\n\nClick or C Button to\nLanguage Choice.";
            selectLanguage.text = "English";
        }
        if (dialogCanvas.enabled){
            return;
        }
        if (Input.GetButtonDown("Jump")){       //  PS4 = 丸  ゲームをスタートする
            if (!dialogCanvas.enabled){         //  終了ダイアログが開いていないなら
                if (!isChoice){
                    loading.NextScene();
                } else if (isChoice){           //  PS4 = 〇
                    SelectJapanepe();
                }
            }
        }
        if (Input.GetButtonDown("Sliding")){    //  PS4 = △  切り替えダイアログを開く
            if (!dialogCanvas.enabled){         //  終了ダイアログが開いていないなら
                if (!isChoice){
                    OpenCanvas();
                } else {
                    SelectEnglish();            //  終了ダイアログが閉じたときに一緒に呼ばれてしまう
                }
            }
        }
        if (Input.GetButtonDown("Run")){
            if (!dialogCanvas.enabled){         //  終了ダイアログが開いていないなら
                if (isChoice) {                 
                    OpenCanvas();
                }
            }
        }
    }

    //  Cボタンかスタート画面の言語選択ボタンより呼ばれる
    public void OpenCanvas(){        
        languageCanvas.enabled = !languageCanvas.enabled;     //  ダイアログが開いていない場合表示。開いていれば非表示
        isChoice = !isChoice;
    }

    //  日本語変更ボタンから呼ばれる
    public void SelectJapanepe(){
        languageNum = 0;
        SaveData.Save(LANGUAGE_KEY, languageNum);
        languageNum = PlayerPrefs.GetInt("languageKey");      //  言語を取得。
        languageCanvas.enabled = false;                       //  言語のキャンバスを非表示にする
        isChoice = false;
    }

    //  英語変更ボタンから呼ばれる
    public void SelectEnglish(){
        languageNum = 1;
        SaveData.Save(LANGUAGE_KEY, languageNum);
        languageNum = PlayerPrefs.GetInt("languageKey");      //  言語を取得。
        languageCanvas.enabled = false;                       //  言語のキャンバスを非表示にする
        isChoice = false;
    }
}
