﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ChestStatus
{
    public bool m_isOpen;
    public int m_chestId;
    public int m_stageId;

	public bool isOpen
    {
        get { return m_isOpen; }
    }

    public int chestId
    {
        get { return m_chestId; }
    }

    public int stageId
    {
        get { return m_stageId; }
    }

    public ChestStatus(bool isOpen, int chestId, int stageId)
    {
        m_isOpen = isOpen;
        m_chestId = chestId;
        m_stageId = stageId;
    }
}
