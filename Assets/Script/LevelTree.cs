﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//  次のレベルに必要なXPとボーナスを表示するテキストのクラス
public class LevelTree : MonoBehaviour {

    public TextMesh tMesh;            //  text表示用のコンポーネントをアサイン  
    public SpriteRenderer sRen;       //  背景画表示用のコンポーネントをアサイン
    public GameObject stageTextBack;  //  背景表示用のオブジェクトをアサイン。iTween用

    [SerializeField]
    int nowNextLevelXp;                  //  次のレベルアップのために必要なxp(見えているが、LevelUpから取得)

    [SerializeField]
    GameObject circle;                   //  演出用

    Vector3 scalePos;　　　　　　　   //  元の大きさの保存用
    bool isRend;                      //  アニメ描画したかどうかのフラグ

    AudioSource sound1;                 //  演出用

    void Start () {
        //  拡大・縮小表示する描画用オブジェクトのScale値を取得する
        scalePos = stageTextBack.transform.localScale;
    }
	
    //  プレイヤーがswitch内にいる間表示するメソッド
    private void OnTriggerStay(Collider col)
    {
        if (!isRend)
        {
            //  プレイヤーがswitchを踏んだらオン
            if (col.gameObject.tag == "Player" || col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
            {
                sound1 = circle.GetComponent<AudioSource>();

                // 次のレベルに必要なXPを確認する
                int nowNextLevelXp = LevelUp.nextLevelXp;

                //  演出を表示、SE再生
                circle.SetActive(true);
                sound1.Play();

                //  textと画像を表示する
                sRen.enabled = true;

                //  拡大・縮小表示する描画用オブジェクトのScale値を最小値にする
                stageTextBack.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

                //  TextとImageを元の大きさまでアニメで表示
                iTween.ScaleTo(stageTextBack, iTween.Hash("x", scalePos.x, "y", scalePos.y, "z", scalePos.z, "time", 1.0f));

                if (UnitychanController.forceLevel < 10)
                {
                    tMesh.text = "-フォースの樹(Force Tree)-\n現在のForceレベル : " + UnitychanController.forceLevel.ToString() + "\n\nハンマーで叩くとXPを消費して、Forceをレベルアップ!!\nハンマーを振る速度が徐々にアップするとともに\nレベルに応じた様々なボーナスを得ることができます。";
                    tMesh.text += "\n\n次のForceレベル " + (UnitychanController.forceLevel + 1).ToString() + " に必要なXP : " + (nowNextLevelXp * (UnitychanController.forceLevel + 1)).ToString() + " pt";
                }
                else
                {
                    tMesh.text = "-フォースの樹(Force Tree)-\n現在のForceレベル : " + UnitychanController.forceLevel.ToString() + "\n\nForceレベルは最大レベルです!!";
                }

                //  現在のForceLevelに応じて内容を変更して表示する
                switch (UnitychanController.forceLevel)
                {
                    case 0:
                        //  レベル1のボーナス内容を表示
                        tMesh.text += "\nボーナス : 移動速度がアップ。";

                        break;

                    case 1:
                        tMesh.text += "\nボーナス : オブジェクト破壊時に獲得できるXPが15%アップ。";

                        break;

                    case 2:
                        tMesh.text += "\nボーナス : オブジェクト破壊時のハート出現率5%アップ。";

                        break;

                    case 3:
                        tMesh.text += "\nボーナス : オブジェクト破壊時の巻き物出現率5%アップ。";

                        break;

                    case 4:
                        tMesh.text += "\nボーナス : ハンマーの攻撃力がアップ。";

                        break;

                    case 5:
                        tMesh.text += "\nボーナス : 移動速度がさらにアップ。";

                        break;

                    case 6:
                        tMesh.text += "\nボーナス : オブジェクト破壊時に獲得できるXPが30%アップ。";

                        break;

                    case 7:
                        tMesh.text += "\nボーナス : オブジェクト破壊時のハート出現率10%アップ。";

                        break;

                    case 8:
                        tMesh.text += "\nボーナス : オブジェクト破壊時の巻き物出現率10%アップ。";

                        break;

                    case 9:
                        tMesh.text += "\nボーナス : ハンマーの攻撃力がさらにアップ。";

                        break;

                    default:
                        break;
                }

                //  描画フラグを立てる
                isRend = true;
            }
        }

        //  描画中にレベルアップがあった場合、非表示にする
        if (isRend)
        {
            //  レベルアップ時のタグになったら
            if (col.gameObject.tag == "LevelUp")
            {
                //  演出を非表示
                circle.SetActive(false);

                //  テキストを空白に、背景の描画を非表示にする。描画フラグを降ろす
                tMesh.text = " ";

                //  textとimageをアニメで非表示
                iTween.ScaleTo(stageTextBack, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.5f));
            }
        }
    }

    //  プレイヤーがswitchから離れたら非表示にするメソッド
    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
        {
            //  テキストを空白に、背景の描画を非表示にする。描画フラグを降ろす
            tMesh.text = " ";

            circle.SetActive(false);
            sound1.Stop();

            if (isRend)
            {
                //  textとimageをアニメで非表示
                iTween.ScaleTo(stageTextBack, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.5f));

                //  描画フラグを降ろす
                isRend = false;
            }
        }
    }
}
