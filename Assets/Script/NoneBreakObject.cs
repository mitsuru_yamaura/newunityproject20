﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoneBreakObject : MonoBehaviour {

    public int hp;

    ParticleSystem particleHammer;            //  ハンマーでたたいた時のエフェクト用

    AudioSource sound1;                  //  ハンマーでたたいた時のSE再生用


    // Use this for initialization
    void Start () {

        //  ハンマーオブジェクトの子であるHammerEffectのパーティクルを取得
        particleHammer = GameObject.Find("HammerEffect").GetComponent<ParticleSystem>();

        //  叩いたSEをハンマーオブジェクトの子であるHammerEffectから取得
        AudioSource[] audioSources = GameObject.Find("HammerEffect").GetComponents<AudioSource>();
        sound1 = audioSources[0];
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    //  当たり判定
    void OnTriggerEnter(Collider col)
    {
        //  Weaponタグに接触した時（ハンマーの振り下ろす時からWeaponタグになる）
        if (col.gameObject.tag == "Weapon")
        {
            //  Hpが残っているなら
            if (hp > 0)
            {
                //  叩いているエフェクトを再生
                particleHammer.Play();

                //  叩いているSEを再生
                sound1.Play();

                //  Hpを減らす
                hp--;

                //  アニメ付きでオブジェクトを揺らす
                iTween.ShakePosition(this.gameObject, iTween.Hash("z", 0.5f, "time", 1.0f));

                //  アニメ付きでオブジェクトを縮小し、叩いて壊れるアピールをする
                iTween.ScaleBy(this.gameObject, iTween.Hash("x", 0.5f, "y", 0.5f, "z", 0.5f, "time", 1.0f));
                //Debug.Log(hp);

                //  叩いたときにHpが0以下になったなら
                if (hp <= 0)
                {               
                    //  自分のparticleを破壊エフェクト用に再生


                    //  SE再生


                    //  破壊する
                    Destroy(this.gameObject, 1.0f);
                }
            }
        }
    }
}
