﻿using UnityEngine;

public class SwitchBox : MonoBehaviour {

    [SerializeField] GameObject[] targets;

    /// <summary>
    /// Hideタグのあるオブジェクトを非表示にする
    /// </summary>
    private void Start() {
        targets = GameObject.FindGameObjectsWithTag("Hide");
        for (int i = 0; i < targets.Length; i++) {
            targets[i].GetComponent<SkinnedMeshRenderer>().enabled = false;
        }
    }

    /// <summary>
    /// プレイヤーがこのスクリプトのアタッチされているスイッチボックスに入ったら表示する
    /// </summary>
    /// <param name="col"></param>
    private void OnTriggerEnter(Collider col) {        //  プレイヤーがswitchを踏んだらオン
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "PlayerDamage" || col.gameObject.tag == "Untouch" || col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge") {
            for (int i = 0; i < targets.Length; i++) {
                targets[i].GetComponent<SkinnedMeshRenderer>().enabled = true;
            }
        }
    }
}

