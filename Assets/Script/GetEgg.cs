﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//  エッグボーナスの中身を決めるクラス
public class GetEgg : MonoBehaviour {

    [SerializeField]
    ZoomOut zoomOutScript;                //  各スクリプトの格納用

    [SerializeField]
    ParticleSystem particle;              //  花火のエフェクト

    [SerializeField]
    ParticleSystem hammerEffect;          //  叩いた時のエフェクト

    [SerializeField]
    AudioSource hammerSE;                 //  叩いた時のSE

    [SerializeField]
    GameObject fireworks;                 //  花火の演出用

    [SerializeField]
    GameObject xpTextPrefab;              //  Xp表示用のプレファブ元

    TextMesh tMesh;                       //  XpText用
    GameObject xpText;                    //  Xp表示用のインスタンス格納用
    XpCounter xpCont;                     //  Xp加算用

    int bonusXp;
    int rank;

    Vector3 particlePos;

    //  Xpを設定するメソッド
    //  ClearBonusクラスから呼ばれる
    public void SetBonus()
    {
        //  ランダムな数値を取る
        int var = Random.Range(0, 101);

        //  ランク1
        if (0 < var && var < 45)
        {
            bonusXp = 100;
            rank = 1;
        }
        else if (45 <= var && var < 70)
        {
            bonusXp = 200;
            rank = 2;
        }
        else if (70 <= var && var < 85)
        {
            bonusXp = 300;
            rank = 3;
        }
        else if (85 <= var && var < 95)
        {
            bonusXp = 500;
            rank = 4;
        }
        else
        {
            bonusXp = 1000;
            rank = 5;
        }
        //  生成時にXPを設定し、XpCounterクラスに渡して加算する
        xpCont = GameObject.Find("ExpText").GetComponent<XpCounter>();
        xpCont.AddXp(bonusXp);

        //  獲得したxp表示用のインスタンス生成し、オブジェクトに入れる
        xpText = (GameObject)Instantiate(xpTextPrefab, transform.position, Quaternion.identity);
        tMesh = xpText.GetComponent<TextMesh>();

        //  rankで別のテキストを表示する
        switch (rank)
        {
            case 1:
                tMesh.text = "Nice!\n\n";
                break;

            case 2:
                tMesh.text = "Great!!\n\n";
                break;

            case 3:
                tMesh.text = "Awesome!!\n\n";
                break;

            case 4:
                tMesh.text = "Excellent!!\n\n";
                break;

            case 5:
                tMesh.text = "...Amaging!!!  Fantastic!!\n\n";
                break;

                default:
                break;
        }
        //  獲得したXpを表示する
        tMesh.text += "BonusXp + " + bonusXp.ToString() + " pt";

        //  xpTextを出現させる演出
        iTween.MoveTo(xpText, iTween.Hash("y", -58.0f, "time", 1.0f));

        //  メソッド呼び出し
        GetBonusEffect();

        //  xpTextを破壊する
        Destroy(xpText, 1.5f);
    }

    //  叩くと演出の判定をする
    //  XpはXpGetクラスのトリガーで判定される
    void GetBonusEffect()
    {
        //  叩いた演出とSE再生
        hammerEffect.Play();
        hammerSE.Play();

        //  ランクによる演出判定
        if (rank > 3)
        {
            //  カメラがズームインする
            //  オーブの位置情報をposで取得し
            //  zoomOutScript内のChestZoomInメソッドに引数として渡す
            Vector3 pos = this.transform.position;

            //  メソッド呼び出し
            zoomOutScript.ZoomIn(pos, "bonus");

            StartCoroutine("FireWorksEffect");
        }
        else
        {
            //particlePos.x = Random.Range(-2,2);
            //particlePos.z = Random.Range(1,3);

            //Vector3 pos = new Vector3(-34.5f + particlePos.x,-61.0f, -81.5f + particlePos.z);

            //fireworks.transform.position = pos;

            //  花火の演出
            StartCoroutine("FireWorksEffect");
        }
    }

    IEnumerator FireWorksEffect()
    {
        for (int i = 0; i < rank; i++)
        {
            particlePos.x = Random.Range(-2, 2);
            particlePos.z = Random.Range(1, 3);

            Vector3 pos = new Vector3(-34.5f + particlePos.x, -61.0f, -81.5f + particlePos.z);

            fireworks.transform.position = pos;

            particle.Play();

            yield return new WaitForSeconds(1.2f);

            particle.Stop();
        }
    }   
}
