﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestTopRotate : MonoBehaviour {

    //Transform target;
    ParticleSystem particle;

    GameObject stone;
    SpriteRenderer stoneRenderer;
    

    AudioSource sound1;             //  掛け声のSE 1-2
    AudioSource sound2;
    AudioSource sound3;             //  錠の開くSE

    public float parentPosY;        //  親のrotation.yの位置調整用
    public float speed;             //  回転する際の速度

    bool isOpenSwitch;               //  宝箱に接触したフラグを踏んだ時の管理用フラグ
    //bool isOpenSePlay;            //  宝箱を開けるSEのフラグ（２回鳴らさないため）

    // Use this for initialization
    void Start()
    {
        //  このオブジェクト自身の位置を取得
        //target = GetComponent<Transform>();
        //Debug.Log(target.transform.rotation.y);

        //  親オブジェクトを取得
        //parent = transform.parent.gameObject;
        //Debug.Log(parent.transform.rotation.y);
    }

    // Update is called once per frame
    void Update()
    {
        //  宝箱に接触したらスタート
        if (isOpenSwitch == true)
        {
            //  回転速度分を時間で動かす力を設定
            float step = speed * Time.deltaTime;         

            //  回転（フタを開くアニメ）
            //  rotation.yは親のrotationと同じにする
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0f, parentPosY, -30f), step);       
        }
    }

    //  宝箱に接触したら呼ばれる
    //  宝箱のフタを開ける処理のスイッチを入れる
    public void ChestOpen()
    {
        isOpenSwitch = true;

        //parent = chestPos;

        //Debug.Log(parent.y);
    

        StartCoroutine("OpenParticlePlay");
    }

    //  宝箱が開きつつparticleとSEを再生させる
    IEnumerator OpenParticlePlay()
    {
        //  SEとparticleを取得
        AudioSource[] audioSources = GetComponents<AudioSource>();
        sound1 = audioSources[0];
        sound2 = audioSources[1];
        sound3 = audioSources[2];

        particle = GetComponent<ParticleSystem>();

        //  子オブジェクトの位置と描画設定を取得
        stone = transform.GetChild(0).gameObject;
        stoneRenderer = stone.GetComponent<SpriteRenderer>();

        yield return new WaitForSeconds(0.2f);

        //  錠の開くSE再生
        sound3.Play();

        //  開き始めてから獲得SEとparticleを再生
        yield return new WaitForSeconds(0.2f);

        particle.Play();

        //  SE再生
        var randomValue = Random.Range(0, audioSources.Length);

        if (randomValue == 0)
        {
            sound1.Play();
        }
        else
        {
            sound2.Play();
        }

        //  開いてから
        yield return new WaitForSeconds(0.5f);

        //  描画設定をオンにしてルーンを表示
        stoneRenderer.enabled = true;

        //  ルーンオブジェクトの位置を取得
        Vector3 stonePos = stone.transform.position;

        //  オブジェクトを宝箱の中から空中にアニメ移動
        iTween.MoveFrom(stone, iTween.Hash("position", new Vector3(stonePos.x, stonePos.y -0.4f ,stonePos.z), "time", 0.8f));
    }
}
