﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitDialog : MonoBehaviour {

    [SerializeField] Canvas canvas;             //  ダイアログのキャンバス
    [SerializeField] Text yesButton;            //  ボタンのテキスト
    [SerializeField] Text noButton;
    [SerializeField] Text noticeText;           //  確認テキスト

    public bool isInActive;    //  キー操作無効のフラグ。GameOver.csで代入される

    // Use this for initialization
    void Start ()
    {
        //  ダイアログのキャンバスを非表示にする
        canvas.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isInActive)    //  GameOver状態なら無効化
        {
            canvas.enabled = false;
            return;
        }

        //  ESCキーを押したらダイアログを呼び出す
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //  ダイアログが開いていない場合
            if (canvas.enabled == false)
            {
                //  終了確認のダイアログを表示するメソッドの呼び出し
                OnApplicationQuit();
            }
            else //(canvas.enabled == true)  //  ダイアログが開いている場合
            {
                // 終了確認のダイアログを非表示するメソッドの呼び出し
                OnCallCancel();
            }
        }
        if (canvas.enabled)　　　　//　ダイアログが開いているならボタンでも操作可能
        {
            if (Input.GetButtonDown("Jump")) //  PS4 = 〇
            {
                OnCallExit();
            }
            if (Input.GetButtonDown("Run"))  //  PS4 = ×
            {
                OnCallCancel();
            }
        }
    }

    public void OnApplicationQuit()
    {
        //  ダイアログが開いていなければ終了処理はキャンセル
        if(canvas.enabled == false)
        
            Application.CancelQuit();

        //  操作とカメラ（上下浮遊）を止める
        //ZoomOut.cameraSetOn = false;

        //  ダイアログの表示
        canvas.enabled = true;

        //  日本語設定なら
        if (LanguageChoice.languageNum == 0) {
            noticeText.text = "アプリを終了しますか？";
            yesButton.text = "はい(space)";
            noButton.text = "いいえ(X)";
        }
        else
        {
            noticeText.text = "Exit the application. Are you okay ?";
            yesButton.text = "Yes(space)";
            noButton.text = "No(X)";
        }
    }

    //  終了ボタンのメソッド
    public void OnCallExit()
    {
        //Application.Quit();
        GameEnd();
    }

    public void OnCallCancel()
    {
        canvas.enabled = false;

        //  操作とカメラ（上下浮遊）を止める
        //ZoomOut.cameraSetOn = true;
    }

    public void GameEnd()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;

#elif UNITY_WEBGL
        Application.OpenURL("https://unityroom.com/");

#else //UNITY_STANDALONE
        Application.Quit();

#endif
    }
}
