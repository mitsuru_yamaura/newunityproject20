﻿using UnityEngine;
using UnityEngine.SceneManagement;    //　シーンマネジメントを有効にする

public class StageChangeBox : MonoBehaviour {

    /// <summary>
    /// ReturnLanternオブジェクトにアタッチされているクラス
    /// </summary>
    [SerializeField,HeaderAttribute("ステージ開放に必要なルーン数取得用")] TextSwitchBox textSwitchBox;
    public string nowSceneName;               // 現在のシーンを設定する
    public int nextStageNo;                   // 遷移したいシーンの番号    
    bool isAttacked;                          // 当たり判定を１回にするフラグ

    /// <summary>
    /// SEを再生する
    /// </summary>
    private void PlaySE() {
        AudioSource sound1;                              //  SE  1-2(GameScene1,2で異なる)
        AudioSource sound2;
        AudioSource[] audioSources = GetComponents<AudioSource>();
        sound1 = audioSources[0];
        sound2 = audioSources[1];
        var randomValue = Random.Range(0, audioSources.Length);  // ランダムにSE再生
        switch (randomValue) {
            case 0:
                sound1.Play();
                break;
            case 1:
                sound2.Play();
                break;
        }
    }

    private void OnTriggerEnter(Collider col){       
        if (Tutorial.tutorialClear == 1){                                                          // チュートリアルが終わっているなら            
            if (!isAttacked) {                                                                     // １回目の当たり判定なら
                if (col.gameObject.tag == "Weapon") {                                              // Weaponタグに接触した時（ハンマーの振り下ろす時からWeaponタグになる）
                    isAttacked = true;                                                             // １回目以上は当たらなくするフラグ   
                    iTween.ShakePosition(this.gameObject, iTween.Hash("z", 0.5f, "time", 1.0f));   // アニメ付きでオブジェクトを揺らす                                                      
                    if (RuneCounter.runeCount1 >= textSwitchBox.releaseRunes) {                    // 所持ルーンの数が各シーンに必要なルーン数を超えているなら
                        PlaySE();
                        if (nextStageNo == 1) {                                                    // 設定されているステージが1なら  
                            SceneManager.LoadScene("GameScene1");
                        } else if (nextStageNo == 2) {
                            SceneManager.LoadScene("GameScene3");
                        } else {
                            // TODO ステージ３への遷移を入れる
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// GameOver時にボタンから呼ばれる
    /// </summary>
    public void Reload() {
        GameObject.Find("SceneLoader").GetComponent<Loading>().NextScene(true);  // NextScene()でLoading画面を呼び出し、trueならステージセレクトシーンに遷移
    }
}
