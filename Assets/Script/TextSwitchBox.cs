﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 


public class TextSwitchBox : MonoBehaviour {

    public TextMesh tMesh;            //  text表示用のコンポーネントをアサイン  
    public SpriteRenderer sRen;       //  背景画表示用のコンポーネントをアサイン
    public GameObject stageTextBack;  //  背景表示用のオブジェクトをアサイン。iTween用

    public int releaseRunes;　　　　  //  ステージ開放に必要なルーン数
    public int stageNo;               //  ステージの番号
    
    Vector3 scalePos;　　　　　　　   //  元の大きさの保存用
    bool isRend;                      //  アニメ描画したかどうかのフラグ

    string STAGE_CLEAR_KER = "stageClearKey";  //  クリア情報の保存用

    // Use this for initialization
    void Start ()
    {
        //  拡大・縮小表示する描画用オブジェクトのScale値を取得する
        scalePos = stageTextBack.transform.localScale;

        //  各ステージのクリア回数を取得する。
        PlayerPrefs.GetInt(STAGE_CLEAR_KER, SaveData.firstStageClearCount);
        PlayerPrefs.GetInt(STAGE_CLEAR_KER, SaveData.secondStageClearCount);
    }

    //  プレイヤーがswitch内にいる間表示するメソッド
    private void OnTriggerStay(Collider col)
    {
        if (!isRend)
        { 
            //  プレイヤーがswitchを踏んだらオン
            if (col.gameObject.tag == "Player" || col.gameObject.tag == "PlayerDamage" || col.gameObject.tag == "Untouch" || col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
            {
                //  textと画像を表示する
                sRen.enabled = true;

                //  拡大・縮小表示する描画用オブジェクトのScale値を最小値にする
                stageTextBack.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

                //  元の大きさまでアニメで表示
                iTween.ScaleTo(stageTextBack, iTween.Hash("x", scalePos.x, "y", scalePos.y, "z", scalePos.z, "time", 1.0f));

                //  ステージの番号を名称を表示
                if (stageNo == 0)
                {
                    if (Tutorial.tutorialClear == 1)           //  チュートリアルが終了しているなら
                    {　　　　　　　　　　　　　　　　　　　　　//  ステージ選択画面に戻ると表示
                        if (LanguageChoice.languageNum == 0)   //  言語で表示テキスト分岐
                        {
                            tMesh.text = "ハンマーで叩くと\n[ゆりかご島(ステージ選択)]へ戻ります\n\n現在のルーンとXPがセーブされます";
                        }
                        else
                        {
                            tMesh.text = "Hit the hammer to \n return to the stage selection scene.\n\nRune and XP are saved.";
                        }
                    }
                }
                if (stageNo == 1)
                {
                    if (Tutorial.tutorialClear == 1)           //  チュートリアルが終了しているなら
                    {
                        if (LanguageChoice.languageNum == 0)   //  言語で表示テキスト分岐
                        {
                            tMesh.text = "Stage " + stageNo.ToString() + "\n\n砂埃の丘\n-Calmwindless Hill-\n\nクリア回数 : " + SaveData.firstStageClearCount.ToString() + " 回\n\n";
                        }
                        else
                        {
                            tMesh.text = "Stage " + stageNo.ToString() + "\n-Calmwindless Hill-\n\nNumber of times cleared : " + SaveData.firstStageClearCount.ToString() + "\n\n";
                        }
                    }
                    else                                       //  終了していないなら
                    {
                        if (LanguageChoice.languageNum == 0)   //  言語で表示テキスト分岐
                        {
                            tMesh.text = "チュートリアルが終了すると開放されます";
                        }
                        else
                        {
                            tMesh.text = "This stage will be released when the tutorial is over.";
                        }
                    }
                }
                if (stageNo == 2)
                {
                    if (LanguageChoice.languageNum == 0)   //  言語で表示テキスト分岐
                    {
                        tMesh.text = "Stage " + stageNo.ToString() + "\n\n極夜の峡谷\n-Valley of the Polar Night-\n\nクリア回数 : " + SaveData.secondStageClearCount.ToString() + " 回\n\n";
                    }
                    else
                    {
                        tMesh.text = "Stage " + stageNo.ToString() + "\n\n-Valley of the Polar Night-\n\nNumber of times cleared : " + SaveData.secondStageClearCount.ToString()+"\n\n";
                    }
                }
                if (stageNo == 3)
                {
                    if (LanguageChoice.languageNum == 0)   //  言語で表示テキスト分岐
                    {
                        tMesh.text = "Stage " + stageNo.ToString() + "\n\nムーンロア遺跡\n-Ruins of Moonlore-\n\nクリア回数 : 未設定\n\n";
                    }
                    else
                    {
                        tMesh.text = "Stage " + stageNo.ToString() + "\n\n-Ruins of Moonlore-\n\nNumber of times cleared : 未設定\n\n";
                    }
                }
            
                //  ステージが１以上なら（ステージ選択画面でないなら）
                if (stageNo > 0)
                {
                    //  開放されているかどうかを確認し、Textの表示内容を決める
                    //  集めているRuneの数が必要ルーン数を超えてるなら
                    if (RuneCounter.runeCount1 >= releaseRunes)
                    {
                        if (Tutorial.tutorialClear == 1)             //  チュートリアルが終了しているなら
                        {
                            if (LanguageChoice.languageNum == 0)     //  言語で表示テキスト分岐
                            {
                                //  ステージは解放されていると表示
                                tMesh.text += "ハンマーで叩いてステージスタート!!";
                            }
                            else
                            {
                                tMesh.text += "This stage has been released!\nPlease hit with a hammer and \nstart playing!!";
                            }
                        }
                    }
                    else
                    {
                        if (LanguageChoice.languageNum == 0)          //  言語で表示テキスト分岐
                        {
                            //  ステージの開放に必要なルーンを集めてと表示
                            tMesh.text += "ルーンを " + releaseRunes.ToString() + " 個 集めてステージを開放しよう!!";
                        }
                        else
                        {
                            tMesh.text += "This stage is not open.\nLet's collect and release " + releaseRunes.ToString() + " Runes!!";
                        }
                    }
                }
                //  描画フラグを立てる
                isRend = true;
            }
        }
    }

    //  プレイヤーがswitchから離れたら非表示にするメソッド
    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "PlayerDamage" || col.gameObject.tag == "Untouch")
        {
            //  テキストを空白に、背景の描画を非表示にする。描画フラグを降ろす
            tMesh.text = " ";

            if (isRend)
            {
                //  元の大きさまでアニメで表示
                iTween.ScaleTo(stageTextBack, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.5f));

                isRend = false;
            }
        }
    }
}
