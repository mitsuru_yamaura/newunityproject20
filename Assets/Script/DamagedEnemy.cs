﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagedEnemy : WatchKeeper
{
    Life lifeScript;                  //  ダメージ参照用の外部スクリプト
    UnitychanController uController;  //  キャラのノックバック用の外部スクリプト
    BreakObject bj;

    //  inTrigger用にコライダーを２つつけること
    public void OnCollisionEnter(Collision col)
    {
        //  ダメージ減少用のscriptの参照先
        lifeScript = GameObject.Find("HeartScore").GetComponent<Life>();

        //  ノックバック用のscriptの参照先
        uController = GameObject.Find("Kohaku").GetComponent<UnitychanController>();

        //  プレイヤーキャラクターに接触したとき
        if (col.gameObject.tag == "Player")
        {
            //  ノックバック用のメソッドを呼ぶ
            uController.KnockBackEffect();

            //  ダメージを減少させるスクリプトのメソッドを呼ぶ
            lifeScript.ApplyDamage(nowAttackPower);
        }

        //  WeaponかChargeタグに接触した時
        if (col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
        {
            //  ダメージ処理を呼ぶ
            OnDamage(scaleY);
        }
    }

    //  ダメージ処理のメソッド
    //  hpの減少、破壊、宝箱の処理はBreakObjectで行う
    void OnDamage(float scaleY)
    {
        //  ユーザーがダメージを受けないようにレイヤーを変えて当たりをなくす
        gameObject.layer = LayerMask.NameToLayer("EnemyDamage");
       
        //  コルーチンで一時的に動きを止める
        StartCoroutine("DamagePause",scaleY);
    }

    //  敵がハンマーで叩かれたときに呼ばれる
    IEnumerator DamagePause(float scaleY)
    {
        bj = GetComponent<BreakObject>();

        yield return new WaitForSeconds(0.6f);

        //  アニメ付きでオブジェクトを叩いて潰れたように見せる
        iTween.ScaleBy(this.gameObject, iTween.Hash("y", 0.3f, "time", 0.5f));

        //  hpが残っていれば元の状態に戻す
        if (bj.hp > 0)
        {
            yield return new WaitForSeconds(0.5f);

            iTween.ScaleTo(this.gameObject, iTween.Hash("y", scaleY, "time", 1.0f));
            Debug.Log(scaleY);

            yield return new WaitForSeconds(0.5f);

            //  レイヤーを元に戻す
            gameObject.layer = LayerMask.NameToLayer("Enemy");
        }
    }
}
