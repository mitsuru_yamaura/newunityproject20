﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenStageChecker : MonoBehaviour {

    [SerializeField] GameObject ElecEffect1;    //  Stage2の開放エフェクト
    [SerializeField] GameObject ElecEffect2;　　//  Stage3の開放エフェクト
    [SerializeField] int releaseRunes1;         //  Stage2の開放に必要なルーン数
    [SerializeField] int releaseRunes2;         //  Stage3の開放に必要なルーン数

    // Use this for initialization
    void Start ()
    {
        OpenCheck();
	}	

    void OpenCheck()
    {
        //  Stage2のルーンが規定値を超えたらエフェクト開始。超えていなければエフェクト非表示
        if (RuneCounter.runeCount1 >= releaseRunes1)
        {
            ElecEffect1.SetActive(true);
        }
        else
        {
            ElecEffect1.SetActive(false);
        }

        //  Stage3のルーンが規定値を超えたらエフェクト開始。超えていなければエフェクト非表示
        if (RuneCounter.runeCount1 >= releaseRunes2)
        {
            ElecEffect2.SetActive(true);
        }
        else
        {
            ElecEffect2.SetActive(false);
        }
    }
}
