﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ActiveRiver : MonoBehaviour {

    AudioSource sound1;

    private void OnEnable()
    {
        AudioSource[] audioSources = GetComponents<AudioSource>();
        sound1 = audioSources[0];
        sound1.Play();
        sound1.DOFade(0f, 2f).SetEase(Ease.Linear);
        StartCoroutine("SE");
    }

    IEnumerator SE()
    {
        yield return new WaitForSeconds(2.0f);
        sound1.Stop();
    }
}
