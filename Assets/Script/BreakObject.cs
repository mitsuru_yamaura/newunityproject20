﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BreakObject : MonoBehaviour {

    public int hp;                            //  HP
    public int exp;                           //  経験値。破壊された時にxpScriptに渡す

    public int chestNum;                      //  宝箱の番号。宝箱生成時にchestScriptに渡す

    public bool isChest;                      //  壊れた時に宝箱が出るかどうかのフラグ。オンなら出る
    public bool isHeart;                      //  壊れた時にハートが出現するかのフラグ。オンなら抽選。

    public float chestPosX;
    public float chestPosY;                   //  宝箱の生成位置の調整用
    public float chestPosZ;

    public float circlePosX;
    public float circlePosY;                  //  魔法陣の生成位置の調整用
    public float circlePosZ;

    public float xpPosY;                      //  XpTextの生成位置の調整用

    public GameObject chestPrefab;            //  宝箱のプレファブ元
    public GameObject circlePrefab;           //  サークルのプレファブ元
    public GameObject xpTextPrefab;           //  Xp表示用のプレファブ元

    UnitychanController kohakuScript;         //  Kohakuのscriptを格納
    Chest chestScript;                        //  宝箱のscriptを格納
    XpCounter xpScript;                       //  ExpTextのscriptを格納
    HeartSpawner hSpawner;                    //  ハート生成のscriptを格納
    EggManager eggManager;                    //  獲得XPと破壊したカウントを送る用

    GameObject chest;                         //  宝箱のインスタンス格納用
    GameObject circle;                        //  サークルのインスタンス格納用
    GameObject xpText;                        //  xp表示用のインスタンス格納用

    ParticleSystem particleHammer;            //  ハンマーでたたいた時のエフェクト用
    AudioSource sound1;

    //AudioClip audioClip1;
    TextMesh textMesh;

    int damage;                               //  ハンマーのダメージを格納用。ハンマーからもらう
    bool isGet;
    bool isCameraSetOn;                       //  一時停止。カメラの移動にかかわるときにオン／オフのフラグ

    // Use this for initialization
    void Start ()
    {
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        //  動作無効のフラグ
        isCameraSetOn = ZoomOut.cameraSetOn;

        if (isCameraSetOn == false)
        {
            return;
        }

        if (isCameraSetOn == true)
        {
            if (!isGet)
            {
                //  叩かれた時のSEを取得
                //AudioSource audioSource = GameObject.Find("HammerEffect").GetComponent<AudioSource>();
                //sound1 = audioSource;

                isGet = true;
            }
        }
    }

    //  当たり判定
    void OnTriggerEnter(Collider col)
    {
        //  kohakuScriptに主人公のscriptを格納
        kohakuScript = GameObject.Find("Kohaku").GetComponent<UnitychanController>();
        
        //  Weapon/Chargeタグに接触した時（ハンマーの振り下ろす時からWeapon/Chargeタグになる）
        if (col.gameObject.tag == "Weapon"|| col.gameObject.tag == "Charge")
        {
            //  タグの種類で攻撃力を変える
            if (col.gameObject.tag == "Weapon")
            {
                //  宝箱か壊せるオブジェクトか敵なら
                if (this.gameObject.tag == "Chest" || this.gameObject.tag == "NomalBreak"||this.gameObject.tag == "Enemy")
                {
                    //  通常の攻撃力を渡す
                    damage = UnitychanController.hammerPower;

                    //  ダメージ処理のメソッド呼び出し
                    Damage(damage);
                }
                if(this.gameObject.tag == "Construct")
                {
                    //  0の攻撃力を渡す(チャージのために、当たる演出だけ出しておく)
                    damage = 0;

                    //  ダメージ処理のメソッド呼び出し
                    Damage(damage);
                }
            }

            if (col.gameObject.tag == "Charge")
            {
                //  上記＋チャージのみ破壊可能なオブジェクトなら
                if (this.gameObject.tag == "Chest" || this.gameObject.tag == "NomalBreak" || this.gameObject.tag == "Construct" || this.gameObject.tag == "Enemy")
                {
                    //  チャージの攻撃力を渡す
                    damage = UnitychanController.hammerPower + kohakuScript.chargePower;

                    Damage(damage);
                }
            }
        }
    }

    //  damageを受け取ってダメージ処理をする
    void Damage(int damage)
    {
        //  Hpが残っているなら
        if (hp > 0)
        {
            //Debug.Log(damage);
            //  ハンマーオブジェクトの子であるHammerEffectのパーティクルを取得
            particleHammer = GameObject.Find("HammerEffect").GetComponent<ParticleSystem>();

            //  叩いたSEをハンマーオブジェクトの子であるHammerEffectから取得
            AudioSource[] audioSources = GameObject.Find("HammerEffect").GetComponents<AudioSource>();
            sound1 = audioSources[0];

            //  叩いているエフェクトを再生
            particleHammer.Play();

            //  叩いているSEを再生
            sound1.Play();

            //  Hpを攻撃力分だけ減らす
            hp -= damage;

            //Debug.Log(hp);

            if (damage > 0)
            {
                //  アニメ付きでオブジェクトを揺らす
                iTween.ShakePosition(this.gameObject, iTween.Hash("z", 0.5f, "time", 1.0f));

                //  アニメ付きでオブジェクトを縮小し、叩いて壊れるアピールをする
                iTween.ScaleBy(this.gameObject, iTween.Hash("x", 0.7f, "y", 0.7f, "z", 0.7f, "time", 1.0f));
            }

            //  叩いたときにHpが0以下になったなら
            if (hp <= 0)
            {
                //  アニメ付きでオブジェクトを縮小し、叩いて壊れるアピールをする
                iTween.ScaleBy(this.gameObject, iTween.Hash("x", 0.01f, "time",1.0f));

                //  ハートを出すフラグにチェックがある
                if (isHeart)
                {
                    //  スクリプトのコンポーネント取得
                    hSpawner = this.gameObject.GetComponent<HeartSpawner>();

                    //  メソッドの呼び出し
                    hSpawner.HeartCreate();
                }

                //  宝箱を出すフラグにチェックがある
                if (isChest)
                {
                    //  この宝箱を取得したことがない
                    if (!FlagManager.Instance.flags[chestNum])
                    {
                        //  宝箱を取得したことがなく宝箱を生成できる
                        //  宝箱インスタンス用のメソッドの呼び出し
                        CreateChest();
                    }                    
                }
                //  int型のxpAddBonusをfloatに変えてXpボーナスを決定する
                float value = 1.0f + (UnitychanController.xpAddBonus * 0.01f);
                Debug.Log(value);

                //  floatにしてボーナス分の乗算の計算する
                float lastExp = exp * value;
                Debug.Log(lastExp);

                //  int型にキャストしてexpに戻す
                exp = (int)lastExp;
                Debug.Log(exp);

                //  expを渡すscriptを格納
                xpScript = GameObject.Find("ExpText").GetComponent<XpCounter>();

                //  expを渡す
                xpScript.AddXp(exp);

                //  クリア時用のXPと破壊カウントを渡すscriptを格納
                eggManager = GameObject.Find("ClearOrb").GetComponent<EggManager>();

                //  xpを渡す
                eggManager.Breaker(exp);

                //  破壊カウントを渡す
                eggManager.Counter(1);

                //  このオブジェクトを基準にxp表示用のインスタンスの生成位置を設定する
                Vector3 xpPos = this.transform.position;

                //  獲得したxp表示用のインスタンス生成する
                xpText = (GameObject)Instantiate(xpTextPrefab, new Vector3(transform.position.x, transform.position.y + xpPosY, transform.position.z), Quaternion.identity);

                textMesh = xpText.GetComponent<TextMesh>();

                textMesh.text = exp.ToString(); 

                //  xpTextを出現させる演出
                iTween.MoveFrom(xpText, iTween.Hash("y", -0.5f, "time", 1.0f));

                //  破壊する(NoneBreakObjectより0.1f長い＝宝箱を出すため)
                Destroy(this.gameObject, 1.1f);

                //  xpTextを破壊する
                Destroy(xpText, 1.1f);
            }
        }
    }


    //  演出の魔法陣と宝箱を作るメソッド
    void CreateChest()
    {
        //  このオブジェクトを基準に生成する位置を設定する
        Vector3 circlePos = this.transform.position;

        //  魔法陣のインスタンスを生成する
        circle = (GameObject)Instantiate(circlePrefab, new Vector3(circlePos.x + circlePosX, circlePos.y + circlePosY + 0.5f, circlePos.z + circlePosZ), Quaternion.identity);

        //  魔法陣を下方から出現させて、上方に移動させる演出
        iTween.MoveFrom(circle, iTween.Hash("y", -100f, "time", 0.8f));

        StartCoroutine("SetChest");
    }


    IEnumerator SetChest()
    {
        //  魔法陣が出現するのを待ってから
        yield return new WaitForSeconds(1.0f);

        //  このオブジェクトを基準に生成する位置を設定する
        Vector3 chestPos = this.transform.position;

        //  生成元となるオブジェクトを基準に生成する向きを設定
        Quaternion rot = this.transform.rotation;

        //  宝箱のプレファブから宝箱のインスタンスを生成
        chest = (GameObject)Instantiate(chestPrefab, new Vector3(chestPos.x + chestPosX , chestPos.y + chestPosY, chestPos.z + chestPosZ), Quaternion.Euler(rot.x, rot.y - 70f, rot.z));

        //  生成元となるオブジェクトから紐づけし、宝箱に番号をつける
        chest.name = "chest" + chestNum;

        //  宝箱インスタンスのChestscriptを取得し
        chestScript = chest.GetComponent<Chest>();

        //  メソッドを呼び出して、宝箱の番号を渡す
        //  壊したオブジェクトと宝箱の番号を同じに紐づけする
        chestScript.GetChestNum(chestNum);

        //  魔法陣を子、宝箱を親にする（宝箱が消えるときに一緒に消えるように）
        circle.transform.parent = chest.transform;
    }
}
