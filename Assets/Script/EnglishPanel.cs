﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnglishPanel : MonoBehaviour {

    [SerializeField] Text jumpPanelText;                //  各パネル
    [SerializeField] Text disarmPanelText;
    [SerializeField] Text actionPanelText;
    [SerializeField] Text setPanelText;
    [SerializeField] Text dashPanelText;
    [SerializeField] Text movePanelText;
    [SerializeField] Text pausePanelText;
    [SerializeField] Text zoomPanelText;

    // Use this for initialization
    void Start () {
        if (LanguageChoice.languageNum == 0)
        {
            jumpPanelText.text ="ジャンプ(Space)";
            disarmPanelText.text = "ハンマーをしまう(X)";
            actionPanelText.text = "ハンマーを振る(Z)";
            setPanelText.text = "ハンマーを構える(Z)";
            dashPanelText.text = "ダッシュ\n(ハンマーしまって移動＋X)";
            movePanelText.text = "前後左右に移動\n(W / A / S / D or 矢印)";
            pausePanelText.text = "ステータス表示 / 非表示(P)";
            zoomPanelText.text = "ズーム & 元の位置に戻す/\n(MouseWheel & O)";
        }
        else
        {
            jumpPanelText.text = "Jump(Space)";
            disarmPanelText.text = "Put away the hammer(X)";
            actionPanelText.text = "Hit a hammer(Z)";
            setPanelText.text = "Set up the hammer(Z)";
            dashPanelText.text = "Move + (X) = Dash\n(Put away the hammer)";
            movePanelText.text = "Move(Arrow keys/\nW / A / S / D)";
            pausePanelText.text = "Display status/\nHide status(P)";
            zoomPanelText.text = "Zoom & Return to default/\n(MouseWheel & O)";
        }

    }
}
