﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerLevelImage : MonoBehaviour {

    private int nowForceLevel;                                    // 現在の攻撃力の取得用
    private int maxForceLevel =10;                                // ForceLevelの最大値
    private GameObject[] powerObject;                             // インスタンスを格納するPowerオブジェクト用の配列
    private Vector3 powerPosition = new Vector3(30f, 380f, 0);    // Powerの初期座標
    public GameObject powerPrefab;                                // PowerImageのプレハブ

    //  プロパティ
    public int ForceLevelPoint{
        set{
            // 現在の攻撃力をスクリプトから取得して代入
            nowForceLevel = value;

            // 無効化
            for (int i = 0; i < maxForceLevel; i++) {
                powerObject[i].SetActive(false);
            }
        }
        get{
            return nowForceLevel;
        }
    }

    void Start(){
        // ゲームオブジェクトの配列を生成
        // ここでそれぞれの最大値を設定しておく
        powerObject = new GameObject[maxForceLevel];

        // 初期値分のForceオブジェクト生成
        // ここでは最大値まで生成しておく
        for (int i = 0; i < maxForceLevel; i++){
            // インスタンス生成
            powerObject[i] = Instantiate(powerPrefab, powerPosition, Quaternion.identity) as GameObject;

            // 子供にする
            powerObject[i].transform.SetParent(gameObject.transform, true);

            // 位置座標と画像の大きさを設定
            powerObject[i].GetComponent<RectTransform>().sizeDelta = new Vector3(1, 1, 1);
        }
        // 現在のLevelを代入してプロパティ実行
        this.ForceLevelPoint = UnitychanController.forceLevel;
    }
}
