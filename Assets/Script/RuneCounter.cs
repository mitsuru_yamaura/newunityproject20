﻿using UnityEngine;
using UnityEngine.UI;
   
public class RuneCounter : MonoBehaviour {

    public static int runeCount1;      //  stage1のルーン合計値  
    Text runeCountText;
    int nowRuneCount;                  //  現在値
    string RUNE_KEY = "runeKey1";      //  PlayerPrefsで各スキル保存／呼び出すためのキー

    void Start (){        
        runeCount1 = PlayerPrefs.GetInt("runeKey1", 0);     // 今までに獲得したルーンをステージごとに取得。取得していなければ0(始めに0が入る)
        nowRuneCount = runeCount1;                          // ここで全部のシーンのルーンを合計する
        runeCountText = GetComponent<Text>();
        runeCountText.text = nowRuneCount.ToString();       // 画面に取得合計ルーン数を表示
    }

    /// <summary>
    /// ルーンの数を加算し、画面表示を更新
    /// </summary>
    /// <param name="runePoint"></param>
    public void GetRune(int runePoint){       
        nowRuneCount += runePoint;                          // ステージ内で獲得した分を現在値に加算
        runeCount1 = nowRuneCount;                          // stageごとに合計値を上書き。ステージが増えたらステージ数を引数でもらい分岐させる
        runeCountText.text = nowRuneCount.ToString();       // 画面のルーン表示数を更新  
        SaveData.Save(RUNE_KEY, runeCount1);　　　　　　    // ルーンの合計値を保存する
    }
}
