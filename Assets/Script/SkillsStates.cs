﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillsStates : MonoBehaviour {

    Text skillsStateText;

    //  戻り値確認のフラグ
    public int isHighJumpGet;                 //  パワーアップスキル「ハイジャンプ」を取得しているか
    public int isSpeedUpGet;                  //  パワーアップスキル「スピードアップ」を取得しているか
    public int ischargeHummerGet;             //  パワーアップスキル「チャージハンマー」を所持しているか

    // Use this for initialization
    void Start () {

        skillsStateText = GetComponent<Text>();

        isHighJumpGet = PlayerPrefs.GetInt("highJumpKey", 0);
        isSpeedUpGet = PlayerPrefs.GetInt("speedUpKey", 0);
        ischargeHummerGet = PlayerPrefs.GetInt("chargeHammerKey", 0);
    }

    public void SkillState()
    {
        isHighJumpGet = PlayerPrefs.GetInt("highJumpKey", 0);
        isSpeedUpGet = PlayerPrefs.GetInt("speedUpKey", 0);
        ischargeHummerGet = PlayerPrefs.GetInt("chargeHammerKey", 0);

        if (isHighJumpGet == 0)
        {
            skillsStateText.color = new Color(130f/255f, 130f/255f, 130f/255f);
            skillsStateText.text = " not mastered...\n";
        }
        else
        {
            //  Text内の文字を一部だけ黒に変更する
            //  ここで色を変えてしまうとすべての色が変わるため
            skillsStateText.text = "<color=Black>  Master !! </color>\n";
        }

        if (isSpeedUpGet == 0)
        {
            skillsStateText.color = new Color(130f / 255f, 130f / 255f, 130f / 255f);
            skillsStateText.text += " not mastered...\n";
        }
        else
        {
            skillsStateText.text += "<color=Black>  Master !! </color>\n";
        }

        if (ischargeHummerGet == 0)
        {
            skillsStateText.color = new Color(130f / 255f, 130f / 255f, 130f / 255f);
            skillsStateText.text += " not mastered...";
        }
        else
        {
            skillsStateText.text += "<color=Black>  Master !! </color>";
        }
    }
}
