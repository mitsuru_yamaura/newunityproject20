﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimateButton : MonoBehaviour {

    Button menuButton;

    // Use this for initialization
    void Start () {
        menuButton = GetComponent<Button>();
        menuButton.animator.SetTrigger("Highlighted");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
