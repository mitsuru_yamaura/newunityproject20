﻿using System.Collections;
using UnityEngine;
using UnityStandardAssets.ImageEffects;     //  DepthOfField用
using UnityEngine.SceneManagement;          //　シーンマネジメントを有効にする

public class ZoomOut : MonoBehaviour{

    [SerializeField] GameObject mainCamObj;    // mainCamObj = Camera.main.gameObject;
    [SerializeField] DepthOfField field;  //  画面のぼかしエフェクトのオンオフ用             
    public static bool cameraSetOn;       //  キャラ操作無効のスイッチ。trueで操作有効化。UnitychanController,Enemy,Chest,Pauseで利用する 

    private void Start(){        
        SetMoveCamera();
    }

    /// <summary>
    /// MainCameraの位置をそれぞれの指定座標からゲーム定位置に2.5秒かけてアニメ移動
    /// </summary>
    private void SetMoveCamera(){        
        iTween.RotateFrom(mainCamObj, iTween.Hash("x", 30f, "time", 2.5f));
        iTween.MoveFrom(mainCamObj, iTween.Hash("z", -85f, "time", 2.5f));
        StartCoroutine(OffEffectCamera());
    }

    /// <summary>
    /// カメラ定位置への移動に合わせて画面エフェクトとキー操作無効を解除
    /// </summary>
    /// <returns></returns>
    private IEnumerator OffEffectCamera(){        
        yield return new WaitForSeconds(2.3f);        // 2.3/2.5秒後に画面のぼかしを元に戻す(focalSize 0f->1f)
        field.focalSize = 1.0f;        
        yield return new WaitForSeconds(0.2f);        // 0.2/2.5秒後にキー操作を有効にするフラグを立てる
        cameraSetOn = true;
        Debug.Log(cameraSetOn);
    }

    /// <summary>
    /// カメラのズームイン。対象物によりズーム位置を調整してズームインする
    /// </summary>
    /// <param name="pos"></param>
    public void ZoomIn(Vector3 pos, string targetName) {
        cameraSetOn = false;                                      // キー操作を無効にする（カメラの位置ずれをなくすため）        
        Vector3 setPos = mainCamObj.transform.position;           // 現在のMainCameraの位置を取得（後で戻すために一時保存）
        switch (targetName) {                                     // 引数Posに対して、対象となるオブジェクトによりカメラ位置を調整する
            case "chest":
                pos = new Vector3(pos.x, pos.y + 1.5f, pos.z - 5f);      // 宝箱の位置情報のとき
                break;
            case "powerup":
                pos = new Vector3(pos.x, pos.y + 2.5f, pos.z - 8f);      // ライフフラワーとフォースツリーの位置情報のとき
                break;
            case "bonus":
                pos = new Vector3(pos.x, pos.y, pos.z - 5.0f);           // ステージクリア時のボーナスの位置情報のとき
                break;
            case "clear":
                pos = new Vector3(pos.x - 1.5f, pos.y - 1.0f, pos.z - 3.5f);   // ステージクリアの位置情報のとき
                break;
        }
        //mainCamObj.transform.position = pos;                   // カメラの位置を移動し、宝箱をフォーカスする(どちらでもできるが、iTweenで移動)
        iTween.MoveTo(mainCamObj, iTween.Hash("position", new Vector3(pos.x, pos.y, pos.z), "time", 1.2f));
        StartCoroutine(ZoomEndDefaultCamera(setPos));
    }

    /// <summary>
    /// ズームインを終了してカメラを定位置に戻す
    /// </summary>
    /// <returns></returns>
    private IEnumerator ZoomEndDefaultCamera(Vector3 setPos){       
        yield return new WaitForSeconds(1.5f);　　　　　　　　// カメラを宝箱にズームインしている時間        
        Vector3 getPos = setPos;　　　　　　　　　　　　　　　// 現在のMainCameraの位置を一時保存した変数から取得し、元の位置を入れなおす。       
        //mainCamObj.transform.position = getPos;　　　　　　 // カメラを元の位置に移動し、プレイヤーをフォーカスする(どちらでもできるが、iTweenで移動)
        iTween.MoveTo(mainCamObj, iTween.Hash("position", new Vector3(getPos.x, getPos.y, getPos.z), "time", 1.2f));
        yield return new WaitForSeconds(1.2f);        
        cameraSetOn = true;                                   // キー操作を有効に戻す
    }

    /// <summary>
    /// 次のシーンに遷移する際のカメラ移動。ステージ終了時、あるいはステージセレクト時にオブジェクト叩いたら呼ばれる
    /// </summary>
    /// <param name="nextStageNo"></param>
    public void ChangeSceneZoomIn(int nextStageNo){       
        int loadStageNo = nextStageNo;　                                      // もらった引数（遷移先のシーン数）を代入        
        GameObject.Find("ExpText").GetComponent<XpCounter>().Save();          // Xpをセーブするためにscriptを探してセーブするメソッドの呼び出し        
        cameraSetOn = false;                                                  //  操作を無効にする（カメラの位置ずれをなくすため）    
        iTween.RotateTo(mainCamObj, iTween.Hash("x", 30f, "time", 2.5f));     //  MainCameraの位置をそれぞれの指定座標に2.5秒かけて移動させる（ZoomIn）
        iTween.MoveTo(mainCamObj, iTween.Hash("z", -85f, "time", 2.5f));
        field.aperture = 0.0f;                                                // 画面をぼかす(focalSize 1f->0f)
        StartCoroutine(LoadSceneWait(loadStageNo));                           // 遷移先のシーンを呼び出す(int型で渡す)
    }

    /// <summary>
    /// 遷移するシーンの呼び出し
    /// </summary>
    /// <param name="loadStageNo"></param>
    /// <returns></returns>
    private IEnumerator LoadSceneWait(int loadStageNo){
        yield return new WaitForSeconds(1.0f);        
        switch (loadStageNo) {
            case 0:                
                SceneManager.LoadScene("GameScene2");
                break;
            case 1:
                SceneManager.LoadScene("GameScene1");
                break;
            case 2:
                SceneManager.LoadScene("GameScene3");
                break;
            case 3:
                //SceneManager.LoadScene("GameScene4");
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// ステージクリア時のカメラの移動。ResultSwitch.csより呼ばれる
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="targetName"></param>
    public void ClearCameraPoint(Vector3 pos, string targetName){
        ZoomIn(pos, "clear");                                          //  ZoomInさせる
        GameObject kohaku = GameObject.Find("Kohaku");                 //  コンポーネントを取得
        iTween.RotateFrom(kohaku,iTween.Hash("y",180,"time",1.5f));    //  Kohakuを正面に向ける        
        StartCoroutine(ClearAnimeZoomIn(kohaku));
    }

    /// <summary>
    /// ステージクリア時のカメラ動作とアニメ処理
    /// </summary>
    /// <returns></returns>
    private IEnumerator ClearAnimeZoomIn(GameObject kohaku){        
        yield return new WaitForSeconds(0.5f);        
        kohaku.GetComponent<UnitychanController>().Win();
        yield return new WaitForSeconds(3.0f);
        kohaku.GetComponent<Animator>().SetBool("Win", false);    //  クリアアニメを終了する        
        cameraSetOn = true;                                       //  操作を有効に戻す
    }
}
