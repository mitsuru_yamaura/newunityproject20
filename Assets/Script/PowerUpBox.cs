﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PowerUpBox : MonoBehaviour {

    [SerializeField]
    ParticleSystem particle;            //  スキル取得時の演出

    public int powerUpNo;                //  このオブジェクトを叩いたときのパワーアップする内容を決める

    public static int highJump;         //  powerUpNoで指定されたフラグを立てる
    public static int speedUp;          //  同時に２回以上は叩けないようにする（xp消費するため）
    public static int chargeHammer;

    public int skillXp;                  //  オフジェクトの持つパワーアップのための必要なxp

    int nowXpCount;                      //  xpの現在値。xpが足りていて取得可能かどうかの確認用に使用
    string initTag;                      //  スキル取得時に元のTagの情報を入れておく

    XpCounter xpCountScript;             //  スキル取得時のXp減算用のscript格納用
    UnitychanController uCont;

    AudioSource sound1;                 //  演出用

    string JumpKey = "highJumpKey";      //  PlayerPrefsで各スキル保存／呼び出すためのキー
    string SpeedKey = "speedUpKey";
    string ChargeKey = "chargeHammerKey";

    // Use this for initialization
    void Start()
    {
        //  取得しているスキルをPlayerPrefより取得する
        highJump = PlayerPrefs.GetInt("highJumpKey", 0);
        speedUp = PlayerPrefs.GetInt("speedUpKey", 0);
        chargeHammer = PlayerPrefs.GetInt("chargeHammerKey", 0);

        //  すでに取得済のスキルのボックスは表示しない
        if (powerUpNo == 0)
        {
            if (highJump == 1)
            {
                gameObject.SetActive(false);
            }
        }
        if (powerUpNo == 1)
        {
            if (speedUp == 1)
            {
                gameObject.SetActive(false);
            }
        }
        if (powerUpNo == 2)
        {
            if (chargeHammer == 1)
            {
                gameObject.SetActive(false);
            }
        }     
    }

    //  当たり判定
    private void OnTriggerEnter(Collider col)
    {
        uCont = GameObject.Find("Kohaku").GetComponent<UnitychanController>();

        //  Weaponタグに接触した時（ハンマーの振り下ろす時からWeaponタグになる）
        if (col.gameObject.tag == "Weapon")
        {            
            //  xpの現在値を取得し、獲得可能か確認させる
            nowXpCount = XpCounter.xpCount;

            //  xpの現在値が必要なスキルポイント以上なら
            if (nowXpCount >= skillXp)
            {
                //  まだ取得していないなら叩ける
                if (highJump == 0)
                {
                    //  No0 = highJump
                    if (powerUpNo == 0)
                    {
                        //  highJumpのフラグを立てる
                        highJump = 1;

                        //  SaveData内のフラグセーブメソッドを呼び出す
                        //  public宣言してあるscriptなので格納不要。直接の名前で呼べる
                        SaveData.Save(JumpKey, highJump);

                        //  skillXpを渡す（減少分）
                        SkillLearned(skillXp);

                        //  タグ繰り替え用のコルーチン
                        StartCoroutine("TagBack");
                    }
                }
                if (speedUp == 0)
                {
                    //  No1 = speedUp
                    if (powerUpNo == 1)
                    {
                        speedUp = 1;
                        
                        SaveData.Save(SpeedKey, speedUp);

                        SkillLearned(skillXp);                       

                        StartCoroutine("TagBack");
                    }
                }
                if (chargeHammer == 0)
                {
                    //  No2 = chargeHammer
                    if (powerUpNo == 2)
                    {
                        chargeHammer = 1;
                        
                        SaveData.Save(ChargeKey, chargeHammer);

                        SkillLearned(skillXp);
                        
                        StartCoroutine("TagBack");
                    }
                }
            }
        }
    }

    //  XPを減算し、このオブジェクト群を破壊するメソッド
    void SkillLearned(int skillXp)
    {
        //  xpを減少させるためにscriptを取得
        xpCountScript = GameObject.Find("ExpText").GetComponent<XpCounter>();

        //  演出を再生
        particle.Play();

        //  叩いているSEを再生
        //sound1.Play();                

        //  アニメ付きでオブジェクトを揺らす
        iTween.ShakePosition(this.gameObject, iTween.Hash("z", 0.5f, "time", 2.0f));

        //  アニメ付きでオブジェクトを縮小し、叩いて壊れるアピールをする
        iTween.ScaleBy(this.gameObject, iTween.Hash("x", 1.3f, "y", 1.3f, "z", 1.3f, "time", 2.2f));
        //Debug.Log(hp);

        //  skillXpを引数にして、XpCounterのSubtractXpメソッドを呼び出す
        xpCountScript.SubtractXp(skillXp);

        //  プレイヤーに取得状態を反映する
        uCont.SkillStateGet();

        //  破壊する
        Destroy(this.gameObject, 2.2f);
    }

    IEnumerator TagBack()
    {
        //  インフォメーションテキストを非表示に２回叩けないようにUnitychanのタグを変える
        initTag = uCont.gameObject.tag;

        //  タグを切り替える
        uCont.gameObject.tag = "LevelUp";

        yield return new WaitForSeconds(1.5f);

        //  元のタグに戻す
        uCont.gameObject.tag = initTag;
        Debug.Log(initTag);
    }
}


