﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMove : MonoBehaviour {

    public float amplitude;   //  振幅幅

    int frameCount = 0;  //  フレームカウント

    bool isCameraSetOn;         //  一時停止。カメラの移動にかかわるときにオン／オフのフラグ

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        //  ゲームの実行状態のフラグを取得。ゲーム実行中ならTrue
        isCameraSetOn = ZoomOut.cameraSetOn;

        //  ゲームがストップしたら上下浮遊を止める
        //  宝箱取得時、ゲームスタート、シーン移動、Pause実行時
        if (isCameraSetOn == false)
        {
            return;
        }
    }

    private void FixedUpdate()
    {
        //  ゲームが実行状態ならカメラを上下に動かす
        if (isCameraSetOn == true)
        {
            frameCount += 1;

            if (10000 <= frameCount)
            {
                frameCount = 0;
            }

            if (0 == frameCount % 2)
            {
                float posYSin = Mathf.Sin(2.0f * Mathf.PI * (float)(frameCount % 200) / (200.0f - 1.0f));
                iTween.MoveAdd(gameObject, new Vector3(0, amplitude * posYSin, 0), 0.0f);
            }
        }
    }
}
