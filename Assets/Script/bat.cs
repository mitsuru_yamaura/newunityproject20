﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bat : MonoBehaviour {

    BreakObject bj;
    Life lifeScript;                  //  ダメージ参照用の外部スクリプト
    UnitychanController uController;  //  キャラのノックバック用の外部スクリプト

    bool isCameraSetOn;         //  一時停止。カメラの移動にかかわるときにオン／オフのフラグ

    float faceCount;            //  向きを変えるまでの待機時間のカウンター
    float scaleY;               //  スケールYの元の値

    int facing = 0;             //  向きの確認用
    int randomFace;             //  向きを変えるまでの待機時間の設定値

    public int attackPower;     //  攻撃力
    public bool isFacing;       //  向きを変えるかどうか
    public bool isCharge;       //  チャージでのみダメージが当たるかどうか。Trueならチャージのみ

    int nowAttackPower;


    // Use this for initialization
    void Start()
    {
        //  現在地に初期値の取得。戻す場合に設定値を使うため
        nowAttackPower = attackPower;
        
        //  小さくなった時に元に戻る値を入れておく
        scaleY = this.transform.localScale.y;

        //  スタートがかかるまで、あるいは宝箱の処理が終わるまで待機のフラグ
        //  オンの時は一時停止。カメラの移動にかかわるときにオン／オフ
        isCameraSetOn = ZoomOut.cameraSetOn;

        //  スタート時に各アクションの待機時間をランダムに設定する
        randomFace = Random.Range(4, 7);
    }

    // Update is called once per frame
    void Update()
    {
        //  動作無効のフラグ
        isCameraSetOn = ZoomOut.cameraSetOn;

        if (isCameraSetOn == false)
        {
            return;
        }

        if (isCameraSetOn == true)
        {
            //  向きを変えるフラグがあるなら
            if (isFacing)
            {
                //  待機カウント開始
                faceCount += Time.deltaTime;

                //  待機時間のカウントがランダムで設定した数値以上になったら方向転換
                if (faceCount > randomFace)
                {
                    if (facing == 0)
                    {
                        //  向きを90度変える
                        transform.rotation = Quaternion.AngleAxis(90, new Vector3(0, 1, 0));
                        facing = 1;
                    }
                    else
                    {
                        //  向きを-90度変える
                        transform.rotation = Quaternion.AngleAxis(-90, new Vector3(0, 1, 0));
                        facing = 0;
                    }

                    //  カウントを戻す
                    faceCount = 0f;

                    //　次回の待機時間をランダムで設定する
                    randomFace = Random.Range(4, 7);
                }
            }
        }
    }

    //  inTrigger用にコライダーを２つつけること
    public void OnTriggerEnter(Collider col)
    {
        //  ダメージ減少用のscriptの参照先
        lifeScript = GameObject.Find("HeartScore").GetComponent<Life>();

        //  ノックバック用のscriptの参照先
        uController = GameObject.Find("Kohaku").GetComponent<UnitychanController>();

        //  プレイヤーキャラクターに接触したとき
        if (col.gameObject.tag == "Player")
        {
            //  ノックバック用のメソッドを呼ぶ
            uController.KnockBackEffect();

            //  ダメージを減少させるスクリプトのメソッドを呼ぶ
            lifeScript.ApplyDamage(nowAttackPower);
        }

        //  チャージのみ有効かどうか。Falseならどちらでも当たる
        if (!isCharge)
        {
            //  WeaponかChargeタグに接触した時
            if (col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
            {
                //  ダメージ処理を呼ぶ
                OnDamage();
            }
        }
        else  //  Trueの場合はチャージのみ当たる
        {
            //  Chargeタグに接触した時
            if (col.gameObject.tag == "Charge")
            {
                //  ダメージ処理を呼ぶ
                OnDamage();
            }
        }
    }

    //  ダメージ処理のメソッド
    //  hpの減少、破壊、宝箱の処理はBreakObjectで行う
    void OnDamage()
    {
        //  ユーザーがダメージを受けないようにレイヤーを変えて当たりをなくす
        gameObject.layer = LayerMask.NameToLayer("EnemyDamage");

        //  コルーチンで一時的に動きを止める
        StartCoroutine("DamagePause");
    }

    //  敵がハンマーで叩かれたときに呼ばれる
    IEnumerator DamagePause()
    {
        bj = GetComponent<BreakObject>();

        //  動きを止める
        nowAttackPower = 0;

        yield return new WaitForSeconds(0.6f);

        //  アニメ付きでオブジェクトを叩いて潰れたように見せる
        iTween.ScaleBy(this.gameObject, iTween.Hash("y", 0.3f, "time", 0.5f));

        //  hpが残っていれば元の状態に戻す
        if (bj.hp > 0)
        {
            yield return new WaitForSeconds(0.5f);

            iTween.ScaleTo(this.gameObject, iTween.Hash("y", scaleY, "time", 1.0f));

            //  現在値に初期値を戻す
            nowAttackPower = attackPower;

            gameObject.layer = LayerMask.NameToLayer("Enemy");
        }
    }
}
