﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WatchKeeper : MonoBehaviour{
    [SerializeField] Transform target;
    Animator anim;
    Rigidbody rb;
    AppearEffect appearEffect;        //  PC発見時のエフェクト

    bool isCameraSetOn;               //  一時停止。カメラの移動にかかわるときにオン／オフのフラグ
    bool isFind;                      //  索敵用フラグ
    bool isWalked;                    //  歩いたフラグ
    bool isLoitered;                  //  見張りのフラグ

    float stateCount;                 //  待機時間のカウンター
    protected float scaleY;           //  スケールYの元の値
    public int facing;                //  向きの確認用
    int randomWait;                   //  待機時間のランダム設定値
    public int maxAttackCount;        //  攻撃回数
    public float moveSpeed;           //  左右への移動速度
    public int attackPower;           //  攻撃力
    float nowMoveSpeed;               //  現在の移動速度
    protected int nowAttackPower;     //  現在の攻撃力

    public EnemyState nowEnemyState;  //  現在の敵の状態
    public enum EnemyState            //  敵の状態の遷移用
    {
        idle,    //  待機
        walk,
        run,
        loiter,  //  見回す
        attack,
    }

    // Use this for initialization
    void Start(){
        nowAttackPower = attackPower;                 //  攻撃力を入れる
        scaleY = this.transform.localScale.y;         //  小さくなった時に元に戻る値を入れておく

        rb = GetComponent<Rigidbody>();               //  各コンポーネント取得
        anim = GetComponent<Animator>();
        appearEffect = GetComponent<AppearEffect>();

        isCameraSetOn = ZoomOut.cameraSetOn;          //  オンの時は一時停止。カメラの移動にかかわるときにオン／オフ
        randomWait = Random.Range(3, 6);              //  スタート時に各アクションの待機時間をランダムに設定する
        nowEnemyState = EnemyState.idle;            //  待機状態にする
    }

    // Update is called once per frame
    void Update(){
        isCameraSetOn = ZoomOut.cameraSetOn;          //  動作無効のフラグ監視

        if (isCameraSetOn == false){                  //  カメラ移動中は操作無効
            return;
        }

        if (isCameraSetOn == true){                   //  カメラが定位置に戻ったら操作開始
            //  移動する（歩くか走る状態になるとnowMoveSpeedに数値が入り移動する）
            rb.velocity = new Vector3(nowMoveSpeed, rb.velocity.y, 0);

            if (nowEnemyState == EnemyState.idle) { //  待機状態なら次の行動を決定する
                stateCount += Time.deltaTime;      //  待機状態を解除するまでのカウントを開始
                                                   //Debug.Log(stateCount);

                if (stateCount > randomWait)
                {      //  待機状態を解除するカウントになったら
                    if (!isWalked){   //  行動していないなら   
                        OnWalk();                  //  歩く
                        Debug.Log("1");
                    }
                    if (!isWalked && isLoitered){
                        OnWalk();                  //  歩く
                        Debug.Log("2");
                    }
                    Debug.Log("Next");
                }
            }
        }
    }
       
    void OnWalk(){                            //  歩く
        anim.SetBool("Loiter", false);         //  見張りモーション停止
        nowEnemyState = EnemyState.walk;      //　歩く状態に変更

        if (facing == 0){                     //  現在の向きに合わせて移動速度を取得
            nowMoveSpeed = moveSpeed * -1.0f;
        }else{ 
            nowMoveSpeed = moveSpeed * 1.0f;
        }
        anim.SetFloat("Speed", 0.8f);          //  歩くアニメを再生

        StartCoroutine("GetIdle");            //  コルーチン呼び出し
    }

    IEnumerator GetIdle(){                         //  idle状態に戻す
        yield return new WaitForSeconds(1.0f);

        if (nowMoveSpeed != 0){                    //  移動していたなら
            yield return new WaitForSeconds(1.7f);                            
            anim.SetFloat("Speed", 0.0f);          //  走るアニメを終了

            yield return new WaitForSeconds(0.3f);
            nowMoveSpeed = 0;                      //  移動速度を0に戻す
        }
        isWalked = true;                       //  歩いたフラグを立てる
        Debug.Log(isWalked);
        OnLoiter();                                //  見張る
    }

    void OnLoiter(){                          //  見張る
        nowEnemyState = EnemyState.loiter;    //  見張り状態に変更
        anim.SetBool("Loiter", true);         //  見張りモーション再生
        
        StartCoroutine("NotFind");            //  見張り用のコルーチン呼び出し
    }

    IEnumerator NotFind(){                    //  見張り用のコルーチン。見失った場合と見張る場合に呼ばれる
        randomWait = Random.Range(3, 6);
        yield return new WaitForSeconds(randomWait);        
        isLoitered = true;
        isFind = false;                        //  見失う処理に移るフラグ
        if(isWalked && isLoitered){
            OnTurnFace();
        }
    }

    void OnTurnFace(){                         //  向きを変える
        if (facing == 0){                      //  現在の向きの状態に合わせて変更する
            //  向きを90度変えて、移動の方向を変えて、向きを変えたフラグを立てる
            transform.rotation = Quaternion.AngleAxis(90, new Vector3(0, 1, 0));
            nowMoveSpeed *= -1;
            facing = 1;
        }else{
            //  向きを-90度変えて、移動の方向を変えて、向きを変えたフラグを立てる
            transform.rotation = Quaternion.AngleAxis(-90, new Vector3(0, 1, 0));
            nowMoveSpeed *= -1;
            facing = 0;
        }
        isWalked = false;                     //  フラグを降ろす
        isLoitered = false;

        stateCount = 0;                        //  待機カウントをリセットして待機時間をランダムに設定
        randomWait = Random.Range(3, 6);
        nowEnemyState = EnemyState.idle;       //  待機状態に変更
        Debug.Log(isFind);
    }

    void OnAttack(){
        //if(maxAttackCount < 2)
        //{
            if (nowEnemyState != EnemyState.attack){
                //  現在のアニメの再生を止める
                anim.SetFloat("Speed", 0.0f);
                anim.SetBool("Loiter", false);

                nowEnemyState = EnemyState.attack;
                rb.velocity = new Vector3(nowMoveSpeed * 1.5f, rb.velocity.y, 0);
                //anim.SetFloat("Speed",1.2f);

                StartCoroutine("AttackAction");
                //maxAttackCount ++;
            }
            //Debug.Log(maxAttackCount);
        //}
        //else
        //{
            //  ２回まで攻撃したら、そこで一旦見失う
            //StartCoroutine("NotFind");
        //}
    }

    IEnumerator AttackAction(){
        yield return new WaitForSeconds(1.0f);
        anim.SetTrigger("Attack");

        yield return new WaitForSeconds(1.0f);
        anim.SetTrigger("Attack");

        if (nowMoveSpeed != 0){                    //  移動していたなら
            anim.SetFloat("Speed", 0.0f);          //  走るアニメを終了
            nowMoveSpeed = 0;                      //  移動速度を0に戻す
        }        
        StartCoroutine("NotFind");
    }

    //  他のトリガーに接触している間だけ判定
    private void OnTriggerStay(Collider col){
        //  もしも他のオブジェクトにPlayerというタグがついていたなら
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge") {        
            if (!isFind) { 
                //  rootを使うと最上位の親の情報を取得できる
                //  LookAtメソッドは指定した方向にオブジェクトの向きを回転させる
                transform.root.LookAt(target);

                //  PCを発見したエフェクトと攻撃のメソッドを呼び出し
                appearEffect.FindOut();
                OnAttack();

                isFind = true;
            }
            //Attack();

            //isFind = true;
            //Debug.Log("Attack!");
        }
    }

    private void OnTriggerExit(Collider col){　　　　　//  トリガーの対象がいなくなったら
        if (isFind){                                   //  索敵に成功していてPCがいない場合
            if (col.gameObject.tag == "Player" || col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge") { 
                OnLoiter();                            //  見張る                
            }
        }
    }
    
}
