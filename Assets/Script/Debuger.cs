﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debuger : MonoBehaviour {

    public int exp;

    XpCounter xpScript;                       //  ExpTextのscriptを格納

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            //  expを渡すscriptを格納
            xpScript = GameObject.Find("ExpText").GetComponent<XpCounter>();

            //  expを渡す
            xpScript.AddXp(exp);
        }
    }
}
