﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearBonus : MonoBehaviour {

    public int bonusPoint;                         //  エッグガチャの回数

    GetEgg getEgg;
    Loading loading;

    [SerializeField]
    TextMesh countText;                            //  エッグガチャの残り回数を表示する

    //  EggManager.csから呼ばれるメソッド
    public void GetBonusPoint(int getCount)
    {
        //  エッグガチャの回数を取得する
        bonusPoint = getCount;

        //  残り回数のカウントを表示する
        countText.text = bonusPoint.ToString();

        getEgg = GetComponent<GetEgg>();
    }

    //  オーブとハンマーの当たり判定
    private void OnTriggerEnter(Collider col)
    {
        //  エッグガチャ回数が残っているなら
        if (bonusPoint > 0)
        {
            //  叩いたらメソッドを呼び出す
            if (col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
            {
                //  メソッドを呼び出して、残り回数を減らす
                EggTrigger();
                bonusPoint--;

                //  残り回数のカウントを表示する
                countText.text = bonusPoint.ToString();
                //Debug.Log(bonusPoint);
            }
        }
    }

    //  オーブを叩くと呼ばれるメソッド
    void EggTrigger()
    {
        //  アニメ付きでオブジェクトを揺らす
        iTween.ShakePosition(gameObject, iTween.Hash("z", 0.5f, "time", 1.0f));

        //  メソッド呼び出し
        getEgg.SetBonus();
    }
}
