﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rader : MonoBehaviour {

    [SerializeField]
    Transform target;

    ShotShell shotShell;
    AppearEffect appearEffect;

    public static bool isShot;

    //  他のトリガーに接触している間だけ判定
    private void OnTriggerStay(Collider col)
    {
        //  弾を発射していないなら
        if (!isShot)
        {
            appearEffect = GetComponent<AppearEffect>();

            //  もしも他のオブジェクトにPlayerというタグがついていたなら
            if (col.gameObject.tag == "Player")
            {
                //  rootを使うと最上位の親の情報を取得できる
                //  LookAtメソッドは指定した方向にオブジェクトの向きを回転させる
                transform.root.LookAt(target);

                isShot = true;

                //  PCを発見したエフェクトの呼び出し
                appearEffect.FindOut();

                //  弾を発射させるメソッドの呼び出し
                shotShell = GetComponent<ShotShell>();
                shotShell.Shot();
                Debug.Log("Shot!");
            }
        }
    }
}
