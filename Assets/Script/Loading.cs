﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour {

    [SerializeField] private GameObject loadUI;           //  シーンロード中に表示するUI画面
    [SerializeField] private Slider slider;               //  Load時間表示用。SliderのValueをスクリプトで操作するため
    [SerializeField] private ImageLoading imageLoading;

    public AsyncOperation async1;       //  非同期動作で使用するためのAsyncOperation用のフラグ　ステージ１用
    public static AsyncOperation async3;       //  ステージ２用
    private AsyncOperation async2;             //  async1,3は次のシーンでも使うため引き継ぐ    
    public bool isTitle;                       //  タイトル画面の確認フラグ

    /// <summary>
    /// ゲーム実行と同時にステージ１を先読み。
    /// IEnumerator async1にメソッドをいれ、yield returnでメソッドの戻り値をコルーチンからもらう
    /// </summary>
    /// <returns></returns>
    private IEnumerator Start() {
        if (isTitle) {     // async1の戻り値はプロパティとして[async1.allowSceneActivation = false]で戻ってくる 
                           //IEnumerator async1 = LoadScene();
            IEnumerator async3 = LoadScene();
            yield return StartCoroutine(async3);
            isTitle = false;
        }
    }

    /// <summary>
    /// GameSceneを先読みする。ゲーム開始時に呼ばれる。
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoadScene() {
        //async1 = SceneManager.LoadSceneAsync("GameScene1", LoadSceneMode.Single);
        async3 = SceneManager.LoadSceneAsync("GameScene3", LoadSceneMode.Single);

        // シーンの遷移は行わないで待つ（この位置にしないとTrueにならない）
        //async1.allowSceneActivation = false;
        async3.allowSceneActivation = false;

        // 読み込みが終わるまで進捗状況をスライダーの値に反映させる
        while (!async3.isDone) {
            // async.progressでfloat型の0-1の値を得られる
            // Mathf.Clamp01を使ってasync.progressが0-1の間になるように補正する
            // 0.9fにしないとisDoneがtrueにならない
            var progressVal = Mathf.Clamp01(async3.progress / 0.9f);

            // Sliderの値はデフォルトでは0 - 1になっているので、そのまま入れる
            // SliderのValueコンポーネントをprogressVal分だけ動かす
            slider.value = progressVal;

            yield return null;
        }
        // シーンの読み込みが終わるまで待つ
        yield return async3;
    }


    /// <summary>
    /// シーン遷移の際のロード画面の呼び出し
    /// </summary>
    public void NextScene(bool isSelectSceneFlg = false) {     
        loadUI.SetActive(true);                      // ロード画面UIをアクティブにする            
        imageLoading.ShowImage();                    // ロード画面に表示する画像をランダムで選択するメソッドの呼び出し
        if (isSelectSceneFlg) {
            SceneManager.LoadScene("GameScene2");    // フラグがあるならステージセレクトシーンを呼び出す
        }
    }

    /// <summary>
    /// タイトル画面でGameStartボタンを押したら先読みのシーンをロードする。
    //  ButtonUIに割り当ててある
    /// </summary>
    public void OnClickStartBtn() {
        NextScene();       
        async3.allowSceneActivation = true;        // 先読みしているGameSceneをtrueにして遷移させる
        SceneManager.LoadScene("GameScene3");
    }

    /// <summary>
    /// 未使用
    /// </summary>
    /// <returns></returns>
    IEnumerator LoadData2()
    {
        //  GameScene2シーンの読み込みをする
        async2 = SceneManager.LoadSceneAsync("GameScene2", LoadSceneMode.Single);

        //  両方にEventSystemが存在する場合は、ここでcurrent = nullしておくことで、GameScene2にEventSystemに処理を移すことができる。
        //UnityEngine.EventSystems.EventSystem.current = null;

        //  シーンの遷移は行わないで待つ（この位置にしないとTrueにならない）
        //async2.allowSceneActivation = false;

        //  読み込みが終わるまで進捗状況をスライダーの値に反映させる
        while (!async2.isDone)
        {
            //  async.progressでfloat型の0-1の値を得られる
            //  Mathf.Clamp01を使ってasync.progressが0-1の間になるように補正する
            //  0.9fにしないとisDoneがtrueにならない
            var progressVal = Mathf.Clamp01(async2.progress / 0.9f);

            // Sliderの値はデフォルトでは0 - 1になっているので、そのまま入れる
            //  SliderのValueコンポーネントをprogressVal分だけ動かす
            slider.value = progressVal;
            
            yield return null;
        }

        //  読み込みが終わるまで待機する
        yield return async2;

        SceneManager.SetActiveScene(SceneManager.GetSceneByName("GameScene2"));
    }    
}
