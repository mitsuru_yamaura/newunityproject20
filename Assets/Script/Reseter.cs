﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reseter : MonoBehaviour {

    private void OnTriggerEnter(Collider col)
    {
        //  フラグをリセットする
        PlayerPrefs.DeleteAll();
        Debug.Log("Reset!");
    }
}
