﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemRotate : MonoBehaviour {

    //  private
    ParticleSystem particle;
    AudioSource sound1;
    AudioSource sound2;

    EggManager eggManager;                //  アイテム獲得数をカウント用
    Life lifeScript;                      //  ライフ回復用

    bool isHeartGet;                      //  複数取れてしまう場合の回避フラグ

    public float rotateSpeed;             //  オブジェクトの回転速度
    public int repair;                    //  ライフ回復値

    // Use this for initialization
    void Start () {

        //  回転を開始する角度を設定
        this.transform.Rotate(0, Random.Range(0, 360), 0);
    }
	
	// Update is called once per frame
	void Update () {

        //  回転する速度
        this.transform.Rotate(0, rotateSpeed, 0);
    }

    public void OnTriggerEnter(Collider col)
    {
        AudioSource[] audioSources = GetComponents<AudioSource>();
        sound1 = audioSources[0];
        sound2 = audioSources[1];

        //  HeartScoreオブジェクトからLifeスクリプトを格納
        lifeScript = GameObject.Find("HeartScore").GetComponent<Life>();

        if (col.gameObject.tag == "Player")
        {
            particle = GetComponent<ParticleSystem>();

            //  SE再生
            var randomValue = Random.Range(0, audioSources.Length);

            if (randomValue == 0)
            {
                sound1.Play();
            }
            else
            {
                sound2.Play();
            }

            //  ハートを取った時にフラグを立てる
            if (isHeartGet == false)
            {
                LifeUp();
                particle.Play();

                //  このハートを取ったというフラグ
                isHeartGet = true;   
            }

            //  このオブジェクトを破壊する
            Destroy(this.gameObject, 2.0f);
        }
    }

    //  回復処理を複数回避けるためにメソッド内で回復処理する
    void LifeUp()
    {
        //  回復量
        repair = 1;

        //  ハートの獲得数を渡すscriptを取得
        eggManager = GameObject.Find("ClearOrb").GetComponent<EggManager>();

        //  ハートの獲得数を渡す
        eggManager.Collector(1);
        
        //  Life.cs内のRecoveryLifeメソッドを呼び出して回復処理する
        lifeScript.RecoveryLife(repair);
        Debug.Log(repair);
    }
}
