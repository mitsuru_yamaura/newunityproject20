﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerLife : MonoBehaviour {

    [SerializeField]
    MaxLife maxLife;

    [SerializeField]
    UnitychanController uCont;

    [SerializeField]
    XpCounter xpCont;                         //  XPの減算処理参照用 

    [SerializeField]
    ZoomOut zoom;                             //  ズームイン処理用

    [SerializeField]
    ParticleSystem particle1;                  //  レベルアップ時の演出

    [SerializeField]
    ParticleSystem particle2;                  //  レベルアップ時の演出

    string initTag;                           //  元のTagの情報を入れておく

    int nowLifeLevel;                          //  現在のレベルの取得用
    int nextLifeXp = 2000;                     //  LifeのレベルアップのためのXP基礎値

    int nowNextXp;                             //  次のレベルに必要なXP
    int nowXpCount;                            //  xpの現在値。xpが足りていて取得可能かどうかの確認用に使用

    bool isLifeUp;

    AudioSource sound1;                          //  SE  1-2(GameScene1,2で異なる)
    AudioSource sound2;
    AudioSource sound3;                       //  SE  レベルアップ時の効果音1
    AudioSource sound4;                       //  SE  レベルアップ時のparticleに合わせたSE

    // Use this for initialization
    void Start()
    {
        //  現在のレベルを取得する
        nowLifeLevel = UnitychanController.maxLife;
        nowLifeLevel -= 5;
        Debug.Log(nowLifeLevel);

        //  現在のレベルに合わせてライフフラワーを大きくしておく（成長度合いを反映するため）
        for (int i = 0; i < nowLifeLevel; i++)
        {
            this.transform.localScale = new Vector3((0.2f * (5 + nowLifeLevel -1)) * (0.2f * (5 + nowLifeLevel)) , (0.2f * (5 + nowLifeLevel - 1)) * (0.2f * (5 + nowLifeLevel)), (0.2f * (5 + nowLifeLevel - 1)) * (0.2f * (5 + nowLifeLevel)));
        }        
    }


    //  ライフの最大値を上昇させるメソッド
    private void OnTriggerEnter(Collider col)
    {
        //  Heartか宝箱に触ったら２個取れないようにLayerを変えて制御する
        if (col.gameObject.tag == "Weapon")
        {
            if (!isLifeUp)
            {
                isLifeUp = true;
                AddLifes();
            }
        }
    }

    private void AddLifes()
    {
        //  Lifeが10なら成長しない
        if (UnitychanController.maxLife == 10)
        {
            return;
        }

        //  xpの現在値を取得し、獲得可能か確認させる
        nowXpCount = XpCounter.xpCount;

        //  現在のレベルを取得する
        nowLifeLevel = UnitychanController.maxLife;

        //  次のレベルに必要なXPを計算する
        nowNextXp = nextLifeXp * (nowLifeLevel + 1) - 10000;
        Debug.Log(nowNextXp);

        //  XPの現在値が次のレベルアップに必要なXP以上なら
        if (nowXpCount >= nowNextXp)
        {
            //  呼びたいメソッドを入れる
            maxLife.LevelUpMaxLife();

            //  nowNextXPを引数にして、XpCounterのSubtractXp(減算)メソッドを呼び出す
            xpCont.SubtractXp(nowNextXp);

            //  レベルツリーの位置情報をposで取得しカメラ制御のメソッドの呼び出し
            //  ZoomOut.cs内のTreeGrowメソッドに引数として渡す
            Vector3 pos = this.transform.position;
            pos.x -= 2;
            zoom.ZoomIn(pos, "powerup");

            //  演出を再生
            particle1.Play();

            AudioSource[] audioSources = GetComponents<AudioSource>();
            sound4 = audioSources[3];

            sound4.Play();

            //  アニメ付きでオブジェクトを揺らす
            iTween.ShakePosition(this.gameObject, iTween.Hash("z", 0.5f, "time", 2.0f));

            //  インフォメーションテキストを非表示にし、２回叩けないようにUnitychanのタグを変える
            initTag = uCont.gameObject.tag;

            //  タグを切り替える
            uCont.gameObject.tag = "LevelUp";

            
            //  ライフ現在値を取得する
            nowLifeLevel = UnitychanController.maxLife;
            Debug.Log(nowLifeLevel);

            //  フラワーをアニメ再生で大きくする
            iTween.ScaleTo(this.gameObject, iTween.Hash("x", 0.2f * (nowLifeLevel - 1) * 0.2f * nowLifeLevel, "y", 0.2f * (nowLifeLevel - 1) * 0.2f * nowLifeLevel, "z", 0.2f * (nowLifeLevel - 1) * 0.2f * nowLifeLevel, "time", 1.5f));
        }
    }


    //  ライフレベルアップ時のタグを元に戻すコルーチン
    IEnumerator TagBack()
    {
        AudioSource[] audioSources = GetComponents<AudioSource>();
        sound1 = audioSources[0];
        sound2 = audioSources[1];
        sound3 = audioSources[2];

        yield return new WaitForSeconds(1.7f);

        //  SE再生
        sound3.Play();

        yield return new WaitForSeconds(0.3f);

        //  音声SEを再生する
        //  ランダムで変える
        var randomValue = Random.Range(0, 2);

        if (randomValue == 0)
        {
            sound1.Play();
        }
        else
        {
            sound2.Play();
        }

        particle2.Play();
       
        //  元のタグに戻す
        uCont.gameObject.tag = initTag;
        isLifeUp = false;
    }
}
