﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeSwitch : MonoBehaviour
{
    [SerializeField]
    LevelUp levelUp;   //  成長させるツリーの参照用

    bool isLevelUp;    //  連続で当たり判定が発生するのを防ぐ

    //  TreeのColliderは大きさが変わってしまうため、このswitch用オブジェクトを専用に使う
    //  レベルアップ用の当たり判定
    void OnTriggerEnter(Collider col)
    {
        //  Weaponタグに接触した時（ハンマーの振り下ろす時からWeaponタグになる）
        if (col.gameObject.tag == "Weapon")
        {
            //  成長判定中でないなら
            if (!isLevelUp)
            {
                //  成長判定を立てる
                isLevelUp = true;
                //Debug.Log(isLevelUp);

                //  レベルアップ用のメソッドの呼び出し
                levelUp.ForceLevelUp();
                StartCoroutine("WaitTime");
            }
        }
    }

    //  連続判定を防ぐための待機時間
    IEnumerator WaitTime()
    {
        yield return new WaitForSeconds(1.5f);

        //  成長判定をおろす
        isLevelUp = false;
        //Debug.Log(isLevelUp);
    }
}
