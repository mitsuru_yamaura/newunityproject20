﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateSwitch : MonoBehaviour {

    [SerializeField]
    GameObject gate;

    [SerializeField]
    TextMesh tMesh;

    [SerializeField]
    SpriteRenderer sRen;       //  背景画表示用のコンポーネントをアサイン

    [SerializeField]
    GameObject textBack;       //  背景表示用のオブジェクトをアサイン。iTween用

    [SerializeField]
    EggManager egg;

    Vector3 scalePos;　　　　　　　   //  元の大きさの保存用
    bool isRend;                      //  描画用のフラグ

    int clearCount;                   //  現在のステージのクリアカウントを取得用

    
    void Start()
    {
        //  拡大・縮小表示する描画用オブジェクトのScale値を取得する
        scalePos = textBack.transform.localScale;
    }

    private void OnTriggerStay(Collider col)
    {       
        if (!isRend)
        {            
            //  プレイヤーがswitchを踏んだらオン
            if (col.gameObject.tag == "Player" || col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
            {
                //  EggManager.csより現在のステージのクリア回数を取得する
                clearCount = egg.nowClearCount;
                Debug.Log(clearCount);

                //  ステージを一回以上クリアしていれば非表示にして通行可能にする
                if (clearCount > 0)
                {
                    gate.SetActive(false);
                    gameObject.SetActive(false);
                }

                //  textと画像を表示する
                sRen.enabled = true;

                //  拡大・縮小表示する描画用オブジェクトのScale値を最小値にする
                textBack.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

                //  元の大きさまでアニメで表示
                iTween.ScaleTo(textBack, iTween.Hash("x", scalePos.x, "y", scalePos.y, "z", scalePos.z, "time", 1.0f));
                
                tMesh.text = "ステージを１回以上クリアすると\n通行可能になります。";
            }
            //  描画フラグを立てる
            isRend = true;
        }
    }

    //  プレイヤーがswitchから離れたら非表示にするメソッド
    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
        {
            //  テキストを空白に、背景の描画を非表示にする。描画フラグを降ろす
            tMesh.text = " ";

            if (isRend)
            {
                //  textとimageをアニメで非表示
                iTween.ScaleTo(textBack, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.5f));

                //  描画フラグを降ろす
                isRend = false;
            }
        }
    }
}
