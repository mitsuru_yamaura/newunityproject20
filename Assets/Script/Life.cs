﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Life : MonoBehaviour {

    private int initLife;                                        //  最大Life数。Unityちゃんから取得する
    public int currentLife = 3;                                 //  現在のLife数   
    private int fadeLife = 2;                                    //  mask分   

    private GameObject[] lifeObject;                             //  インスタンスを格納するLifeオブジェクト配列
    private GameObject[] maskObject;                             //  格納するMaskオブジェクト配列

    private Vector3 lifePosition = new Vector3(30f, 380f, 0);    //　Lifeの初期座標

    public GameObject lifePrefab;                                //  Lifeのプレハブ
    public GameObject maskPrefab;                                //  ターゲットである暗転処理をするプレファブ

    public XpCounter xCont;                                      //  スクリプト
    UnitychanController uni;
    EggManager eggManager;                                       //  ライフMax時の獲得XPを送る

    int exp;                                                     //  LifeMax時のボーナスexp

    //  現在のLife数を代入して、すべてのLifeを無効化し、現在のLife数分を有効化する
    //  プロパティ書式
    public int LifePoint
    {
        set
        {
            //  現在のLife数を代入
            currentLife = value;
            fadeLife = initLife - currentLife;

            //  すべてのLifeを無効化
            for(int i = 0; i < initLife; i++)
            {
                lifeObject[i].SetActive(false);                
                //Debug.Log(i);
            }

            //  すべてのマスクを無効化
            for (int i = 0; i < initLife; i++)
            {
                maskObject[i].SetActive(false);
                //Debug.Log(i);
            }
            
            //  現在のLife数分有効化
            for (int i = 0;i < currentLife; i++)
            {
                lifeObject[i].SetActive(true);
                //Debug.Log(i);
            }

            //  現在のMaskLife数分有効化
            for (int i = 0; i < fadeLife; i++)
            {
                maskObject[i].SetActive(true);
                //Debug.Log(i);
            }         
        }
        get
        {
            return currentLife;
        }
    }


    // Use this for initialization
    public void Start()
    {
        //  //  UnityちゃんのLife最大値を取得する
        initLife = UnitychanController.maxLife;
        //Debug.Log(initLife);

        //  ゲームオブジェクトの配列を生成
        //  ここでそれぞれの最大値を設定しておく
        lifeObject = new GameObject[initLife];
        maskObject = new GameObject[initLife];

        //  初期値分のLife生成
        //  ここでは最大値まで生成しておく
        for (int i = 0; i < initLife; i++)
        {
            //  ハートのインスタンス生成
            lifeObject[i] = Instantiate(lifePrefab, lifePosition, Quaternion.identity) as GameObject;

            //  子供にする
            lifeObject[i].transform.SetParent(gameObject.transform, true);

            //  位置座標と画像の大きさを設定
            lifeObject[i].GetComponent<RectTransform>().sizeDelta = new Vector3(1, 1, 1);
        }

        //  初期値分のマスク生成
        //  こちらもここでは最大値まで生成しておく
        for (int j = 0; j < initLife; j++)
        {
            //  マスクのインスタンス生成
            maskObject[j] = Instantiate(maskPrefab, lifePosition, Quaternion.identity) as GameObject;

            //  子供にする
            maskObject[j].transform.SetParent(gameObject.transform, true);

            //  位置座標と画像の大きさを設定
            maskObject[j].GetComponent<RectTransform>().sizeDelta = new Vector3(.01f, 1.0f, 1.0f);
        } 

        //  スタート時に所持・画面に表示するライフ数
        //  ここで設定しないと多い数になる
        //  上記の生成時にこの値にしてしまうとリセットするときに配列が足りなくて止まる
        currentLife = 3;
        fadeLife = initLife - currentLife;

        //  現在のLifeを代入してプロパティ実行
        this.LifePoint = currentLife;
    }
	
    //  Lfe減算メソッド
    public void ApplyDamage(int nowAttackPower)
    {
        //Debug.Log(damage);

        //  もしも現在のLifeが0以上あるなら
        if (currentLife > 0){

            //  ダメージが現在値よりも大きくなるなら（マイナス）
            if (nowAttackPower > currentLife)
            {
                currentLife = 0;
                fadeLife = initLife;
            }
            else
            {
                //  ダメージ分だけライフの現在値を減算し、マスク分を加算する
                currentLife -= nowAttackPower;
                fadeLife += nowAttackPower;
            }
            
            //Debug.Log(currentLife);
            //Debug.Log(fadeLife);
           
            //  もしも現在のLifeが0以下になったなら
            if (currentLife <= 0)
            {
                //  ゲームオーバー処理
                uni = GameObject.Find("Kohaku").GetComponent<UnitychanController>();
                
                //  操作を無効にするメソッドを呼び出す
                uni.Lose();
            }

            //  現在のLifeを代入してプロパティ実行
            this.LifePoint = currentLife;
        }
    }

    //  Life加算メソッド
    public void RecoveryLife(int repair)
    {
        //Debug.Log(repair);

        if (currentLife == initLife)
        {
            //  LifeMaxの数に応じてボーナスexp
            exp = initLife * 50;

            //  LifeMaxのボーナスexpを引数にして渡す
            xCont.AddXp(exp);

            //  クリア時用のXPと破壊カウントを渡すscriptを格納
            eggManager = GameObject.Find("ClearOrb").GetComponent<EggManager>();

            //  xpを渡す
            eggManager.Breaker(exp);
            Debug.Log(exp);
        }

        //  もしも現在のLifeが初期(最大)Life未満なら
        if (currentLife < initLife)
        {
            //  まずMask分のライフ表示を現在のfadeLifeで破棄する
            //this.LossPoint = fadeLife;

            //  現在値を加算し、マスク値を減算する
            currentLife += repair;
            fadeLife -= repair;

            //  現在値（currentLife）が最大値(initLife)を超えたら、最大値にする。
            if (currentLife > initLife)
            {
                //  LifeMaxの数に応じてボーナスexp
                exp = initLife * 30;

                //  LifeMaxのボーナスexpを引数にして渡す
                xCont.AddXp(exp);
                Debug.Log(exp);

                //  現在値を最大値にする
                currentLife = initLife;                
            }

            //Debug.Log(currentLife);
            //Debug.Log(fadeLife);

            //  現在のLifeを代入し、プロパティ実行
            this.LifePoint = currentLife;
        }
    }
}
