﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour {

    [SerializeField] Canvas gameOverCanvas;    //  GameOverのキャンバス
    [SerializeField] Text stageSelectText;     //  ステージセレクトのボタン
    [SerializeField] Text exitText;            //  ゲーム終了のボタン

    ExitDialog exit;                           //  操作無効フラグの受け渡し用
    Loading loading;                           //  ステージセレクトへ戻る参照用

    AudioSource sound1;
    AudioSource sound2;
 
    public bool isLose;                        //  GameOverフラグ。Pause画面を出さないようにする

    // Use this for initialization
    void Start()
    {
        //  GameOverキャンバスを非表示にする
        gameOverCanvas.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {        
        if (gameOverCanvas.enabled)　　　　//　ゲームオーバーキャンバスが開いているならボタンでも操作可能
        {
            if (Input.GetButtonDown("Jump")) //  PS4 = 〇
            {
                Restart();                 //  ステージセレクト遷移のメソッド
            }
            if (Input.GetButtonDown("Run"))  //  PS4 = ×
            {
                exit = GetComponent<ExitDialog>();
                exit.OnCallExit();         //  ゲーム終了のメソッド
            }
        }
    }

    //  UnitychanController.csより呼ばれるメソッド
    public void LoseSelecsion()
    {
        //  GameOverの表示
        gameOverCanvas.enabled = true;

        //  日本語設定なら
        if (LanguageChoice.languageNum == 0)
        {
            stageSelectText.text = "ステージセレクトに戻る";
            exitText.text = "ゲームを終了する";
        }
        else
        {
            stageSelectText.text = "Return to stage selection Scene.";
            exitText.text = "Quit game.";
        }

        isLose = true;

        //  スクリプトを取得し、フラグを立てる
        exit = GetComponent<ExitDialog>();
        exit.isInActive = true;

        //  BGMを止めて、ゲームーオーバーのジングルを再生する
        AudioSource[] audioSources = GameObject.Find("BGM").GetComponents<AudioSource>();
        sound1 = audioSources[0];
        sound2 = audioSources[1];

        sound1.Stop();
        sound2.Play();
    }

    //  [ステージセレクトへ戻る]ボタンから呼ばれるメソッド
    public void Restart()
    {
        gameOverCanvas.enabled = false;                                       //  GameOverキャンバスを非表示にする
        loading = GameObject.Find("SceneLoader").GetComponent<Loading>();     //  Loading.NextSceneメソッドの呼び出し
        loading.NextScene();
    }
}
