﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NonDamageAreaSwitch : MonoBehaviour {

    [SerializeField] GameObject [] damageAreas;     //  ダメージエリアの配列

    public bool isSwitchOn;                         //  ダメージエリア表示のオンオフスイッチ
    AudioSource sound1;                             //  水音のSE

	// Use this for initialization
	void Start ()
    {
        //  フラグが立っているか確認して、立っているならダメージエリアを非表示にする
        if (isSwitchOn)
        {
            for (int i = 0; i < damageAreas.Length; i ++)
            {
                damageAreas[i].SetActive(false);    
            }
        }
	}

    private void OnTriggerEnter(Collider col)
    {
        //  フラグがないなら
        if (!isSwitchOn)
        {
            if (col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
            {
                //  フラグをオンにし、SEを再生してダメージエリアを非表示にする
                isSwitchOn = true;
                AudioSource [] audioSources = GetComponents<AudioSource>();
                sound1 = audioSources[0];
                sound1.Play();
                sound1.DOFade(0f, 2f).SetEase(Ease.Linear);       //  SEフェードアウト
                StartCoroutine("WaterSe");

                for (int i = 0; i < damageAreas.Length; i++)
                {
                    damageAreas[i].SetActive(false);
                }
            }
        }
    }

    IEnumerator WaterSe()
    {
        yield return new WaitForSeconds(2.0f);
        sound1.Stop();
    }
}
