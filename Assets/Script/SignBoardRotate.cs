﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignBoardRotate : MonoBehaviour {

    public float rotateSpeed;             //  オブジェクトの回転速度

    // Use this for initialization
    void Start () {

        //  回転を開始する角度を設定
        this.transform.Rotate(0, Random.Range(0, 360), 0);
    }
	
	// Update is called once per frame
	void Update () {

        //  回転する速度
        this.transform.Rotate(0, rotateSpeed, 0);
    }
}
