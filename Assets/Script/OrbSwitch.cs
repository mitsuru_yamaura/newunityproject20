﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbSwitch : MonoBehaviour {

    [SerializeField]
    GameObject textBack;       //  背景表示用のオブジェクトをアサイン。iTween用

    [SerializeField]
    SpriteRenderer sRen;       //  背景画表示用のコンポーネントをアサイン

    [SerializeField]
    TextMesh tMesh;            //  Text

    Vector3 scalePos;　　　　　　　   //  元の大きさの保存用
    bool isRend;                      //  描画用のフラグ


    // Use this for initialization
    void Start()
    {
        //  拡大・縮小表示する描画用オブジェクトのScale値を取得する
        scalePos = textBack.transform.localScale;
    }

    private void OnTriggerStay(Collider col)
    {
        if (!isRend)
        {
            //  プレイヤーがswitchを踏んだらオン
            if (col.gameObject.tag == "Player" || col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
            {
                //  textと画像を表示する
                sRen.enabled = true;

                //  拡大・縮小表示する描画用オブジェクトのScale値を最小値にする
                textBack.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

                //  元の大きさまでアニメで表示
                iTween.ScaleTo(textBack, iTween.Hash("x", scalePos.x, "y", scalePos.y, "z", scalePos.z, "time", 1.0f));

                tMesh.text = "-Bonus Charenge!!-\n\n\nオーブに表示されている回数だけ\nボーナスとしてXpを獲得することができます。\n\nボーナスのランクは1～5まであります。";
            }
            //  描画フラグを立てる
            isRend = true;

            StartCoroutine("Inactive");
        }
    }

    //  プレイヤーがswitchから離れたら非表示にするメソッド
    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
        {
            //  テキストを空白に、背景の描画を非表示にする。描画フラグを降ろす
            tMesh.text = " ";

            if (isRend)
            {
                //  textとimageをアニメで非表示
                iTween.ScaleTo(textBack, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.5f));

                //  描画フラグを降ろす
                isRend = false;
            }
        }
    }

    IEnumerator Inactive()
    {
        yield return new WaitForSeconds(2.0f);

        //  テキストを空白に、背景の描画を非表示にする。描画フラグを降ろす
        tMesh.text = " ";

        if (isRend)
        {
            //  textとimageをアニメで非表示
            iTween.ScaleTo(textBack, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.5f));
        }
    }
}
