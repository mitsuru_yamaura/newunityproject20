﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillViewSwitch : MonoBehaviour {

    public TextMesh tMesh;            //  text表示用のコンポーネントをアサイン  
    public SpriteRenderer sRen;       //  背景画表示用のコンポーネントをアサイン
    public GameObject stageTextBack;  //  背景表示用のオブジェクトをアサイン。iTween用

    Vector3 scalePos;　　　　　　　   //  元の大きさの保存用
    bool isRend;                      //  描画用のフラグ

    [SerializeField] PowerUpBox skillBox;

    int nowSkillNo;                   //  スキル番号取得用
    int nowSkillXp;                   //  スキルXP取得用

    // Use this for initialization
    void Start ()
    {
        //  拡大・縮小表示する描画用オブジェクトのScale値を取得する
        scalePos = stageTextBack.transform.localScale;
    }

    //  プレイヤーがswitch内にいる間表示するメソッド
    private void OnTriggerStay(Collider col)
    {
        nowSkillNo = skillBox.powerUpNo;

        if (PowerUpBox.highJump == 1)                  //  すでに取得済のスキルのボックスは表示しない
        {
            if(nowSkillNo == 0)
            {
                return;
            }
        }
        if (PowerUpBox.speedUp == 1)
        {
            if (nowSkillNo == 1)
            {
                return;
            }
        }
        if (PowerUpBox.chargeHammer == 1)
        {
            if (nowSkillNo == 2)
            {
                return;
            }
        }

        if (!isRend)
        {
            //  プレイヤーがswitchを踏んだらオン
            if (col.gameObject.tag == "Player" || col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
            {                
                //  textと画像を表示する
                sRen.enabled = true;

                //  スキル取得に必要なXPを取得する
                nowSkillXp = skillBox.skillXp;

                //  拡大・縮小表示する描画用オブジェクトのScale値を最小値にする
                stageTextBack.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

                //  元の大きさまでアニメで表示
                iTween.ScaleTo(stageTextBack, iTween.Hash("x", scalePos.x, "y", scalePos.y, "z", scalePos.z, "time", 1.0f));

                //  言語で表示テキスト分岐
                if (LanguageChoice.languageNum == 0) {
                    tMesh.text = "-スキルボックス-\n\nハンマーで叩くとXPを消費して、スキルを取得出来ます。\n\nスキル名 : ";
                }
                else
                {
                    tMesh.text = "-Skill Box-\n\nConsuming XP and hit with a hammer,\nyou can get the skills.\n\nSkill Name : ";
                }

                //  各スキルの取得状況を見て、取得していなければ表示
                //  HighJumpのテキストと必要ポイントの表示
                if (PowerUpBox.highJump == 0)
                {
                    //  習得していないなら
                    if (nowSkillNo == 0)
                    {
                        if (LanguageChoice.languageNum == 0)
                        {
                            tMesh.text += "High Jump(ハイジャンプ)\n\n必要なXP : " + nowSkillXp.ToString() + " pt\n\nジャンプボタンを長押して離すことで、\n高くジャンプすることが出来るようになります";
                        }
                        else
                        { 
                            tMesh.text += "High Jump\n\nConsume XP : " + nowSkillXp.ToString() + " pt\n\nBy long pressing and releasing\nthe jump button, you will be able to high jump!";
                        }
                    }
                }
                //  SpeedUpのテキストと必要ポイントの表示
                if (PowerUpBox.speedUp == 0)
                {
                    //  習得していないなら
                    if (nowSkillNo == 1)
                    {
                        if (LanguageChoice.languageNum == 0)
                        {
                            tMesh.text += "Sliding(スライディング)\n\n必要なXP : " + nowSkillXp.ToString() + " pt\n\nハンマーをしまって移動中、またはダッシュ中に\nスライディングボタンを押すとスライディングが出来ます\n空中の床や岩場の上などでスライディングを\nすることで下に降りることが出来ます";
                        }
                        else
                        {
                            tMesh.text += "Sliding\n\nConsume XP : " + nowSkillXp.ToString() + " pt\n\nSliding can be done by disarming the hammer and \npushing the sliding button while moving or dashing. \nYou can get down by sliding\non a floor in the air or on a rocky place.";
                        }
                    }
                }
                //  ChargeHammerのテキストと必要ポイントの表示
                if (PowerUpBox.chargeHammer == 0)
                {
                    //  習得していないなら
                    if (nowSkillNo == 2)
                    {
                        if (LanguageChoice.languageNum == 0)
                        {
                            tMesh.text += "Charge Attack(チャージアタック)\n\n必要なXP : " + nowSkillXp.ToString() + " pt\n\n攻撃ボタンを長押して離すことで\nチャージ攻撃が出来るようになります\n通常攻撃よりも高い攻撃力を持ち、通常攻撃では破壊できない\n一部のオブジェクトも壊せるようになります";
                        }
                        else
                        {
                            tMesh.text += "Charge Attack\n\nConsume XP : " + nowSkillXp.ToString() + " pt\nCharge Attack can be done by long pressing\nand releasing attack button. \nIt will be able to break some objects that\nhave higher attack power than regular attacks \nand can't be destroyed by regular attacks\nsome objects can be destroyed";
                        }
                    }
                }
                //  描画フラグを立てる
                isRend = true;
            }
        }

        //  描画中にレベルアップがあった場合、非表示にする
        if (isRend)
        {
            //  レベルアップ時のタグになったら
            if (col.gameObject.tag == "LevelUp")
            {
                 //  テキストを空白に、背景の描画を非表示にする。描画フラグを降ろす
                tMesh.text = " ";

                //  textとimageをアニメで非表示
                iTween.ScaleTo(stageTextBack, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.5f));
            }
        }
    }

    //  プレイヤーがswitchから離れたら非表示にするメソッド
    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
        {
            //  テキストを空白に、背景の描画を非表示にする。描画フラグを降ろす
            tMesh.text = " ";

            if (isRend)
            {
                //  textとimageをアニメで非表示
                iTween.ScaleTo(stageTextBack, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.5f));

                //  描画フラグを降ろす
                isRend = false;
            }
        }
    }
}
