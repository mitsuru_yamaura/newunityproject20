﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitAnime : MonoBehaviour {

    Animator anim;               //  コンポーネント格納用
    UnitychanController uniCon;

    float waitTime;       //  待機時間計測用
    bool isRest;          //  待機状態のフラグ

    private void Start()
    {
        anim = GetComponent<Animator>();     //  Kohakuのコンポーネント格納
    }

    // Update is called once per frame
    void Update ()
    {
        if(!ZoomOut.cameraSetOn)    //  Cammeraが整うまではカウントストップ
        {
            return;
        }

        //  待機状態フラグが立っておらず
        if (!isRest)
        {
            //  途中でIdel状態でなくなった場合にはカウントをリセットする
            if (!anim.GetCurrentAnimatorStateInfo(0).shortNameHash.Equals(Animator.StringToHash("Idle")))
            {
                waitTime = 0.0f;
                //Debug.Log(waitTime);
            }

            //  Idle状態の場合にカウントアップ。あるいは待機用キーに反応して待機アニメ再生
            if (anim.GetCurrentAnimatorStateInfo(0).shortNameHash.Equals(Animator.StringToHash("Idle"))) 
            {
                waitTime += Time.deltaTime;
                //Debug.Log(waitTime);

                if (Input.GetKeyDown("u"))
                {
                    anim.SetBool("Rest", true);
                    isRest = true;

                    uniCon = GetComponent<UnitychanController>();
                    //  ハンマーを持たない状態にする（所持のフラグオフと同義）
                    uniCon.nowPlayerState = UnitychanController.PlayerState.normal;

                    //  しまう（インスタンスではなく、オブジェクトを非アクティブにする）
                    uniCon.hammer.SetActive(false);

                    StartCoroutine("RestStop");
                }

                if (waitTime > 5.0f)
                {
                    anim.SetBool("Rest", true);
                    isRest = true;

                    uniCon = GetComponent<UnitychanController>();
                    //  ハンマーを持たない状態にする（所持のフラグオフと同義）
                    uniCon.nowPlayerState = UnitychanController.PlayerState.normal;

                    //  しまう（インスタンスではなく、オブジェクトを非アクティブにする）
                    uniCon.hammer.SetActive(false);

                    StartCoroutine("RestStop");
                }
            }
        }
        if (isRest)   //  待機フラグが立ったときにメソッド実行
        {
            DownKeyCheck();           
        }
    }

    void DownKeyCheck()    //  待機キー以外のキー入力があった場合、待機フラグを降ろしカウントをリセット
    {
        if (Input.anyKeyDown)
        {
            if (!Input.GetKeyDown("u"))   //  これをいれないと待機キー自体も反応しなくなる
            {
                anim.SetBool("Rest", false);
                isRest = false;

                waitTime = 0.0f;
                //Debug.Log(waitTime);

                StopAllCoroutines();
                //Debug.Log("stop");
            }
        }
    }

    IEnumerator RestStop()
    {
        //  待機アニメが終わるまで再生してから停止し、フラグを降ろす。カウントをリセットする
        yield return new WaitForSeconds(6.5f);

        anim.SetBool("Rest",false);
        isRest = false;

        waitTime = 0.0f;
        //Debug.Log(waitTime);
    }
}
