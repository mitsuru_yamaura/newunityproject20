﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleMove : MonoBehaviour {

    [SerializeField]
    GameObject kohakuSprite;

	// Use this for initialization
	void Start () {
        
        iTween.MoveTo(kohakuSprite,iTween.Hash("x",-8.0f,"Time",3.5f));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
