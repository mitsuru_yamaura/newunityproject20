﻿using TMPro;                               //  TextMeshProを使用する場合
using UnityEngine;

public class HeartSpawner : MonoBehaviour {

    public GameObject ojPrefab;         //  生成用の各プレファブ指定（配列でも可能）
    public GameObject scrollPrefab;
    public GameObject effectPrefab;
    public GameObject bonusXpPrefab;

    public int heartCreate;             //  オブジェクトの生成率。変更可能
    public int scrollCreate;            //  オブジェクトの生成率。上記と通算

    public int bonusXp;                 //  XpGetに渡す

    GameObject oj;                      //  生成した各プレファブのインスタンス格納用
    GameObject scroll;
    GameObject effect;
    GameObject bonusXpText;

    TextMeshPro tMesh;
    //XpGet scrollScript;

    /// <summary>
    /// アイテムを生成するか抽選する
    /// </summary>
    public void HeartCreate(){
        var randomCreate = Random.Range(0, 100);       // 抽選用の変数をとる       
        Vector3 ojPos = this.transform.position;       // このオブジェクトを基準に生成する位置を設定する

        heartCreate += UnitychanController.heartAddBonus;     // ハートの出現率を確定する
        Debug.Log(heartCreate);        
        scrollCreate += UnitychanController.scrollAddBonus;   // 巻物の出現率を確定する
        Debug.Log(scrollCreate);

        if (heartCreate　> randomCreate) {                  // 生成率よりも変数が低いならオブジェクトを生成する
            //  演出エフェクトを出す
            effect = (GameObject)Instantiate(effectPrefab, new Vector3(ojPos.x, ojPos.y + 2.0f, ojPos.z), Quaternion.identity);

            //  オブジェクトのインスタンスを生成し格納する
            oj = (GameObject)Instantiate(ojPrefab, new Vector3(ojPos.x, ojPos.y + 2.0f, ojPos.z), Quaternion.identity);

            //  魔法陣を下方から出現させて、上方に移動させる演出
            iTween.MoveFrom(oj, iTween.Hash("y", -100f, "time", 0.8f));            
        }
        else if (heartCreate < randomCreate && randomCreate < scrollCreate)
        {
            //  演出エフェクトを出す
            effect = (GameObject)Instantiate(effectPrefab, new Vector3(ojPos.x -0.2f, ojPos.y + 2.0f, ojPos.z), Quaternion.identity);

            //  オブジェクトのインスタンスを生成し格納する
            scroll = (GameObject)Instantiate(scrollPrefab, new Vector3(ojPos.x, ojPos.y + 2.0f, ojPos.z), Quaternion.identity);

            //  スクリプトを格納
            XpGet scrollScript = scroll.GetComponent<XpGet>();

            //  bonusXpを引数としてメソッドに渡す
            scrollScript.GetBonusXp(bonusXp);

            //  魔法陣を下方から出現させて、上方に移動させる演出
            iTween.MoveFrom(scroll, iTween.Hash("y", -100f, "time", 0.8f));

            //  テキスト表示位置設定用
            Vector3 meshPos = scroll.transform.position;

            //  bonusXpのTextをインスタンスで生成し、オブジェクトに入れる
            bonusXpText = (GameObject)Instantiate(bonusXpPrefab, new Vector3(meshPos.x -0.2f, meshPos.y +0.6f, meshPos.z), Quaternion.identity);

            //  TextMeshを格納する
            tMesh = bonusXpText.GetComponent<TextMeshPro>();

            //  bonusXpを表示する
            tMesh.text = bonusXp.ToString();

            //  bonusXpTextを子、巻物を親にする（消えるときに一緒に消えるように）
            bonusXpText.transform.SetParent(scroll.transform);
        }

        //  生成時のエフェクトは一定時間後に破棄
        Destroy(effect, 2.5f);
    }
}
