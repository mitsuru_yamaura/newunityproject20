﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageReturnArea : MonoBehaviour {

    public int power;                           //  落下時のダメージ
    public Transform returnPoint;               //  復帰場所の代入用

    private RotateCamera rotateCamera;          //  カメラの制御用

    Life lifeScript;                            //  ダメージ参照用の外部スクリプト
    UnitychanController uni;


    private void Update()
    {
        rotateCamera = GameObject.Find("MainCamera").GetComponent<RotateCamera>();
    }

    private void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            //  Life減少用とアニメ用のスクリプトを取得
            lifeScript = GameObject.Find("HeartScore").GetComponent<Life>();
            uni = col.gameObject.GetComponent<UnitychanController>();

            StartCoroutine("ReturnCharacter",col.gameObject);

            //  カメラのアングルを固定
            rotateCamera.fixCamera = true;
        }
    }

    private IEnumerator ReturnCharacter(GameObject chara)
    {
        yield return new WaitForSeconds(0.7f);

        //  カメラのアングルを固定を解除
        rotateCamera.fixCamera = false;

        //  キャラクターを復帰エリアに戻す
        chara.transform.position = returnPoint.position;

        //  ダメージを減少させるスクリプトのメソッドを呼ぶ
        lifeScript.ApplyDamage(power);

        //  アニメと音声のメソッドを呼ぶ
        uni.Restart();
    }
}
