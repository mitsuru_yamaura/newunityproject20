﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrontMoveEnemy : MonoBehaviour {

    Rigidbody rb;

    float faceCount;            //  向きを変えるまでの待機時間のカウンター
    float jumpCount;            //  ジャンプするまでの待機時間のカウンター

    float scaleY;               //  スケールYの元の値

    int facing = 0;             //  向きの確認用
    int randomFace;             //  向きを変えるまでの待機時間の設定値
    int randomJump;             //  ジャンプするまでの待機時間の設定値

    public float moveSpeedZ;    //  左右への移動速度
    public float jumpPower;     //  ジャンプ力
    public int attackPower;     //  攻撃力

    float nowMoveSpeedZ;         //  現在の移動速度
    float nowJumpPower;
    int nowAttackPower;

    void Start(){
        rb = GetComponent<Rigidbody>();
        //  現在地に初期値の取得。戻す場合に設定値を使うため
        nowMoveSpeedZ = moveSpeedZ;
        nowJumpPower = jumpPower;
        nowAttackPower = attackPower;
        //  小さくなった時に元に戻る値を入れておく
        scaleY = this.transform.localScale.y;
        //  スタート時に各アクションの待機時間をランダムに設定する
        randomFace = Random.Range(4, 7);
        randomJump = Random.Range(4, 7);
    }
    
    void Update(){
        if (!ZoomOut.cameraSetOn){
            return;
        } else {
            // 奥移動
            Move();
            faceCount += Time.deltaTime;
            jumpCount += Time.deltaTime;     
            if (faceCount > randomFace){
                // 待機時間のカウントがランダムで設定した数値以上になったら方向転換
                if (facing == 0){
                    //  向きを90度変える
                    transform.rotation = Quaternion.AngleAxis(-5, new Vector3(0, 1, 0));
                    nowMoveSpeedZ *= -1;
                    facing = 1;
                } else {
                    // 向きを-90度変える
                    transform.rotation = Quaternion.AngleAxis(175, new Vector3(0, 1, 0));
                    nowMoveSpeedZ *= -1;
                    facing = 0;
                }
                faceCount = 0f;
                randomFace = Random.Range(4, 7);
            }
            if (jumpCount > randomJump){
                // 待機時間のカウントがランダムで設定した数値以上になったらジャンプ
                rb.velocity = new Vector3(0, nowJumpPower, nowMoveSpeedZ);
                jumpCount = 0f;
                randomJump = Random.Range(4, 7);
            }
        }
    }

    /// <summary>
    /// 常に移動させる
    /// </summary>
    private void Move() {
        rb.velocity = new Vector3(0, rb.velocity.y, nowMoveSpeedZ);
    }
    
    public void OnTriggerEnter(Collider col) { // isTrigger用にコライダーを２つつけること
        if (col.gameObject.tag == "Player"){
            // プレイヤーキャラクターに接触したとき、ノックバック処理とライフ減算処理
            UnitychanController uniCon = GameObject.Find("Kohaku").GetComponent<UnitychanController>();
            Life life = GameObject.Find("HeartScore").GetComponent<Life>();
            uniCon.KnockBackEffect();
            life.ApplyDamage(nowAttackPower);
        } 
        if (col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge"){
            // WeaponかChargeタグに接触した時、被ダメージ処理
            PauseEnemy();
        }
    }

    /// <summary>
    /// 被ダメージ時の敵の停止処理と元に戻す処理
    /// hpの減少、破壊、宝箱の処理はBreakObjectで行う
    /// <summary>
    /// <returns></returns>
    IEnumerator PauseEnemy(){
        gameObject.layer = LayerMask.NameToLayer("EnemyDamage");
        BreakObject bObj = GetComponent<BreakObject>();        
        nowMoveSpeedZ = 0;
        nowJumpPower = 0;
        nowAttackPower = 0;

        yield return new WaitForSeconds(0.6f);
        // アニメ付きでオブジェクトを叩いて潰れたように見せる
        iTween.ScaleBy(this.gameObject, iTween.Hash("y", 0.3f, "time", 0.5f));
        
        if (bObj.hp > 0) {
            // hpが残っていれば元の状態に戻す
            yield return new WaitForSeconds(0.5f);
            iTween.ScaleTo(this.gameObject, iTween.Hash("y", scaleY, "time", 1.0f));
            nowMoveSpeedZ = moveSpeedZ;
            nowJumpPower = jumpPower;
            nowAttackPower = attackPower;
        }
    }  
}
