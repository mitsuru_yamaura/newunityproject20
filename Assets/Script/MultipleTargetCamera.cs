﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class MultipleTargetCamera : MonoBehaviour {

    public Camera cam;                    //  Cameraの取得
    public List<Transform> targets;       //  Listで対象を選択する

    public Vector3 offset;                //  Cameraのオフセットを指定する 
    public float smoothTime = 0.5f;       //  ターゲットが複数になった際のカメラの移動速度

    public float minZoom = 40;            //  FieldOfViewの最低値と最大値、リミット
    public float maxZoom = 10;　　　　　　　　
    public float zoomLimiter = 50;

    private Vector3 velocity;             //  カメラの移動用

    private void Reser()
    {
        cam = GetComponent<Camera>();
    }

    private void LateUpdate()
    {
        //  対象がいない場合には何もしない。
        if (targets.Count == 0) return;

        Move();
        Zoom();
    }

    private void Zoom()
    {
        var newZoom = Mathf.Lerp(maxZoom, minZoom, GetGreatestDistance() / zoomLimiter);
        cam.fieldOfView = Mathf.Lerp(cam.fieldOfView,newZoom,Time.deltaTime);
    }

    private void Move()
    {
        var centerPoint = GetCenterPoint();
        var newPosition = centerPoint + offset;

        transform.position = Vector3.SmoothDamp(transform.position, newPosition,ref velocity, smoothTime);
    }

    private float GetGreatestDistance()
    {
        var bounds = new Bounds(targets[0].position, Vector3.zero);
        for(int i =0; i < targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].position);
        }
        return bounds.size.x;
    }

    private Vector3 GetCenterPoint()
    {
        //
        if (targets.Count == 1) return targets[0].position;

        var bounds = new Bounds(targets[0].position, Vector3.zero);

        for(int i = 0; i < targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].position);
        }
        return bounds.center;
    }
}
