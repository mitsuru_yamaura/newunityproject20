﻿using UnityEngine;

public class SaveData : MonoBehaviour {

    public static int firstStageClearCount;
    public static int secondStageClearCount;

    void Awake (){
        firstStageClearCount = PlayerPrefs.GetInt("stage1ClearKey", firstStageClearCount);
        secondStageClearCount = PlayerPrefs.GetInt("stage2ClearKey", secondStageClearCount);
        Debug.Log(firstStageClearCount);
        Debug.Log(secondStageClearCount);
    }

    /// <summary>
    /// 保存されたスキル取得情報をリセット
    /// </summary>
    [ContextMenu("ResetSkills")]
    public void ResetFlags(){
        PlayerPrefs.DeleteKey("highJumpKey");
        PlayerPrefs.DeleteKey("speedUpKey");
        PlayerPrefs.DeleteKey("chargeHammerKey");
        Debug.Log("Reset Skills!!");
    }

    /// <summary>
    /// データの保存。staticでいつでも呼べるようにし、すべてint型を保存
    /// 保存対象とタイミング【ライフ最大値更新時、フォースレベルアップ時、スキル取得時、ルーン獲得時、ステージクリア時、言語選択時、チュートリアル終了時】
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    public static void Save(string key,int value){
        PlayerPrefs.SetInt(key, value);
        PlayerPrefs.Save();
        Debug.Log(key);
        Debug.Log(value);
    }
}
