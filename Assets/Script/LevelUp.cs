﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUp : MonoBehaviour
{
    [SerializeField] ParticleSystem particle;                  //  レベルアップ時の演出
    [SerializeField] public static int nextLevelXp = 1000;     //  レベルアップのために必要なXPの基礎値
    
    AudioSource sound1;                          //  SE  1-2(GameScene1,2で異なる)
    AudioSource sound2;
    AudioSource sound3;                          //  SE  レベルアップ時の効果音1
    AudioSource sound4;                          //  SE  レベルアップ時のparticleに合わせたSE

    void Start(){
        this.transform.localScale = new Vector3(1.2f * (0.1f * (UnitychanController.forceLevel + 1)), 1.2f * (0.1f * (UnitychanController.forceLevel + 1)), 1.2f * (0.1f * (UnitychanController.forceLevel + 1)));
    }

    /// <summary>
    /// フォースツリーをレベルアップさせる。TreeSwitchから呼ばれる
    /// </summary>
    public void ForceLevelUp(){       
        if (UnitychanController.forceLevel == 10) {   // ForceLevelが10なら成長しない
            return;
        }                                         // xpの現在値を取得し、獲得可能か確認させる
        int nowNextXp = nextLevelXp * (UnitychanController.forceLevel +1);    // 次のレベルアップに必要なXPを計算し代入する        
        if (XpCounter.xpCount >= nowNextXp){                              // XPの現在値が次のレベルアップに必要なXP以上なら                    
            Vector3 pos = this.transform.position;                        // レベルツリーの位置情報をposで取得しカメラをズームイン
            GameObject.Find("MainCamera").GetComponent<ZoomOut>().ZoomIn(pos, "powerup");
            GameObject.Find("ExpText").GetComponent<XpCounter>().SubtractXp(nowNextXp);   // nowNextXPを引数にして、XpCounterのSubtractXp(減算)            
            UnitychanController.forceLevel++;                             // レベルを上げる
            string LEVEL_KEY = "LevelKey";                                // PlayerPrefsでレベル保存／呼び出すためのキー
            SaveData.Save(LEVEL_KEY, UnitychanController.forceLevel);     // セーブ
            particle.Play();                                              // 演出を入れる
            AudioSource[] audioSources = GetComponents<AudioSource>();    // 演出SEを鳴らす
            sound4 = audioSources[3];
            sound4.Play();
            GameObject.Find("ForcesStateText").GetComponent<ForceBonus>().AddForce(UnitychanController.forceLevel);   // レベルアップボーナス取得
            UnitychanController uniCon = GameObject.Find("Kohaku").GetComponent<UnitychanController>();
            string initTag = uniCon.gameObject.tag;                              // インフォテキストを非表示に。元のタグを入れておく
            uniCon.gameObject.tag = "LevelUp";                            // ２回叩けないようにUnitychanのタグを切り替える
            StartCoroutine(BackDefaultTag(initTag, uniCon));                                    //  ツリーをアニメ再生で大きくする
            iTween.ScaleTo(this.gameObject, iTween.Hash("x", 1.2f * (0.1f * (UnitychanController.forceLevel +1)), "y", 1.2f * (0.1f * (UnitychanController.forceLevel +1)), "z", 1.2f * (0.1f * (UnitychanController.forceLevel +1)), "time", 1.5f));
            Debug.Log(initTag);
            Debug.Log(uniCon.gameObject.tag);
        }
    }

    /// <summary>
    /// レベルアップタグを元のタグに戻す
    /// </summary>
    /// <returns></returns>
    private IEnumerator BackDefaultTag(string initTag, UnitychanController uniCon){
        AudioSource[] audioSources = GetComponents<AudioSource>();
        sound1 = audioSources[0];
        sound2 = audioSources[1];
        sound3 = audioSources[2];
        yield return new WaitForSeconds(1.7f);
        sound3.Play();                             // レベルアップ時のSE再生
        yield return new WaitForSeconds(0.3f);
        var randomValue = Random.Range(0, 2);      // ランダムで音声SEを再生
        if (randomValue == 0){
            sound1.Play();
        } else {
            sound2.Play();
        }
        uniCon.gameObject.tag = initTag;                          // 元のタグに戻す
    }
}

