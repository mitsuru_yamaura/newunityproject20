﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpearTrap : MonoBehaviour {

    float scaleY;               //  スケールYの元の値

    float trapCount;            //　発動までのタイマー
    int randomTrapTimer;        //  罠発動までのランダム待機時間

    //public float speed;         //  動作する速度

    // Use this for initialization
    void Start () {

        //  大きくなった後に戻す元の大きさを取得しておく
        scaleY = this.transform.localScale.y;

        //  罠発動までの時間をランダムに設定
        randomTrapTimer = Random.Range(3, 5);
	}

    // Update is called once per frame
    void Update()
    {
        //  発動までのタイマースタート
        trapCount += Time.deltaTime;

        //  発動までのランダム待機時間を超えたら
        if(randomTrapTimer > trapCount)
        {
            iTween.ScaleTo(this.gameObject, iTween.Hash("y", 0.8f, "time", 0.2f));

            StartCoroutine("Flat");
        }
    }

    //  元の大きさに戻すコルーチン
    IEnumerator Flat()
    {
        yield return new WaitForSeconds(1.0f);

        iTween.ScaleTo(this.gameObject, iTween.Hash("y", scaleY, "time", 0.2f));

        //  カウントを戻す
        trapCount = 0f;

        //　次回の待機時間をランダムで設定する
        randomTrapTimer = Random.Range(3, 5);
    }
       
}
