﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotShell : MonoBehaviour
{
    public GameObject enemyShellPrefab;     //  インスタンス生成元
    GameObject enemyShell;                  //  インスタンスを入れるオブジェクト
    public float shotSpeed;                 //  弾の速度
    public AudioClip shotSound;             //  SE
    public Animator anim;                   //  敵のアニメーション制御用
   
    private float shotIntarval;             //  弾を発射するためのインターバル
    bool isStart;                           //  弾を発射しているかどうかのフラグ。発射中は弾を打たない

    // Update is called once per frame
    void Update()
    {
        if (!isStart)
        {
            return;
        }

        if (isStart)
        {
            //  時間経過とともにカウントする
            shotIntarval += Time.deltaTime;

            //  時間が規定値に達したら弾のプレファブを作り、RigidBodyをつける
            if (shotIntarval > 5.0f)
            {
                anim.SetTrigger("Action");
                StartCoroutine("ShotAction");
                shotIntarval = 0;

                isStart = false;
                Rader.isShot = false;
            }
        }
    }

    public void Shot()
    {
        isStart = true;

        anim.SetTrigger("Action");
        StartCoroutine("ShotAction");
    }

    IEnumerator ShotAction()
    {
        //  アニメと合わせるためにSEと発射を遅らせる
        yield return new WaitForSeconds(1.4f);

        AudioSource.PlayClipAtPoint(shotSound, transform.position);

        //  prefabから弾を生成し、enemyShellにいれる
        enemyShell = (GameObject)Instantiate(enemyShellPrefab, transform.position, Quaternion.identity);
        Rigidbody enemyShellRb = enemyShell.GetComponent<Rigidbody>();

        //  前方に飛ばす
        enemyShellRb.AddForce(transform.forward * shotSpeed);

        Destroy(enemyShell, 2.5f);
    }

    private void OnCollisionEnter(Collision col)            //  プレイヤーに接触したら弾を破壊する
    {
        if(col.gameObject.tag == "Player"|| col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
        {
            Destroy(enemyShell);
        }
    }
}