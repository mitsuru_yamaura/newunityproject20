﻿using System.Collections;
using UnityEngine;

public class StageStartSwitch : MonoBehaviour {

    [SerializeField] TextMesh tMesh;            //  text表示用のオブジェクトをアサイン
    [SerializeField] GameObject stageTextBack;  //  背景表示用のオブジェクトをアサイン。iTween用
    public SpriteRenderer sRen;       //  背景画表示用のオブジェクトをアサイン。同時に操作可能のフラグを兼ねる
    public int stageNo;                         //  ステージの番号

    private void Start(){               
        StartCoroutine(OpenStageNamePopup());             //  カメラの移動に合わせてコルーチンで表示を制御
    }

    /// <summary>
    /// // ステージ開始時にステージ名をポップアップ表示
    /// </summary>
    /// <returns></returns>
    private IEnumerator OpenStageNamePopup(){
        Vector3 scalePos = stageTextBack.transform.localScale;    //  拡大・縮小表示する描画用オブジェクトのScale値を取得する
        yield return new WaitForSeconds(1.0f);
        sRen.enabled = true;   //  textと画像を表示する
        stageTextBack.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);          //  拡大・縮小表示する描画用オブジェクトのScale値を最小値にする
        iTween.ScaleTo(stageTextBack, iTween.Hash("x", scalePos.x, "y", scalePos.y, "z", scalePos.z, "time", 1.0f));  //  元の大きさまでアニメで表示
        switch (stageNo) {
            case 0:                                                                                            //  ステージの番号を名称を表示
                if (LanguageChoice.languageNum == 0) {                                                                    //  言語選択で表示テキスト分岐             
                    tMesh.text = "ゆりかご島\n-Cradle Island-\n\n画面左側ではステージ選択が行えます\n画面右側ではキャラクターの育成を行えます\n\nスキルを取得したりライフの最大値を上げて\n冒険を有利に進めましょう!";
                } else {
                    tMesh.text = "\n\n-BaseCamp in Cradle Island-\n\nStage selection scene.\nYou can learn skills and select stages.";
                }
                break;
            case 1:
                tMesh.text = "\n\nStage " + stageNo.ToString() + "\n\n砂埃の丘\n-Calmwindless Hill-\n\n";
                break;
            case 2:
                tMesh.text = "\n\nStage " + stageNo.ToString() + "\n\n極夜の峡谷\n-Valley of the Polar Night-\n\n";
                break;
            case 3:
                tMesh.text = "\n\nStage " + stageNo.ToString() + "\n\nムーンロア遺跡\n-Ruins of Moonlore-\n\n";
                break;
        }
        yield return new WaitForSeconds(3.5f);        
        tMesh.text = " ";                                                                            //  テキストを空白に、背景の描画を非表示にする。描画フラグを降ろす
        iTween.ScaleTo(stageTextBack, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 1.0f));
        yield return new WaitForSeconds(1.0f);
        sRen.enabled = false;
    }
}
