﻿using UnityEngine;
using UnityEngine.UI;

public class ForceBonus : MonoBehaviour {

    [SerializeField] Text fStateText;
    private int nowForceLevel;

    /// <summary>
    /// レベルアップ時にForceLevelに応じたボーナスを獲得。LevelUp.csから呼ばれる
    /// </summary>
    /// <param name="value"></param>
    public void AddForce(int value){      
        nowForceLevel = value;        // レベルを取得する       
        switch (nowForceLevel) {      //　ベルに応じたボーナスを獲得する
            case 1:              
                UnitychanController.moveSpeed = 6;            // 移動速度増加
                UnitychanController.actionSpeed = 1.1f;                
                break;
            case 2:                
                UnitychanController.xpAddBonus = 15;          // 獲得XP増加
                UnitychanController.actionSpeed = 1.2f;
                break;
            case 3:              
                UnitychanController.heartAddBonus = 5;        // ハート出現率増加
                UnitychanController.actionSpeed = 1.3f;
                break;
            case 4:                
                UnitychanController.scrollAddBonus = 5;       // 巻物出現率増加
                UnitychanController.actionSpeed = 1.4f;
                break;
            case 5:             
                UnitychanController.hammerPower = 2;          // 攻撃力増加
                UnitychanController.actionSpeed = 1.5f;
                break;
            case 6:            
                UnitychanController.moveSpeed = 7;            // 移動速度増加
                UnitychanController.actionSpeed = 1.6f;
                break;
            case 7:            
                UnitychanController.xpAddBonus = 30;          // 獲得XP増加
                UnitychanController.actionSpeed = 1.7f;
                break;
            case 8:             
                UnitychanController.heartAddBonus = 10;       // ハート出現率増加
                UnitychanController.actionSpeed = 1.8f;
                break;
            case 9:             
                UnitychanController.scrollAddBonus = 10;      // 巻物出現率増加
                UnitychanController.actionSpeed = 1.9f;
                break;
            case 10:                
                UnitychanController.hammerPower = 3;          // 攻撃力増加
                UnitychanController.actionSpeed = 2.0f;
                break;
            default:
                break;
        }
    }

    /// <summary>
    //  現在取得しているForceBonusをすべて取得する。Pause時と各ステージ開始時にUnitychanController.csから呼ばれる
    //  LevelUp.cs内のforceLevelをvalueで受け取る
    /// </summary>
    /// <param name="value"></param>
    public void GetForceBonus(int value, bool isPrintTxt = false){     
        nowForceLevel = value;                            // レベルを取得する
        for (int i = 0; i <= nowForceLevel; i++){
            switch (i) {
                case 0:
                    UnitychanController.moveSpeed = 5;    // 移動力に初期値を入れる
                    UnitychanController.hammerPower = 1;  // 攻撃力に初期値を入れる
                    UnitychanController.actionSpeed = 1.0f;   //  攻撃速度のアニメに初期値を入れる
                    if (isPrintTxt) {
                        fStateText.text = "新米のブーツ (Nomal)\n新品ハンマー (Min)\n\nボーナスなし (no Bonus)\nボーナスなし (no Bonus)\nボーナスなし (no Bonus)";
                    }
                    break;
                case 1:
                    UnitychanController.moveSpeed = 6;       // 移動速度増加
                    UnitychanController.actionSpeed = 1.1f;
                    if (isPrintTxt) {
                        fStateText.text = "踏破者のブーツ (Fast)\n新品ハンマー (Min)\n\nボーナスなし (no Bonus)\nボーナスなし (no Bonus)\nボーナスなし (no Bonus)";
                    }
                    break;
                case 2:
                    UnitychanController.xpAddBonus = 15;     // 獲得XP増加
                    UnitychanController.actionSpeed = 1.2f;
                    if (isPrintTxt) {
                        fStateText.text = "踏破者のブーツ (Fast)\n新品ハンマー (Min)\n\nうさ耳飾り + " + UnitychanController.xpAddBonus.ToString() + " %\nボーナスなし (no Bonus)\nボーナスなし (no Bonus)";
                    }
                    break;
                case 3:
                    UnitychanController.heartAddBonus = 5;   // ハート出現率増加
                    UnitychanController.actionSpeed = 1.3f;
                    if (isPrintTxt) {
                        fStateText.text = "踏破者のブーツ (Fast)\n新品ハンマー (Min)\n\nうさ耳飾り " + UnitychanController.xpAddBonus.ToString() + " %\nValue Up + " + UnitychanController.heartAddBonus.ToString() + " %\nボーナスなし (no Bonus)";
                    }
                    break;
                case 4:
                    UnitychanController.scrollAddBonus = 5;  // 巻物出現率増加
                    UnitychanController.actionSpeed = 1.4f;
                    if (isPrintTxt) {
                        fStateText.text = "踏破者のブーツ (Fast)\n新品ハンマー (Min)\n\nうさ耳飾り " + UnitychanController.xpAddBonus.ToString() + " %\nValue Up + " + UnitychanController.heartAddBonus.ToString() + " %\nValue Up + " + UnitychanController.scrollAddBonus.ToString() + " % ";
                    }
                    break;
                case 5:
                    UnitychanController.hammerPower = 2;     // 攻撃力増加
                    UnitychanController.actionSpeed = 1.5f;
                    if (isPrintTxt) {
                        fStateText.text = "踏破者のブーツ (Fast)\n愛用ハンマー (Mid)\n\nうさ耳飾り " + UnitychanController.xpAddBonus.ToString() + " %\nValue Up + " + UnitychanController.heartAddBonus.ToString() + " %\nValue Up + " + UnitychanController.scrollAddBonus.ToString() + " % ";
                    }
                    break;
                case 6:
                    UnitychanController.moveSpeed = 7;       // 移動速度増加
                    UnitychanController.actionSpeed = 1.6f;
                    if (isPrintTxt) {
                        fStateText.text = "ウィングブーツ (Very Fast)\n愛用ハンマー (Mid)\n\nうさ耳飾り " + UnitychanController.xpAddBonus.ToString() + " %\nValue Up + " + UnitychanController.heartAddBonus.ToString() + " %\nValue Up + " + UnitychanController.scrollAddBonus.ToString() + " % ";
                    }
                    break;
                case 7:
                    UnitychanController.xpAddBonus = 30;     // 獲得XP増加
                    UnitychanController.actionSpeed = 1.7f;
                    if (isPrintTxt) {
                        fStateText.text = "ウィングブーツ (Very Fast)\n愛用ハンマー (Mid)\n\nうさ耳ブレス " + UnitychanController.xpAddBonus.ToString() + " %\nValue Up + " + UnitychanController.heartAddBonus.ToString() + " %\nValue Up + " + UnitychanController.scrollAddBonus.ToString() + " % ";
                    }
                    break;
                case 8:
                    UnitychanController.heartAddBonus = 10;  // ハート出現率増加
                    UnitychanController.actionSpeed = 1.8f;
                    if (isPrintTxt) {
                        fStateText.text = "ウィングブーツ (Very Fast)\n愛用ハンマー (Mid)\n\nうさ耳ブレス " + UnitychanController.xpAddBonus.ToString() + " %\nValue Up + " + UnitychanController.heartAddBonus.ToString() + " %\nValue Up + " + UnitychanController.scrollAddBonus.ToString() + " % ";
                    }
                    break;
                case 9:
                    UnitychanController.scrollAddBonus = 10; // 巻物出現率増加
                    UnitychanController.actionSpeed = 1.9f;
                    if (isPrintTxt) {
                        fStateText.text = "ウィングブーツ (Very Fast)\n愛用ハンマー (Mid)\n\nうさ耳ブレス" + UnitychanController.xpAddBonus.ToString() + " %\nValue Up + " + UnitychanController.heartAddBonus.ToString() + " %\nValue Up + " + UnitychanController.scrollAddBonus.ToString() + " % ";
                    }
                    break;
                case 10:
                    UnitychanController.hammerPower = 3;     // 攻撃力増加
                    UnitychanController.actionSpeed = 2.0f;
                    if (isPrintTxt) {
                        fStateText.text = "ウィングブーツ (Very Fast)\n万能ハンマー (Max)\n\nうさ耳ブレス " + UnitychanController.xpAddBonus.ToString() + " %\nValue Up + " + UnitychanController.heartAddBonus.ToString() + " %\nValue Up + " + UnitychanController.scrollAddBonus.ToString() + " % ";
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
