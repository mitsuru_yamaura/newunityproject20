﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLifeView : MonoBehaviour {

    public TextMesh tMesh;            //  text表示用のコンポーネントをアサイン  
    public SpriteRenderer sRen;       //  背景画表示用のコンポーネントをアサイン
    public GameObject stageTextBack;  //  背景表示用のオブジェクトをアサイン。iTween用

    [SerializeField]
    GameObject circle;                //  演出用

    Vector3 scalePos;　　　　　　　   //  元の大きさの保存用
    bool isRend;                      //  アニメ描画したかどうかのフラグ

    int nowNextLifeXp = 2000;

    AudioSource sound1;               //  演出用SE

    // Use this for initialization
    void Start()
    {
        //  拡大・縮小表示する描画用オブジェクトのScale値を取得する
        scalePos = stageTextBack.transform.localScale;
    }

    //  プレイヤーがswitch内にいる間表示するメソッド
    private void OnTriggerStay(Collider col)
    {
        if (!isRend)
        {
            //  プレイヤーがswitchを踏んだらオン
            if (col.gameObject.tag == "Player" || col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
            {
                sound1 = circle.GetComponent<AudioSource>();

                //  演出を表示、SE再生
                circle.SetActive(true);
                sound1.Play();

                //  textと画像を表示する
                sRen.enabled = true;

                //  拡大・縮小表示する描画用オブジェクトのScale値を最小値にする
                stageTextBack.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

                //  TextとImageを元の大きさまでアニメで表示
                iTween.ScaleTo(stageTextBack, iTween.Hash("x", scalePos.x, "y", scalePos.y, "z", scalePos.z, "time", 1.0f));

                tMesh.text = "-ライフの花(Life Flowers)-\n\n必要なXPを消費することでMax Life(最大体力)を１個\n増やすことができます。（最大１０個）\n\n\n現在のMax Life(最大体力) : " + UnitychanController.maxLife.ToString() + "\n↓";

                //  現在のMaxLifeに応じて内容を変更して表示する
                if (UnitychanController.maxLife < 10)
                {
                     //  レベル0-4のボーナス内容を表示
                     tMesh.text += "\nMax Life " + (UnitychanController.maxLife + 1).ToString() + " に必要なXP : " + (nowNextLifeXp * (UnitychanController.maxLife-4)).ToString() + " pt\n\n\nハンマーで叩いてLifeの最大値アップ!!";
                }
                else    //  maxLife10の場合
                {
                     tMesh.text += "\nMax Life は最大値です!!";
                }
                //  描画フラグを立てる
                isRend = true;
            }
        }

        //  描画中にレベルアップがあった場合、非表示にする
        if (isRend)
        {
            //  レベルアップ時のタグになったら
            if (col.gameObject.tag == "LevelUp")
            {
                circle.SetActive(false);
                
                //  テキストを空白に、背景の描画を非表示にする。描画フラグを降ろす
                tMesh.text = " ";

                //  textとimageをアニメで非表示
                iTween.ScaleTo(stageTextBack, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.5f));
            }

        }
    }

    //  プレイヤーがswitchから離れたら非表示にするメソッド
    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
        {
            //  テキストを空白に、背景の描画を非表示にする。描画フラグを降ろす
            tMesh.text = " ";

            //  演出を非表示
            circle.SetActive(false);

            if (isRend)
            {
                //  textとimageをアニメで非表示
                iTween.ScaleTo(stageTextBack, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.5f));

                //  描画フラグを降ろす
                isRend = false;
            }
        }
    }
}
