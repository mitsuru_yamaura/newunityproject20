﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotate : MonoBehaviour {

    AudioSource sound1;          //  倒れた時のSE

    int count = 0;               //  叩いた回数のカウント。多くなるほど揺れが大きくなる
    float speed = 50.0f;         //  回転する際の速度

    public bool isSwitch;        //  回転するフラグ管理用フラグ。外部参照可能にしてある
    bool isSePlay;

    public int hp;

    public float rotX;      //  回転軸と角度を設定
    public float rotY;
    public float rotZ;

    ParticleSystem particleHammer;            //  ハンマーでたたいた時のエフェクト用

    // Use this for initialization
    void Start ()
    {
        //  ハンマーオブジェクトの子であるHammerEffectのパーティクルを取得
        particleHammer = GameObject.Find("HammerEffect").GetComponent<ParticleSystem>();

        sound1 = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isSwitch)
        {
            //  回転を時間に合わせて行う
            float step = speed * Time.deltaTime;

            //  回転させて地面に倒す処理
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(rotX, rotY, rotZ), step);

            //  SEがなっていないなら
            if (!isSePlay)
            {
                //  SE再生のメソッドを呼び出す
                Seplay();
            }
        }
    }

    //  当たり判定
    void OnTriggerEnter(Collider col)
    {
        //  Weaponタグに接触した時（ハンマーの振り下ろす時からWeaponタグになる）
        if (col.gameObject.tag == "Weapon"|| col.gameObject.tag == "Charge")
        {
            //  Hpが残っているなら
            if (hp > 0)
            {
                //  叩いているエフェクトを再生
                particleHammer.Play();

                //  叩いているSEを再生
                sound1.Play();

                //  Hpを減らす
                hp--;

                //  叩いた回数のカウント
                count++;

                //  アニメ付きでオブジェクトを揺らして叩いて倒れるアピールをする
                //  １回目よりの２回目の方が大きく揺れるようにする
                iTween.ShakePosition(this.gameObject, iTween.Hash("z", 0.5f * count, "time", 1.0f));
                //Debug.Log(hp);

                //  叩いたときにHpが0以下になったなら
                if (hp <= 0)
                {
                    //  回転させるフラグをオンにする
                    isSwitch = true;
                }
            }
        }
    }


    //  SE再生用
    private void Seplay()
    {
        //  SEが再生されたフラグを立てて１回だけ鳴らす
        isSePlay = true;

        //  コルーチンの呼出し
        StartCoroutine("SePlay");
    }

    IEnumerator SePlay()
    {
        //  地面に柱が倒れるタイミングでSEを鳴らす
        yield return new WaitForSeconds(1.5f);
        sound1.Play();
    }
}
