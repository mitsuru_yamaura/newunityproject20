﻿
using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
#if UNITY_EDITOR
using UnityEditor.Animations;
#endif

public class UnitychanController : MonoBehaviour {

    [SerializeField]
    public static int forceLevel;               //  ForceLevelの現在値

    public static int nowRune1;        //  stage1のrune合計獲得値  
    public static int expCount;        //  xp合計値
    public static int hammerPower;     //  ハンマーの最大攻撃力
    public static int maxLife;         //  現在の最大Life
    public float jumpPower;            //  キャラのジャンプ力
    public static float moveSpeed;     //  キャラの移動速度
    public static float actionSpeed;   //  キャラの攻撃速度
    public int chargePower;            //  チャージ攻撃の攻撃力
    public bool isEnd;                 //  ゲームオーバーの管理フラグ。Pauseでも利用する

    public GameObject hammer;
    public GameObject hammerEffect;
    public LayerMask groundLayer;      //  Linecastでの着地判定用のLayer指定
    public LayerMask nonBottomLayer;   //  Linecastでの着地判定用のLayer指定（空中床用）
    public ParticleSystem particle;    //  チャージ用エフェクト。非表示のcircleを利用

    float inputHorizontal;              //  移動上下方向
    float inputVertical;                //  移動左右方向
    float jumpCount;                    //  ハイジャンプ用のカウンター管理
    float dashCount;                    //  ダッシュ時のカウンター管理
    float chargeCount;                  //  チャージ攻撃のカウンター管理

    public static int xpAddBonus;           //  XPへのボーナス加算値
    public static int heartAddBonus;        //  ハートの出現率へのボーナス
    public static int scrollAddBonus;       //  巻物の出現率へのボーナス 

    string LIFE_KEY = "lifeKey";            //  最大Life読み込み用のキー
    string LEVEL_KEY = "LevelKey";          //  PlayerPrefsでレベル保存／呼び出すためのキー

    [SerializeField] private Rigidbody rb;
    [SerializeField] private Animator anim;
    [SerializeField] private ForceBonus fBonus;
    Tutorial tutorial;
    public StageStartSwitch stageStartSwitch;   //  キー操作可能のフラグ用（ロードシーン先読み対応）

#if UNITY_EDITOR
    AnimatorState state;           　　　　　//  アニメーションの状態
    AnimatorController animCon;
#endif

    AudioSource sound1;                 //  SE  ジャンプの掛け声 1-3
    AudioSource sound2;
    AudioSource sound3;
    AudioSource sound4;                 //  SE  被ダメージ 1-2
    AudioSource sound5;
    AudioSource sound6;                 //  SE  ゲームオーバー 1-2
    AudioSource sound7;
    AudioSource sound8;                 //  SE  クリア　1-2
    AudioSource sound9;
    AudioSource sound10;                //  ハンマーを振るSE
    AudioSource sound11;                //  ハンマーをしまうSE
    AudioSource sound12;                //  ハンマーを構えるSE
    AudioSource sound13;                //  チャージ用SE

    bool isGround;                      //  着地判定用管理フラグ
    bool isDamage;                      //  ダメージ、無敵判定管理用フラグ
    public bool isDebugFlg;      //  デバッグ用フラグ。trueでオン
    bool isJumpCharge;                  //  SE用

    //  戻り値確認のフラグ
    public int isHighJumpGet;           //  パワーアップスキル「ハイジャンプ」を取得しているか
    public int isSpeedUpGet;            //  パワーアップスキル「スピードアップ」を取得しているか
    public int ischargeHummerGet;       //  パワーアップスキル「チャージハンマー」を所持しているか
    public PlayerState nowPlayerState;  //  現在のプレイヤーの状態の管理    

    public enum PlayerState{            //  プレイヤーの状態の遷移用
        normal,
        hammerGet,
        highJump,
        speedUp,
        charge,
        slide
    }

    private void Awake(){
        maxLife = PlayerPrefs.GetInt(LIFE_KEY, 5);         // 現在のライフの最大値を取得
        forceLevel = PlayerPrefs.GetInt(LEVEL_KEY, 0);     // 現在のレベルを取得
    }

    
    void Start(){
        fBonus.GetForceBonus(forceLevel);    //  獲得済みのForceを取得するメソッドの呼び出し
        AudioSource[] audioSources = GetComponents<AudioSource>();
        sound1 = audioSources[0];
        sound2 = audioSources[1];
        sound3 = audioSources[2];
        sound10 = audioSources[9];
        sound11 = audioSources[10];
        sound12 = audioSources[11];
        sound13 = audioSources[12];
        
        isHighJumpGet = PlayerPrefs.GetInt("highJumpKey", 0);                  // ステージセレクトで獲得したスキルの取得状況をPlayerPrefより取得
        isSpeedUpGet = PlayerPrefs.GetInt("speedUpKey", 0);
        ischargeHummerGet = PlayerPrefs.GetInt("chargeHammerKey", 0);        

#if UNITY_EDITOR       
        animCon = anim.runtimeAnimatorController as AnimatorController;        // 今使っているAnimatorControllerを取得
        var layers = animCon.layers;                             // AnimatorControllerのレイヤーを取得
        foreach (var layer in layers){           
            if (layer.stateMachine.name == "Base Layer") {       // Base Layerの中で指定のステートを探す
                var animStates = layer.stateMachine.states;
                foreach (var animState in animStates){                    
                    if (animState.state.name == "Action") {      // Actionを探し、攻撃速度を代入する
                        state = animState.state;
                        state.speed = actionSpeed;
                        anim.Rebind();
                        Debug.Log(actionSpeed);
                    }
                }
            }
        }
#endif
        if (isDebugFlg) {
            return;
        } else if (Tutorial.tutorialClear == 0){                  // チュートリアルが終わっていないなら
            tutorial = GameObject.Find("TutorialCanvas").GetComponent<Tutorial>();
        }
    }


    void Update(){
        if (!ZoomOut.cameraSetOn){　　　　　          //  MainCameraのZoomOutが終了したか確認。それまでは操作無効
            return;
        }        
        if (stageStartSwitch.sRen.enabled == true){   //  ステージ開始時のウインドウが開いている間は操作不能
            return;
        }        
        if (isDamage) {                               //  ダメージを受けたら点滅させる
            var renderComponent = GetComponent<Renderer>();
            renderComponent.enabled = !renderComponent.enabled;
        }

        //  上下左右キーの入力用変数(Rawは補正をかけず-1か1で返す)  アナログキーで動く
        inputHorizontal = Input.GetAxisRaw("Horizontal");
        inputVertical = Input.GetAxisRaw("Vertical");

        inputHorizontal = CrossPlatformInputManager.GetAxisRaw("Horizontal");
        inputVertical = CrossPlatformInputManager.GetAxisRaw("Vertical");

        if(Mathf.Abs(inputHorizontal) > Mathf.Abs(inputVertical)){
            inputVertical = 0;
        } else if (Mathf.Abs(inputHorizontal) < Mathf.Abs(inputVertical)){
            inputHorizontal = 0;
        }
      
        //  ここからジャンプ処理  //
        //  ゲームオーバーでないなら操作可能
        if (isEnd == false)
        {
            //  jumpステートを確認し、jumpしていない状態ならjumpアニメをfalseにセットする
            if (this.anim.GetCurrentAnimatorStateInfo(0).IsName("Jump"))
            {
                anim.SetBool("Jump", false);
            }

            //  Linecastでキャラの足元に地面があるかレイヤーで判定。あるならtrueを返す
            //  キャラの位置からみて足元を定義する
            isGround = Physics.Linecast(
                transform.position + transform.up * 1,
                transform.position - transform.up * 0.3f,
                groundLayer);

            if (!isGround)
            {
                //  同様に空中床の場合もジャンプできるようにフラグを返す
                isGround = Physics.Linecast(
                    transform.position + transform.up * 1,
                    transform.position - transform.up * 0.3f,
                    nonBottomLayer);
            }

            //  ジャンプしたとき
            if (!isGround)
            {
                //  下方向から上方向にある床をすり抜けられるようにするレイヤーに一時的に変更する
                gameObject.layer = LayerMask.NameToLayer("JumpPlayer");
                //Debug.Log(gameObject.layer);

                //  コルーチンを呼び出し元に戻す
                StartCoroutine("JumpLayer");
            }
            
            //  highJumpを取得しているなら
            if (isHighJumpGet ==1)
            {
                //  ジャンプボタンを押し続けてためる
                if (Input.GetButton("Jump"))
                {
                    //  着地していたとき
                    if (isGround)
                    {
                        //  ハイジャンプ用のカウント
                        jumpCount += Time.deltaTime;
                        //Debug.Log(jumpCount);                    
                    }
                }

                //  これをいれないとチャージ前にジャンプしてしまう
                if (CrossPlatformInputManager.GetButtonDown("Jump") || Input.GetButtonDown("Jump"))
                {
                    //  着地していたとき
                    if (isGround)
                    {
                        //  1秒後からparticleの演出をして利用可能を表現する
                        particle.Play();
                    }                    
                }

                //  チャージ時間があってジャンプする場合、SEを鳴らす
                //  この位置でボタンの長押しか１回かの入力を確認している
                if (jumpCount > 0.2f)
                {
                    //  ジャンプ用のチャージがあるか
                    if (!isJumpCharge)
                    {
                        //  ないならフラグを立てて
                        isJumpCharge = true;

                        //  チャージ状態に入ったことを示すSEを鳴らす
                        sound13.Play();
                    }
                }

                //　キーを離した時のカウントの内容で分岐 
                if (CrossPlatformInputManager.GetButtonUp("Jump") || Input.GetButtonUp("Jump")) {
                    //  1秒以上押してから離していたら
                    if (jumpCount > 1.0f) {
                        //  着地していたとき
                        if (isGround) {
                            //  着地判定をfalse
                            isGround = false;

                            //  jumpステートへ遷移してジャンプアニメを再生
                            //  Triggerなので１回のみ再生。戻す必要はない
                            anim.SetTrigger("Jump");

                            //  ハイジャンプ
                            //　AddForceにて上方向へ力を加える
                            rb.AddForce(Vector3.up * jumpPower * 2);
                        }
                    } else {
                        //  着地していたとき
                        if (isGround) {
                            //  着地判定をfalse
                            isGround = false;

                            //  jumpステートへ遷移してジャンプアニメを再生
                            //  Triggerなので１回のみ再生。戻す必要はない
                            anim.SetTrigger("Jump");

                            //  普通にジャンプ
                            //　AddForceにて上方向へ力を加える
                            rb.AddForce(Vector3.up * jumpPower);
                        }
                    }
                    jumpCount = 0;                            // チャージ用のカウンターを0にリセット
                    particle.Stop();                          // particleの再生を止める
                    var randomValue = Random.Range(0, 3);     // ランダムでSEを再生する
                    if (randomValue == 0) {
                        sound1.Play();
                    } else if (randomValue == 1) {
                        sound2.Play();
                    } else {
                        sound3.Play();
                    }
                    isJumpCharge = false;                     // チャージ用SEのフラグを降ろす
                }
            } else {                                                                                     // ハイジャンプを取得していないなら
                if (CrossPlatformInputManager.GetButtonDown("Jump") || Input.GetButtonDown("Jump")) {     // キー入力のjumpで反応（スペースキー）
                    if (isGround) {                              // 着地していたとき
                        isGround = false;                        // 着地判定をfalse
                        anim.SetTrigger("Jump");                 // jumpステートへ遷移してジャンプアニメを再生(Triggerなので１回のみ再生。戻す必要はない)
                        rb.AddForce(Vector3.up * jumpPower);     // AddForceにて上方向へ力を加える
                    }
                    var randomValue = Random.Range(0, 3);        // ランダムでSEを再生する
                    if (randomValue == 0) {
                        sound1.Play();
                    } else if (randomValue == 1) {
                        sound2.Play();
                    } else {
                        sound3.Play();
                    }
                    if (isDebugFlg) {
                        return;
                    } else if (Tutorial.tutorialClear == 0) {   // チュートリアルが終わっていないなら
                        if (!Tutorial.isJump) {
                            Tutorial.isJump = true;
                            int value = 0;
                            tutorial.Checker(value);
                        }
                    }
                }
            }
            //  ここまでジャンプ処理


            //  ハンマー収納行動            
            if (nowPlayerState == PlayerState.hammerGet) {      // ハンマーを持っているなら
                if (CrossPlatformInputManager.GetButtonDown("Run") || Input.GetButtonDown("Run")){                                        
                    nowPlayerState = PlayerState.normal;        // ハンマーを持たない状態にする（所持のフラグオフと同義）                    
                    hammer.SetActive(false);                    // しまう（インスタンスではなく、オブジェクトを非アクティブにする）
                    sound12.Play();                             // ハンマーをしまうSEを再生
                    if (isDebugFlg) {
                        return;
                    } else if (Tutorial.tutorialClear == 0) {   // チュートリアルが終わっていないなら
                        if (!Tutorial.isDisarm) {
                            Tutorial.isDisarm = true;
                            int value = 1;
                            tutorial.Checker(value);
                        }
                    }
                }
            }

            //  ハンマー攻撃処理           
            if (nowPlayerState == PlayerState.hammerGet){                                                      // ハンマーを構えている状態なら           
                if (ischargeHummerGet == 1) {                                                                  // chargeHammerを取得しているか
                    if (CrossPlatformInputManager.GetButton("Action") || Input.GetButton("Action")){                        
                        chargeCount += Time.deltaTime;                                                         // 攻撃をためるカウントを開始
                    }
                    if (1.2f > chargeCount && chargeCount > 0.2f) {                                            // 0.2秒以上チャージしたらハンマーが大きくなる。1.2秒まで       
                        particle.Play();                                                                       // 完全にチャージするとparticle再生  
                        iTween.ScaleBy(hammer, iTween.Hash("x", 1.5f, "y", 1.5f, "z", 1.5f, "time", 0.8f));    // アニメ付きでオブジェクトを拡大する
                    }
                    if (CrossPlatformInputManager.GetButtonUp("Action") || Input.GetButtonUp("Action")) {
                        anim.SetTrigger("Action");                // 攻撃アニメの再生
                        sound10.Play();                           // 振るSEを再生
                        if (chargeCount > 1.2f) {                 // チャージした時間で攻撃タグが変化する
                            StartCoroutine(ChargeAttack());       // チャージ攻撃のタグで振り下ろす
                        } else {
                            StartCoroutine(HummerAttack());       // 攻撃タグに切り替え
                        }
                        particle.Stop();                          // particle止める
                        chargeCount = 0;                          // ためるカウンターを0に戻す
                    }
                } else {                                      // chargeHammerを取得していないなら
                    if (CrossPlatformInputManager.GetButtonDown("Action") || Input.GetButtonDown("Action")){
                        anim.SetTrigger("Action");          // 攻撃アニメの再生
                        sound10.Play();                     // 振るSEを再生
                        StartCoroutine(HummerAttack());     // 攻撃タグに切り替え
                    }
                }
                if (isDebugFlg) {
                    return;
                } else if (Tutorial.tutorialClear == 0) {   // チュートリアルが終わっていないなら
                    if (!Tutorial.isAction) {
                        Tutorial.isAction = true;
                        int value = 2;
                        tutorial.Checker(value);
                    }
                }
            }

            //  ハンマー構え処理(chargeHammerに関係しない)  ハンマーを持っていないなら
            if (nowPlayerState == PlayerState.normal){       //（Zキーか右Ctrlキーを押したら）
                if (CrossPlatformInputManager.GetButtonDown("Action") || Input.GetButtonDown("Action")){
                    hammer.SetActive(true);                  // 所持しているhammerオブジェクトをアクティブにする
                    sound11.Play();                          // ハンマーを構えるSEを再生
                    nowPlayerState = PlayerState.hammerGet;  //  ハンマー保持の状態にする（フラグを立てると同義）
                }
                if (isDebugFlg) {
                    return;
                } else if (Tutorial.tutorialClear == 0) {   // チュートリアルが終わっていないなら
                    if (!Tutorial.isSet) {
                        Tutorial.isSet = true;
                        int value = 3;
                        tutorial.Checker(value);
                    }
                }
            }
            //  ここまでハンマー行動処理


            //  ダッシュ処理
            if (nowPlayerState == PlayerState.normal) {         // ハンマーを出していないなら  
                if (CrossPlatformInputManager.GetButtonDown("Run") || Input.GetButton("Run")) {
                    dashCount += Time.deltaTime;                    // 徐々にスピードがあがる
                    if (dashCount > 0.5f) {
                        moveSpeed = 6f;                             // アニメの再生速度も徐々に速くする
                        anim.SetFloat("Speed", 1.0f);
                    }
                    if (dashCount > 1.0f) {
                        moveSpeed = 7f;
                        anim.SetFloat("Speed", 1.5f);
                    }
                }
                if (CrossPlatformInputManager.GetButtonUp("Run") || Input.GetButtonUp("Run")) {  //  ボタンを離したら、スピードを元に戻す
                    moveSpeed = 5.0f;
                    anim.SetFloat("Speed", 0.8f);
                    dashCount = 0f;
                }
                if (isDebugFlg) {
                    return;
                } else if (Tutorial.tutorialClear == 0) {   // チュートリアルが終わっていないなら
                    if (!Tutorial.isDash) {
                        Tutorial.isDash = true;
                        int value = 4;
                        tutorial.Checker(value);
                    }
                }
            }           
        }
    }

    /// <summary>
    /// キャラの実移動
    /// </summary>
    void FixedUpdate(){        
        if (!ZoomOut.cameraSetOn) {                // MainCameraのZoomOutが終了したか確認。それまでは操作無効            
            rb.velocity = new Vector3(0, 0, 0);    // 移動の力とアニメの再生を止める
            anim.SetFloat("Speed", 0.0f);
            return;
        }        
        if (!isEnd){                               // ゲームオーバーでないなら操作可能          
            // カメラの方向からx-z平面の単位ベクトルを取得して移動方向を求める
            Vector3 cameraForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
            // 方向キーの入力値とカメラの向きから移動方向の決定
            Vector3 moveForward = cameraForward * inputVertical + Camera.main.transform.right * inputHorizontal;
            // 移動方向にスピードをかける。ジャンプや落下がある場合は別途Y軸方向の速度ベクトルを足す
            rb.velocity = moveForward * moveSpeed + new Vector3(0, rb.velocity.y, 0);
                                   
            if (moveForward != Vector3.zero){      // 移動しているならキャラクターの向きを進行方向にする
                transform.rotation = Quaternion.LookRotation(moveForward);                                
                anim.SetFloat("Speed", 0.8f);               // 歩くアニメを再生する              
                if (nowPlayerState == PlayerState.normal && isSpeedUpGet == 1) {  // ハンマーを出しておらずSlidingを取得しているなら
                    if (CrossPlatformInputManager.GetButtonDown("Sliding") || Input.GetButtonDown("Sliding")) {  //  Cボタンを押したら                        
                        nowPlayerState = PlayerState.slide;        //  状態をスライドに変更する
                        anim.SetTrigger("Slide");
                        gameObject.layer = LayerMask.NameToLayer("JumpPlayer");
                        StartCoroutine(BackToNormalState());
                    }
                }
            } else {
                anim.SetFloat("Speed", 0.0f);
            }
            if (isDebugFlg) {
                return;
            } else if (Tutorial.tutorialClear == 0) {   // チュートリアルが終わっていないなら
                if (!Tutorial.isMove) {
                    Tutorial.isMove = true;
                    int value = 5;
                    tutorial.Checker(value);
                }
            }
        }
    }
   
    public void OnTriggerEnter(Collider col) {    　　　　　　　　　　　　　 // 当たり判定        
        if (col.gameObject.tag == "Chest"||col.gameObject.tag == "Heart") {  // Heartか宝箱に触ったら２個取れないようにLayerを変えて制御する            
            gameObject.layer = LayerMask.NameToLayer("Untouch");             // レイヤーを変更  触れないようにする。無敵時間にもなる
            StartCoroutine(UntouchItems());
        }
    }

    /// <summary>
    /// プレイヤーの状態をノーマルに戻す
    /// </summary>
    /// <returns></returns>
    private IEnumerator BackToNormalState() {
        yield return new WaitForSeconds(1.2f);
        nowPlayerState = PlayerState.normal;
        gameObject.layer = LayerMask.NameToLayer("Player");
    }

    /// <summary>
    /// ノックバック処理①。プレイヤーを進行方向の後ろ側に飛ばす
    /// </summary>
    public void KnockBackEffect(){
        AudioSource[] audioSources = GetComponents<AudioSource>();
        sound4 = audioSources[3];
        sound5 = audioSources[4];        
        isDamage = true;                                           // ダメージフラグをオンにする        
        float s = 100.0f * Time.deltaTime;                         // ノックバックさせる力
        gameObject.transform.Translate(Vector3.forward * -s);      // ノックバック実行
        anim.SetBool("Damage", true);                              // ダメージを受けたアニメを再生する
        var randomValue = Random.Range(0, 2);　　　　　　　　　　　// ランダムでSE再生
        if (randomValue == 0){
            sound4.Play();
        } else {
            sound5.Play();
        }
        StartCoroutine(InvincibleTime());
    }

    /// <summary>
    /// ノックバック処理②。２秒間の無敵時間を作る
    /// </summary>
    /// <returns></returns>
    private IEnumerator InvincibleTime(){
        gameObject.layer = LayerMask.NameToLayer("PlayerDamage");    // レイヤーをUntouchに変更、タグをNoDamageに変更し、敵との当たり判定を無効にする
        gameObject.tag = "NoDamage";
        yield return new WaitForSeconds(2.0f);
        isDamage = false;                                            // ダメージフラグを元に戻す
        gameObject.layer = LayerMask.NameToLayer("Player");          // レイヤーとタグを元に戻す
        gameObject.tag = "Player";
    }

    /// <summary>
    /// ハートと宝箱の二重判定防止処理。続けて取れなくする
    /// </summary>
    /// <returns></returns>
    private IEnumerator UntouchItems(){        
        yield return new WaitForSeconds(2.0f);               //  ２秒間宝箱に触れないようにする
        gameObject.layer = LayerMask.NameToLayer("Player");  //  レイヤーを元に戻す
    }


    //  通常攻撃時のコルーチン(攻撃モーションは止まらない)
    IEnumerator HummerAttack()
    {     
        //CapsuleCollider cCollider = obj.GetComponent<CapsuleCollider>();

        //  colliderをオンにする
        //cCollider.enabled = true;

        //  ハンマーのタグを攻撃が当たるタグに変更
        yield return new WaitForSeconds(0.4f);
        hammer.gameObject.tag = "Weapon";
        //Debug.Log(hammer.gameObject.tag);

        //Debug.Log(cCollider.enabled);

        //  通常の当たり判定のないタグに変更
        yield return new WaitForSeconds(0.5f);

        //anim.SetFloat("MovingSpeed", 1.0f);

        //  アニメ付きでハンマーを元の大きさに戻す
        iTween.ScaleTo(hammer, iTween.Hash("x", 2.0f, "y", 1.69f, "z", 1.97f, "time", 0.5f));

        //  Colliderをオフにする
        //cCollider.enabled = false;
        hammer.gameObject.tag = "Untagged";        
    }


    //  チャージ後、チャージ攻撃のコルーチン
    IEnumerator ChargeAttack()
    {
        //  ハンマーのタグを攻撃が当たるタグに変更
        yield return new WaitForSeconds(0.4f);

        hammer.gameObject.tag = "Charge";
        //anim.SetFloat("MovingSpeed", 1.0f);

        yield return new WaitForSeconds(0.5f);

        //  アニメ付きでハンマーを元の大きさに戻す
        iTween.ScaleTo(hammer, iTween.Hash("x", 2.0f, "y", 1.69f, "z", 1.97f, "time", 0.5f));

        //  タグを元に戻す
        hammer.gameObject.tag = "Untagged";
    }

    //  SlideGroud通過ジャンプ後にレイヤーを戻すコルーチン
    IEnumerator JumpLayer()
    {
        yield return new WaitForSeconds(0.3f);

        //  レイヤーを元に戻す
        gameObject.layer = LayerMask.NameToLayer("Player");
        //Debug.Log(gameObject.layer);
    }

    //  取得したスキルを確認するメソッド
    //  PowerUpBox内から呼ばれる
    public void SkillStateGet()
    {
        //  ステージセレクトで獲得したスキルの取得状況をPlayerPrefより取得
        isHighJumpGet = PlayerPrefs.GetInt("highJumpKey", 0);
        isSpeedUpGet = PlayerPrefs.GetInt("speedUpKey", 0);
        ischargeHummerGet = PlayerPrefs.GetInt("chargeHammerKey", 0);
    }

    //  ゲームオーバーになったら呼ばれるメソッド
    public void Lose()
    {
        AudioSource[] audioSources = GetComponents<AudioSource>();

        sound8 = audioSources[7];
        sound9 = audioSources[8];        

        // ゲームオーバーのアニメを再生する
        anim.SetBool("Lose", true);

        //　タグとレイヤーを変えるコルーチン
        StartCoroutine("Untouchble");
        
        //  SE再生
        //  ランダムで変える
        var randomValue = Random.Range(0, 2);

        if (randomValue == 0)
        {
            sound8.Play();

        }
        else
        {
            sound9.Play();
        }

        //  ゲーム終了のフラグを立ててキー操作を無効にする。Pause.csでも使用する
        isEnd = true;

        //  GameOver.cs内のメソッドを呼び出す
        GameObject.Find("GameManager").GetComponent<GameOver>().LoseSelecsion();
    }

    //  ゲームクリア処理のメソッド
    public void Win()
    {
        //  ハンマーを出しているなら
        if (nowPlayerState == PlayerState.hammerGet)
        {
            //  所持しているhammerオブジェクトを非アクティブにする　SEは省略
            hammer.SetActive(false);

            //  ハンマーをしまった状態にする
            nowPlayerState = PlayerState.normal;
        }

        AudioSource[] audioSources = GetComponents<AudioSource>();

        sound6 = audioSources[5];
        sound7 = audioSources[6];

        //  ゲームクリアのアニメを再生する
        anim.SetBool("Win", true);
        Debug.Log("Win");

        //  レイヤーを変更  触れないようにする
        gameObject.layer = LayerMask.NameToLayer("Untouch");

        //  SE再生
        //  ランダムで変える
        var randomValue = Random.Range(0, 2);

        if (randomValue == 0)
        {
            sound6.Play();

        }
        else
        {
            sound7.Play();
        }
        //  クリアアニメを終了する　＝  ZoomOut内で行う
        //　ゲーム終了のフラグを立ててキー操作を無効にする
        //isEnd = true;
    }

    //  ゲームオーバー後
    IEnumerator Untouchble()
    {
        //  敵に当たった後には一度Playerに戻ってしまうので、時間差で変更をかける
        yield return new WaitForSeconds(2.5f);

        //  レイヤーとタグを変更  触れないようにする
        gameObject.layer = LayerMask.NameToLayer("Untouch");

        gameObject.tag = "Lose";
    }

    //  落とし穴に落ちたときに呼ばれるメソッド
    public void Restart()
    {
        AudioSource[] audioSources = GetComponents<AudioSource>();

        sound4 = audioSources[3];
        sound5 = audioSources[4];

        //  SE再生
        //  ランダムで変える
        var randomValue = Random.Range(0, 2);

        if (randomValue == 0)
        {
            sound4.Play();
        }
        else
        {
            sound5.Play();
        }

        anim.SetTrigger("Restart");

        StartCoroutine("Reflesh");
    }

    IEnumerator Reflesh()
    {
        yield return new WaitForSeconds(1.0f);

        anim.SetTrigger("Idle");
    }
}
