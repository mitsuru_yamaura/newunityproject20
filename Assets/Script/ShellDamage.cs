﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellDamage : MonoBehaviour {

    public int attackPower;

    Life life;
    UnitychanController uController;

    //  inTrigger用にコライダーを２つつけること
    public void OnTriggerEnter(Collider col)
    {
        //  ダメージ減少用のscriptの参照先
        life = GameObject.Find("HeartScore").GetComponent<Life>();

        //  ノックバック用のscriptの参照先
        uController = GameObject.Find("Kohaku").GetComponent<UnitychanController>();

        //  プレイヤーキャラクターに接触したとき
        if (col.gameObject.tag == "Player")
        {
            //  ノックバック用のメソッドを呼ぶ
            uController.KnockBackEffect();

            //  ダメージを減少させるスクリプトのメソッドを呼ぶ
            life.ApplyDamage(attackPower);

            Destroy(this.gameObject);
        }
    }
}
