﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiverSwitch : MonoBehaviour {

    [Header("元の川と新しい川")]
    [SerializeField] GameObject riverObj1;     //  始めに表示している川のオブジェクト
    [SerializeField] GameObject riverObj2;     //  岩を破壊した後に表示される川のオブジェクト

    private void OnDestroy()
    { 
        if (this.gameObject == null)               //  破壊される可能性のあるオブジェクトはnullの確認が必要
        {
            riverObj1.SetActive(false);            //  始めの川を非表示にして、新しい川を表示する
            riverObj2.SetActive(true);
        }
    }
}
