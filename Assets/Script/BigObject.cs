﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigObject : MonoBehaviour
{
    public float playerJump;                 //  叩いたときのJummpPower
    GameObject kohaku;                       //  プレイヤーキャラクター

    Vector3 temp;                            //  アタッチされているObjectの元Scaleを代入用

    bool isBig;                              //  大きくなっ状態の管理フラグ。大きい間は叩けないようにする

    ParticleSystem particleHammer;            //  ハンマーでたたいた時のエフェクト用とSEをハンマーからもらう
    AudioSource sound1;

    // Use this for initialization
    void Start()
    {
        //  元に戻す際の大きさを格納しておく
        temp = this.transform.localScale;               
    }

    //  当たり判定
    //  ハンマーでたたくとオブジェクトが大きくなる処理
    void OnTriggerEnter(Collider col)
    {
        //  大きくなっていないなら
        if (!isBig)
        {
            //  Weapon/Chargeタグに接触した時（ハンマーの振り下ろす時からWeapon/Chargeタグになる）
            if (col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge")
            {
                kohaku = GameObject.Find("Kohaku");

                //  ハンマーオブジェクトの子であるHammerEffectのパーティクルを取得
                particleHammer = GameObject.Find("HammerEffect").GetComponent<ParticleSystem>();

                //  叩いたSEをハンマーオブジェクトの子であるHammerEffectから取得
                AudioSource[] audioSources = GameObject.Find("HammerEffect").GetComponents<AudioSource>();
                sound1 = audioSources[0];

                //  大きくなっているフラグを立てる
                isBig = true;

                //  叩いているエフェクトを再生
                particleHammer.Play();

                //  叩いているSEを再生
                sound1.Play();

                //  アニメ付きでオブジェクトを揺らす
                iTween.ShakePosition(this.gameObject, iTween.Hash("z", 0.5f, "time", 1.0f));

                //  アニメ付きでオブジェクトを大きくなる
                iTween.ScaleBy(this.gameObject, iTween.Hash("x", 2.0f, "y", 2.0f, "z", 2.0f, "time", 1.0f));

                //  叩いたキャラクターを大きくジャンプさせる
                kohaku.GetComponent<Rigidbody>().AddForce(transform.up * playerJump * Time.deltaTime, ForceMode.Impulse);

                //  待機時間付きでフラグと大きさを戻すためのコルーチン呼び出し
                StartCoroutine(BigTime());
            }
        }
    }

    IEnumerator BigTime()
    {
        yield return new WaitForSeconds(1.0f);

        //  アニメ付きでオブジェクトを揺らす
        iTween.ShakePosition(this.gameObject, iTween.Hash("z", 0.5f, "time", 1.0f));

        //  アニメ付きでオブジェクトを元の大きさに戻す
        iTween.ScaleTo(this.gameObject, iTween.Hash("x", temp.x, "y", temp.y, "z", temp.x, "time", 1.0f));

        //  フラグを降ろす(再び叩ける)
        isBig = false;
    }
}
