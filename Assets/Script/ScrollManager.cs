﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollManager : MonoBehaviour {

    //  構造体の宣言
    //public struct Scroll
    //{
    //public int scrollFlags;
    //public int scrollNum;
    //}

    //public List<int> scrolls = new List<int>(); 
    //public Scroll [] scrolls = new Scroll[6];

    public int[] scrolls;// = new Scroll[6];

    string SCROLL_GET_KEY = "scrollGetKey";
    string SAVE_KEY = "saveKey";

    //  シングルトン化
    private static ScrollManager instance = null;
    public static ScrollManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<ScrollManager>();

                if (instance == null)
                {
                    instance = new GameObject("ScrollManager").AddComponent<ScrollManager>();
                }
            }
            return instance;
        }
    }

    void Awake()
    {
        if (Instance == this)
        {
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        //  シングルトンここまで

        //  保存されている場合のみ取得する
        if (PlayerPrefs.HasKey("saveKey"))
        {
            //  保存されている配列を取得
            scrolls = PlayerPrefsX.GetIntArray("scrollGetKey");
        }
    }


    //  巻き物を取得したら保存する
    public void Save(int itemNum)
    {
        int i = itemNum;
        //scrolls[i].scrollNum = itemNum;

        scrolls[i] = itemNum;
        Debug.Log(scrolls[i]);

        //  配列に入れる
        //scrolls[i].scrollFlags = 1;

        //  保存する
        //PlayerPrefs.SetInt(SCROLL_GET_KEY, scrolls[i].scrollFlags);
        //PlayerPrefs.SetInt(SCROLL_GET_KEY, scrolls[i].scrollNum);
        //Debug.Log(scrolls[i].scrollNum);

        //  配列に保存するのでPlayerPrefsXを利用する
        PlayerPrefsX.SetIntArray(SCROLL_GET_KEY, scrolls);
        PlayerPrefs.SetInt(SAVE_KEY, 1);
        Debug.Log(scrolls[i]);
    }
}
