﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ItemManager : MonoBehaviour {

    //  シングルトン化
    private static ItemManager instance = null;
    public static ItemManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<ItemManager>();

                if (instance == null)
                {
                    instance = new GameObject("ItemManager").AddComponent<ItemManager>();
                }
            }
            return instance;
        }
    }

    void Awake()
    {
        if (Instance == this)
        {
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        //  Chest.csのStart()で宝箱が配置されるので、その前に配置フラグを読み込む
        Load();
    }
    //  ここまで

    //  コンストラクタ
    //  DictionaryにてChestStatusクラスを生成する
    public Dictionary<string, ChestStatus> rune_dict = new Dictionary<string, ChestStatus>();

    //  ListでChestStatusクラスを生成する
    //public List<ChestStatus> m_chestStatusList = new List<ChestStatus>();

    //  保存用のKeyを宣言する    
    const string PLAYER_PREFS_KEY = "ChestStatus";

    //  メンバ変数の宣言
    bool isOpen;
    int nowChestNum;
    int nowStageNum;


    //  Chest.csより呼ばれるメソッド
    //  宝箱の情報がListにない場合、オブジェクトを作成し、追加する
    //public void ChestCreate(int nowChestNum, int nowStageNum)
    //{
    //  Listの宣言
    //List<ChestStatus> m_chestStatusList = new List<ChestStatus>();

    //  直接代入できないので一度
    //  クラスのインスタンス化を行う
    //ChestStatus chestStatus = new ChestStatus(isOpen, nowChestNum, nowStageNum);

    //  1.フラグの情報
    //chestStatus.m_isOpen = false;

    //  2.宝箱のID
    //chestStatus.m_chestId = nowChestNum;

    //  3.ステージNo
    //chestStatus.m_stageId = nowStageNum;

    //m_chestStatusList.Add(chestStatus);
    //}


    //  Chest.csより呼ばれるメソッド
    //  宝箱の情報がDictionayにない場合、オブジェクトを作成し、追加する
    public void ChestCreate(int nowChestNum, int nowStageNum)
    {
        //  まずChestStatus型のオブジェクトを、Chestクラスに宣言・生成する
        ChestStatus chestStatus = new ChestStatus(false, nowChestNum, nowStageNum);

        //  要素を決め、中身を入れる
        //  1.フラグの情報
        chestStatus.m_isOpen = false;

        //  2..stageNoを戻り値でいれる(呼び出し側が引数で渡す)
        chestStatus.m_stageId = nowStageNum;

        //  Dictionayに追加をする(String,ChestStatusの指定をならう)
        rune_dict.Add("chest_" + nowChestNum.ToString().PadLeft(2, '0'), chestStatus);

        //Debug.Log("chest_" + nowChestNum);
        //Debug.Log(chestStatus.m_isOpen);
        //Debug.Log(chestStatus.m_stageId);
    }
    
    //  ルーンとフラグのリセットをするメソッド
    //  [ContextMenu]でインスペクタから操作可能
    [ContextMenu("ResetFlagsRunes")]
    public void ResetFlags()
    {
        //  Dictionayの要素をすべてクリアする
        rune_dict.Clear();      

        //  保存先もリセットする
        PlayerPrefs.DeleteKey("ChestStatus");

        //  Listのリセット用
        //m_chestStatusList.Clear();

        //  ルーンの保存をリセットする
        PlayerPrefs.DeleteKey("runeKey1");

        Debug.Log("Exe ResetFlags!!");
        Debug.Log("Exe ResetRunes!!");
    }

    //  Dictionaryデータの保存メソッド
    //  Chest.cs内で呼ばれる
    public void Save(int nowChestNum, ChestStatus dict_chest)
    {
        //  PlayerPrefsUtilityのSaveDictメソッドを呼び出してデータを保存
        PlayerPrefsUtility.SaveDict<string, ChestStatus>(PLAYER_PREFS_KEY, rune_dict);

        Debug.Log("chest_" + nowChestNum.ToString().PadLeft(2, '0'));
        Debug.Log(dict_chest.m_isOpen);
    }

    //  Dictionaryデータの呼び出しメソッド
    //  Awakeで呼び出し
    public void Load()
    {
        //  PlayerPrefsUtilityのSaveDictメソッドを呼び出して保存データを呼び出し
        //  rune_dictの前に型名をつけてしまうと
        //  publicで宣言したrune_dictとは別のrune_dictを作ってしまうため
        //  ここではthis.rune_dictを使う
        this.rune_dict = PlayerPrefsUtility.LoadDict<string, ChestStatus>(PLAYER_PREFS_KEY);

        //  List用
        //this.m_chestStatusList = PlayerPrefsUtility.LoadList<ChestStatus>(PLAYER_PREFS_KEY);       
    }
}
