﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainSwitch : MonoBehaviour {

    public Terrain terrain;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        //terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
	}

    private void OnTriggerStay(Collider col)
    {
        if(col.gameObject.tag == "Player"|| col.gameObject.tag == "PlayerDamage"|| col.gameObject.tag == "Untouch"){

            terrain.enabled = true;
        }   
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "PlayerDamage" || col.gameObject.tag == "Untouch")
        {

            terrain.enabled = false;
        }
    }
}
