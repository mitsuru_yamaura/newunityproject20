﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NextLoader : MonoBehaviour {

    [SerializeField]
    private GameObject loadTimeUI;           //  シーンロード中に表示するUI画面

    [SerializeField]
    private Slider loadTimeBar;               //  Load時間表示用。SliderのValueをスクリプトで操作するため

    private AsyncOperation async;             //  プレロード後待機用


    // Use this for initialization
    void Start ()
    {
        StartCoroutine("NextLoadStart");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator NextLoadStart()
    {
        //  GameScene1シーンの読み込みをする
        async = SceneManager.LoadSceneAsync("GameScene1");

        //  シーンの遷移は行わないで待つ（この位置にしないとTrueにならない）
        async.allowSceneActivation = false;

        //  ループで読み込みが終わるまで進捗状況をスライダーの値に反映させる
        while (!async.isDone)
        {
            //  async.progressでfloat型の0-1の値を得られる
            //  Mathf.Clamp01を使ってasync.progressが0-1の間になるように補正する
            //  0.9fにしないとisDoneがtrueにならない
            var progressVal = Mathf.Clamp01(async.progress / 0.9f);

            //  Sliderの値はデフォルトでは0-1になっているので、そのまま入れる
            //  SliderのValueをprogressVal分だけ動かす
            loadTimeBar.value = progressVal;

            Debug.Log(loadTimeBar.value);

            yield return null;
            
        }
        //loadTimeBar.value = 1.0f;
    }

    //  ハンマーで叩くと呼ばれるメソッド
    public void LoadStart()
    {
        //  ロード画面UIをアクティブにする
        loadTimeUI.SetActive(true);

        //  待機していたシーンを読み込む
        async.allowSceneActivation = true;
        Debug.Log("NextLaodStart!");
    }
}
