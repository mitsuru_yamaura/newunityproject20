﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class XpCounter : MonoBehaviour {

    public static int xpCount;              //  シーン間で引き継ぐため
    
    Text xpText;                            //  表示用

    string XpKey = "xp";                    //  PlayerPrefsで保存するためのキー
    

    // Use this for initialization
    void Start () {

        //  今までに獲得したxpを取得。保存されていなければ０を取得する）
        xpCount = PlayerPrefs.GetInt(XpKey, 0);

        xpText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {

        //  現在のXpを表示
        xpText.text = xpCount.ToString();

        //　xpCountのリセット
        if (Input.GetKey("left ctrl"))
        {
            PlayerPrefs.DeleteKey("xp");
        }

        //　skillフラグのリセット
        if (Input.GetKey("left alt"))
        {
            PlayerPrefs.DeleteAll();
        }
    }

    //  BreakObjectから呼ばれるメソッド
    //  戻り値分、xpを加算する
    public void AddXp(int exp)
    {       
        xpCount += exp;

        Save();
        Debug.Log(xpCount);
    }

    //  PowerUpBoxから呼ばれるメソッド
    //  戻り値分、xpを減算する
    //  Skillの取得、レベルアップ時に呼ばれる
    public void SubtractXp(int value)
    {
        xpCount -= value;

        Save();
    }

    //  xpの保存用のメソッド
    //  シーン終了後、遷移時に保存される
    public void Save(){
        //  xpCountをxpKeyに保存する
        PlayerPrefs.SetInt(XpKey, xpCount);
        PlayerPrefs.Save();
    }
}
