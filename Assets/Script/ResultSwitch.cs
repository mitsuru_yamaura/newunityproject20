﻿using System.Collections;
using UnityEngine;

public class ResultSwitch : MonoBehaviour {

    [SerializeField] ParticleSystem clearEffect;
    bool isClear;

    private void OnTriggerEnter(Collider col){        
        if (!isClear){                                     // クリアフラグが立っていないなら
            if (col.gameObject.tag == "Player"){
                Vector3 pos = this.transform.position;     // カメラの現在値を引数にしてカメラに渡す
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<ZoomOut>().ClearCameraPoint(pos, "clear");  // カメラをクリア位置へ移動
                isClear = true;
                StartCoroutine(ClearEffect());             // クリアの演出
            }
        }
    }

    /// <summary>
    /// クリア時のエフェクト処理
    /// </summary>
    /// <returns></returns>
    private IEnumerator ClearEffect(){
        yield return new WaitForSeconds(2.0f);
        clearEffect.Play();
        yield return new WaitForSeconds(2.0f);
        clearEffect.Stop();
    }
}
