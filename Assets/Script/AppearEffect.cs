﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppearEffect : MonoBehaviour {

    [SerializeField]
    private GameObject exclamationEffect;    //  出現させるエフェクト

    [SerializeField]
    private float deleteTime;                //  エフェクトを消す秒数

    [SerializeField]
    private float offset;                    //  エフェクトの出現位置のオフセット

	// Use this for initialization
	public void FindOut () {
        //  ゲームオブジェクト生成時にエフェクトをインスタンス化して、消去
        var instantiateEffect = GameObject.Instantiate(exclamationEffect, transform.position + new Vector3(0f, offset, 0f), Quaternion.identity) as GameObject;
        Destroy(instantiateEffect, deleteTime);
    }
}
