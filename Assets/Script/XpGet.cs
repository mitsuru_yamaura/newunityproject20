﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XpGet : MonoBehaviour {

    //  private
    ParticleSystem particle;
    AudioSource sound1;
    AudioSource sound2;

    HeartSpawner hSpawner;
    XpCounter xpCont;
    EggManager eggManager;

    bool isXpGet;                      //  複数取れてしまう場合の回避フラグ

    int xp;                     //  獲得できるXp。HeartSpawnerからもらう。設置時には指定しておく


    // Use this for initialization
    void Start () { 

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //  HeartSpawner内で呼ばれる戻り値受け取りメソッド
    public void GetBonusXp(int bonusXp)
    {
        //  xpを取得する
        xp = bonusXp;
    }

    public void OnTriggerEnter(Collider col)
    {
        AudioSource[] audioSources = GetComponents<AudioSource>();
        sound1 = audioSources[0];
        sound2 = audioSources[1];

        //  スクリプトを格納
        xpCont = GameObject.Find("ExpText").GetComponent<XpCounter>();

        if (col.gameObject.tag == "Player")
        {
            particle = GetComponent<ParticleSystem>();

            //  SE再生
            var randomValue = Random.Range(0, audioSources.Length);

            if (randomValue == 0)
            {
                sound1.Play();
            }
            else
            {
                sound2.Play();
            }

            //  Xpを取った時にフラグを立てる
            if (isXpGet == false)
            {
                //  xpを加算するメソッドの呼び出し
                xpCont.AddXp(xp);
                particle.Play();

                //  巻き物の獲得数を渡すscriptを取得
                eggManager = GameObject.Find("ClearOrb").GetComponent<EggManager>();

                //  巻き物の獲得数を渡す。同時にXpも渡す
                eggManager.Collector(1);
                eggManager.Breaker(xp);

                //  このXpを取ったというフラグ
                isXpGet = true;
            }

            //  このオブジェクトを破壊する
            Destroy(this.gameObject, 2.0f);
        }
    }
}
