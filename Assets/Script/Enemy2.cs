﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : MonoBehaviour {

    Rigidbody rb;
    Animator anim;

    float stateCount;　　　　　 //  状態を変更するまでの待機時間のカウンター
    float scaleX;
    float scaleY;               //  スケールYの元の値
    float scaleZ;

    int facing = 0;             //  向きの確認用
    public float moveSpeed;     //  移動速度
    public float dashPower;     //  ダッシュ力
    public int attackPower;     //  攻撃力
    float nowMoveSpeed;         //  現在の移動速度
    int nowAttackPower;
    
    public enum EnemyState{      //  敵の状態の遷移用
        IDLE,    //  待機
        WALK,
        RUN,
        LOOK,  //  見回す
        ATTACK,
        FACING
    }

    public EnemyState nowEnemyState;   //  現在の敵の状態

    void Start(){
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        // 小さくなった時に元に戻る値を入れておく
        scaleX = this.transform.localScale.x;
        scaleY = this.transform.localScale.y;
        scaleZ = this.transform.localScale.z;      
        // 待機状態。その中で次の行動を呼び出す
        nowEnemyState = EnemyState.IDLE;
    }

    void Update(){
        if (!ZoomOut.cameraSetOn){
            return;
        } else {
            // 移動
            if (nowMoveSpeed > 0) {
                Move();
            }          
            if (nowEnemyState == EnemyState.IDLE){
                // 待機状態なら待機状態を解除するまでのカウントを開始
                stateCount += Time.deltaTime;
                if(stateCount > 2){
                    // 待機状態を解除するカウントになったら次の行動を決定
                    OnNextState();
                }
            }
        }       
    }

    /// <summary>
    /// 移動の実処理。歩くか走る状態になるとnowMoveSpeedに数値が入り移動
    /// </summary>
    private void Move() {
        rb.velocity = new Vector3(nowMoveSpeed, rb.velocity.y, 0);
    }

    /// <summary>
    /// 次の行動をランダムで決定
    /// </summary>
    private void OnNextState(){
        int randomState = Random.Range(0, 5);
        switch (randomState) {
            case 0:
                Attack(EnemyState.ATTACK);      // 攻撃
                break;
            case 1:
                Facing(EnemyState.FACING);      // 向きを変える
                break;
            case 2:
                Run(EnemyState.RUN);         // 走る
                break;
            case 3:
                Walk(EnemyState.WALK);        // 歩く
                break;
            case 4:
                LookAround(EnemyState.LOOK);  // 辺りを見回す
                break;
        }
    }

    /// <summary>
    /// 攻撃処理
    /// </summary>
    private void Attack(EnemyState state){
        nowEnemyState = state;
        // 攻撃力を取得
        nowAttackPower = attackPower * 2;
        anim.SetTrigger("Attack");
        StartCoroutine(GetIdle());
    }

    /// <summary>
    /// 向きを180度転換
    /// </summary>
    /// <param name="state"></param>
    private void Facing(EnemyState state){
        nowEnemyState = state;
        if (facing == 0) { // 現在の向きの状態に合わせて向く方向と移動方向を変更する
            transform.rotation = Quaternion.AngleAxis(90, new Vector3(0,1,0));
            nowMoveSpeed *= -1;
            facing = 1;
        } else{
            transform.rotation = Quaternion.AngleAxis(-90, new Vector3(0, 1, 0));
            nowMoveSpeed *= -1;
            facing = 0;
        }
        StartCoroutine(GetIdle());
    }

    /// <summary>
    /// 走る処理
    /// </summary>
    /// <param name="state"></param>
    private void Run(EnemyState state){
        nowEnemyState = state;
        if (facing == 0){
            nowMoveSpeed = moveSpeed * 1.5f;
        } else {
            nowMoveSpeed = moveSpeed * -1.5f;
        }
        anim.SetFloat("Run",1.0f);
        StartCoroutine(GetIdle());
    }

    /// <summary>
    /// 歩く処理
    /// </summary>
    /// <param name="state"></param>
    void Walk(EnemyState state){
        nowEnemyState = state;
        if (facing == 0){
            nowMoveSpeed = moveSpeed * 1.0f;
        } else {
            nowMoveSpeed = moveSpeed * -1.0f;
        }
        anim.SetFloat("Walk", 0.8f);
        StartCoroutine(GetIdle());
    }

    /// <summary>
    /// その場で辺りを見回す処理
    /// </summary>
    /// <param name="state"></param>
    //  のメソッド
    private void LookAround(EnemyState state) {
        nowEnemyState = state;
        nowAttackPower = 0;
        anim.SetTrigger("SecondIdle");
        StartCoroutine(GetIdle());
    }
    
    public void OnTriggerEnter(Collider col) {  // isTrigger用にコライダーを２つつけること
        if (col.gameObject.tag == "Player"){
            // プレイヤーキャラクターに接触したとき、ノックバック処理とライフ減算処理呼び出し
            UnitychanController uniCon = GameObject.Find("Kohaku").GetComponent<UnitychanController>();
            Life life = GameObject.Find("HeartScore").GetComponent<Life>();
            uniCon.KnockBackEffect();
            life.ApplyDamage(nowAttackPower);
        }
        if (col.gameObject.tag == "Weapon" || col.gameObject.tag == "Charge"){
            // WeaponかChargeタグに接触した時、被ダメージ処理を呼ぶ
            PauseDamage();
        }
    }

    /// <summary>
    /// 被ダメージ時の敵の停止処理と元に戻す処理
    /// hpの減少、破壊、宝箱の処理はBreakObjectで行う
    /// </summary>
    /// <returns></returns>
    IEnumerator PauseDamage(){
        gameObject.layer = LayerMask.NameToLayer("EnemyDamage");
        BreakObject bObj = GetComponent<BreakObject>();
        nowMoveSpeed = 0;
        nowAttackPower = 0;

        yield return new WaitForSeconds(0.6f);
        if (bObj.hp > 0){
            yield return new WaitForSeconds(0.5f);
            iTween.ScaleTo(this.gameObject, iTween.Hash("x", scaleX, "y", scaleY, "z", scaleZ, "time", 1.0f));
            yield return new WaitForSeconds(0.5f);
            gameObject.layer = LayerMask.NameToLayer("Enemy");
        }
    }

    /// <summary>
    /// 待機状態に戻す
    /// </summary>
    /// <returns></returns>
    private IEnumerator GetIdle(){
        yield return new WaitForSeconds(1.0f);
        nowAttackPower = attackPower;
        if (nowMoveSpeed != 0){
            // 走るアニメを終了
            yield return new WaitForSeconds(1.7f);
            anim.SetFloat("Run", 0.0f);
            anim.SetFloat("Walk", 0.0f);
            
            yield return new WaitForSeconds(0.3f);
            nowMoveSpeed = 0;
        }
        nowEnemyState = EnemyState.IDLE;
        // 待機カウントをリセット
        stateCount = 0;
    }
}
