﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject[] gameObjects;   //  プレファブの配列   

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void AppearGameObject()
    {
        //  ランダム用
        //int randomValue = Random.Range(0, gameObjects.Length);

        //  プレファブで指定されたインスタンスを生成
        Instantiate(gameObjects[0],transform.position, transform.rotation);
    }
}
