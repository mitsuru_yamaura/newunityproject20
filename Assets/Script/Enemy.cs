﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    //private
    Rigidbody rb;

    //ParticleSystem particleHammer;            //  ハンマーでたたいた時のエフェクト用
    AudioSource sound1;　　　　　　　　//  叩かれた時のSE（ハンマーから取得）

    BreakObject bj;
    Life lifeScript;                  //  ダメージ参照用の外部スクリプト
    UnitychanController uController;  //  キャラのノックバック用の外部スクリプト

    bool isCameraSetOn;         //  一時停止。カメラの移動にかかわるときにオン／オフのフラグ
    bool isGet;

    float faceCount;            //  向きを変えるまでの待機時間のカウンター
    float jumpCount;            //  ジャンプするまでの待機時間のカウンター     
    float dashCount;            //  ダッシュするまでの待機時間のカウンター
    float extendCount;          //  縦に伸びるまでの待機時間のカウンター
    float scaleY;               //  スケールYの元の値

    int facing = 0;             //  向きの確認用
    int randomFace;             //  向きを変えるまでの待機時間の設定値
    int randomJump;             //  ジャンプするまでの待機時間の設定値
    int randomDash;             //  ダッシュするまでの待機時間の設定値
    int randomExtend;           //  伸びるまでの待機時間の設定値

    public float moveSpeed;    //  左右への移動速度
    public float jumpPower;     //  ジャンプ力
    public float dashPower;     //  ダッシュ力
    public float extendPower;    //  縦に伸びる力 1に対してプラスする

    public int attackPower;     //  攻撃力

    float nowMoveSpeed;         //  現在の移動速度
    float nowJumpPower;
    float nowDashPower;
    float nowExtendPower;
    int nowAttackPower;


    // Use this for initialization
    void Start() {

        //  現在地に初期値の取得。戻す場合に設定値を使うため
        nowMoveSpeed = moveSpeed;
        nowJumpPower = jumpPower;
        nowExtendPower = extendPower;
        nowAttackPower = attackPower;
        nowDashPower = dashPower;

        //  小さくなった時に元に戻る値を入れておく
        scaleY = this.transform.localScale.y;

        rb = GetComponent<Rigidbody>();

        //  ハンマーオブジェクトの子であるHammerEffectのパーティクルを取得
        //particleHammer = GameObject.Find("HammerEffect").GetComponent<ParticleSystem>();

        //  叩かれた時のSEを取得
        //AudioSource[] audioSources = GameObject.Find("HammerEffect").GetComponents<AudioSource>();
        //sound1 = audioSources[0];

        //  スタートがかかるまで、あるいは宝箱の処理が終わるまで待機のフラグ
        //  オンの時は一時停止。カメラの移動にかかわるときにオン／オフ
        isCameraSetOn = ZoomOut.cameraSetOn;

        //  スタート時に各アクションの待機時間をランダムに設定する
        randomFace = Random.Range(4, 7);
        randomDash = Random.Range(4, 7);
        randomJump = Random.Range(4, 7);
        randomExtend = Random.Range(4, 7);
    }

    // Update is called once per frame
    void Update()
    {
        //  動作無効のフラグ
        isCameraSetOn = ZoomOut.cameraSetOn;

        if (isCameraSetOn == false)
        {
            return;
        }

        if (isCameraSetOn == true)
        {
            if (!isGet)
            {
                //  叩かれた時のSEを取得
                //AudioSource audioSource = GameObject.Find("HammerEffect").GetComponent<AudioSource>();
                //sound1 = audioSource;

                isGet = true;
            }

            //  横移動
            rb.velocity = new Vector3(nowMoveSpeed, rb.velocity.y, 0);

            //  待機時間をカウント開始
            faceCount += Time.deltaTime;
            dashCount += Time.deltaTime;
            jumpCount += Time.deltaTime;
            extendCount += Time.deltaTime;

            //  待機時間のカウントがランダムで設定した数値以上になったら方向転換
            if (faceCount > randomFace)
            {
                if (facing == 0)
                {
                    //  向きを90度変える
                    transform.rotation = Quaternion.AngleAxis(90, new Vector3(0, 1, 0));
                    nowMoveSpeed *= -1;
                    facing = 1;
                }
                else
                {
                    //  向きを-90度変える
                    transform.rotation = Quaternion.AngleAxis(-90, new Vector3(0, 1, 0));
                    nowMoveSpeed *= -1;
                    facing = 0;
                }

                //  カウントを戻す
                faceCount = 0f;

                //　次回の待機時間をランダムで設定する
                randomFace = Random.Range(4, 7);
            }

            //  待機時間のカウントがランダムで設定した数値以上になったらダッシュ
            if (dashCount > randomDash)
            {
                //  コルーチン呼び出し
                StartCoroutine("Dash");
            }

            //  待機時間のカウントがランダムで設定した数値以上になったらジャンプ
            if (jumpCount > randomJump)
            {
                //  ジャンプさせる
                rb.velocity = new Vector3(nowMoveSpeed, nowJumpPower, 0);

                //  カウントを戻す
                jumpCount = 0f;

                //　次回の待機時間をランダムで設定する
                randomJump = Random.Range(4, 7);
            }

            //  待機時間のカウントがランダムで設定した数値以上になったら縦に伸びる
            if (extendCount > randomExtend)
            {
                //  アニメ付きで縦に伸びる
                iTween.ScaleBy(this.gameObject, iTween.Hash("y", extendPower + 1.0f, "time", 1.0f));

                //  コルーチン呼び出し
                StartCoroutine(Extend());
            }
        }
    }

    //  inTrigger用にコライダーを２つつけること
    public void OnTriggerEnter(Collider col)
    {
        //  ダメージ減少用のscriptの参照先
        lifeScript = GameObject.Find("HeartScore").GetComponent<Life>();

        //  ノックバック用のscriptの参照先
        uController = GameObject.Find("Kohaku").GetComponent<UnitychanController>();

        //  プレイヤーキャラクターに接触したとき
        if (col.gameObject.tag == "Player")
        {
            //  ノックバック用のメソッドを呼ぶ
            uController.KnockBackEffect();

            //  ダメージを減少させるスクリプトのメソッドを呼ぶ
            lifeScript.ApplyDamage(nowAttackPower);
        }

        //  WeaponかChargeタグに接触した時
        if (col.gameObject.tag == "Weapon"||col.gameObject.tag == "Charge")
        {
            //  ダメージ処理を呼ぶ
            OnDamage();
        }
    }

    //  ダメージ処理のメソッド
    //  hpの減少、破壊、宝箱の処理はBreakObjectで行う
    void OnDamage()
    {
        //  ユーザーがダメージを受けないようにレイヤーを変えて当たりをなくす
        gameObject.layer = LayerMask.NameToLayer("EnemyDamage");

        //  コルーチンで一時的に動きを止める
        StartCoroutine("DamagePause");
    }

    //  敵がハンマーで叩かれたときに呼ばれる
    IEnumerator DamagePause()
    {
        bj = GetComponent<BreakObject>();

        //  動きを止める
        nowMoveSpeed = 0;
        nowJumpPower = 0;
        nowAttackPower = 0;
        nowExtendPower = 0;

        yield return new WaitForSeconds(0.6f);

        //  アニメ付きでオブジェクトを叩いて潰れたように見せる
        iTween.ScaleBy(this.gameObject, iTween.Hash("y", 0.3f, "time", 0.5f));

        //  hpが残っていれば元の状態に戻す
        if (bj.hp > 0)
        {           
            yield return new WaitForSeconds(0.5f);

            iTween.ScaleTo(this.gameObject, iTween.Hash("y", scaleY, "time", 1.0f));

            //  現在値に初期値を戻す
            nowMoveSpeed = moveSpeed;
            nowJumpPower = jumpPower;
            nowAttackPower = attackPower;
            nowExtendPower = extendPower;

            yield return new WaitForSeconds(0.5f);

            //  レイヤーを元に戻す
            gameObject.layer = LayerMask.NameToLayer("Enemy");
        }
    }

    //  縦に伸びたときに呼ばれる
    IEnumerator Extend()
    {
        //  元の大きさに戻す
        yield return new WaitForSeconds(1.0f);

        iTween.ScaleTo(this.gameObject, iTween.Hash("y", scaleY, "time", 1.0f));

        //  カウントを戻す
        extendCount = 0f;

        //　次回の待機時間をランダムで設定する
        randomExtend = Random.Range(4, 7);
    }

    //  ダッシュした時に呼ばれる
    IEnumerator Dash()
    {
        //  ダッシュする時間
        yield return new WaitForSeconds(1.5f);

        //  カウントを戻す
        dashCount = 0f;

        //　次回の待機時間をランダムで設定する
        randomDash = Random.Range(4, 7);
    }
}
