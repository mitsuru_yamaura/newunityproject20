﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinchinPinchOut : MonoBehaviour {

    public Text scaleText;            //  デバッグ用Scaleサイズ確認用テキスト

    private float scale;
    private float backDist = 0.0f;
    private float defaultScale;

	// Use this for initialization
	void Start () {
        RectTransform rt = this.GetComponent(typeof(RectTransform)) as RectTransform;   //  初期値取得
        defaultScale = rt.sizeDelta.x;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.touchCount >= 2)
        {
            RectTransform rt = this.GetComponent(typeof(RectTransform)) as RectTransform;  //  タッチ開始時に初期値を取得
            defaultScale = rt.sizeDelta.x;

            Touch touch1 = Input.GetTouch(0);    //  タッチしている２点を取得
            Touch touch2 = Input.GetTouch(1);

            if(touch2.phase == TouchPhase.Began)  //  ２点タッチ開始時の距離を記憶
            {
                backDist = Vector2.Distance(touch1.position, touch2.position);
            }else if (touch1.phase == TouchPhase.Moved && touch2.phase == TouchPhase.Moved)
            {
                float newDist = Vector2.Distance(touch1.position, touch2.position);    //  タッチ位置の移動後、長さを再測定し、前回の巨利からの相対値を取る
                scale = defaultScale + (newDist - backDist) / 100.0f;
                scaleText.text = "scales = " + scale.ToString();

                if(scale != 0)                   //  相対値を変更した場合、オブジェクトに相対値を反映させる
                {
                    UpdateScaling(scale);
                }
            }
        }
	}

    private void UpdateScaling(float argScale)   //  設定された拡大率に基づいてオブジェクトの大きさを更新する
    {
        RectTransform rt = this.GetComponent(typeof(RectTransform)) as RectTransform;
        rt.sizeDelta = new Vector2(argScale, argScale);
    }
}
