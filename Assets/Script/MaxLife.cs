﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaxLife : MonoBehaviour {

    private int nowInitLife;                                     //  初期のLife数
    private int fullLife = 10;

    private GameObject[] lifeObject;                             //  インスタンスを格納するLifeオブジェクト配列

    private Vector3 lifePosition = new Vector3(30f, 380f, 0);    //　Lifeの初期座標

    public GameObject lifePrefab;                                //  Lifeのプレハブ
   
    string LIFE_KEY = "lifeKey";                                 //  保存用のキー


    //  現在のLife数を代入して、すべてのLifeを無効化し、現在のLife数分を有効化する
    //  プロパティ書式
    public int MaxLifePoint
    {
        set
        {
            //  現在のLife数を代入
            nowInitLife = value;

            //  すべてのLifeを無効化
            for (int i = 0; i < fullLife; i++)
            {
                lifeObject[i].SetActive(false);
                //Debug.Log(i);
            }

            //  現在のLife数分有効化
            for (int i = 0; i < nowInitLife; i++)
            {
                lifeObject[i].SetActive(true);
                //Debug.Log(i);
            }
        }
        get
        {
            return nowInitLife;
        }
    }


    // Use this for initialization
    void Start()
    {
        //  UnityちゃんのLife最大値を取得する
        nowInitLife = UnitychanController.maxLife;
        //Debug.Log(nowInitLife);

        //  ゲームオブジェクトの配列を生成
        //  ここで描画の最大値を設定しておく
        lifeObject = new GameObject[fullLife];

        //  初期値分のLife生成
        //  ここでは最大値まで生成しておく
        for (int i = 0; i < fullLife; i++)
        {
            //  ハートのインスタンス生成
            lifeObject[i] = Instantiate(lifePrefab, lifePosition, Quaternion.identity) as GameObject;

            //  子供にする
            lifeObject[i].transform.SetParent(gameObject.transform, true);

            //  位置座標と画像の大きさを設定
            lifeObject[i].GetComponent<RectTransform>().sizeDelta = new Vector3(1, 1, 1);
        }

        //  現在の最大Lifeを代入してプロパティ実行
        this.MaxLifePoint = nowInitLife;
    }

    public void LevelUpMaxLife()
    {
        //  体力の最大値以上なら終了
        if (UnitychanController.maxLife == fullLife)
        {
            return;
        }

        //  最大値を上げる
        UnitychanController.maxLife++;

        //  SaveData内のレベルセーブメソッドを呼び出す
        //  public宣言してあるscriptなので格納不要。直接の名前で呼べる
        SaveData.Save(LIFE_KEY, UnitychanController.maxLife);

        //  UnityちゃんのLife最大値を取得する
        nowInitLife = UnitychanController.maxLife;
        Debug.Log(nowInitLife);

        //  現在の最大Lifeを代入してプロパティ実行
        this.MaxLifePoint = nowInitLife;
    }
}
