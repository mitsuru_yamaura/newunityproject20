﻿using UnityEngine;

public class Tutorial : MonoBehaviour {

    public static int tutorialClear;               //  チュートリアル終了の確認フラグ 0=未終了、1=終了
    string TUTORIAL_KEY = "tutorialKey";           //  チュートリアル終了を保存
    public int tutorialCount;                      //  チュートリアルが終わる数をカウント
    public int nowCount;                           //  現在のチュートリアル数をカウント

    public static bool isJump;                            //  0.ジャンプボタンでチェック（0-5までUniConより）
    public static bool isDisarm;                          //  1.しまってチェック
    public static bool isAction;                          //  2.アクションボタンでチェック
    public static bool isSet;                             //  3.構えでチェック
    public static bool isDash;                            //  4.ダッシュでチェック
    public static bool isMove;                            //  5.左右移動でチェック
    public static bool isPause;                           //  6.ポーズボタンでチェック（Pauseより）   
    public static bool isMouseZoom;                       //  7.ズームイン／アウト（RotateCameraより）

    [SerializeField] GameObject jumpPanel;                //  各パネル
    [SerializeField] GameObject disarmPanel;
    [SerializeField] GameObject actionPanel;
    [SerializeField] GameObject setPanel;
    [SerializeField] GameObject dashPanel;
    [SerializeField] GameObject movePanel;
    [SerializeField] GameObject pausePanel;
    [SerializeField] GameObject zoomPanel;

    [SerializeField] GameObject elecEffect;                  //  ステージ１のエフェクトを表示
    [SerializeField] GameObject tutorialCanvas;              //  チュートリアル用のキャンバス全体の管理用
    [SerializeField] GameObject startText;                   //  スタート時のテキスト表示の制御用

    StageStartSwitch stageStartSwitch;                    //  キー操作可能のフラグ用

    AudioSource sound1;                                   //  SE1-2
    AudioSource sound2;

    [SerializeField] GameObject tutorialTextBack;
    TextMesh tutorialText;

    void Start (){
        tutorialClear = PlayerPrefs.GetInt("tutorialKey",0);            //  チュートリアルの終了状態を取得
        stageStartSwitch = startText.GetComponent<StageStartSwitch>();  //  ステージ確認用のコンポーネントを取得
        if (tutorialClear == 1){                   //  チュートリアルが終了していて
            if (stageStartSwitch.stageNo == 0){    //  セレクト画面ならチュートリアルを非表示にしてステージ１を開放する
                tutorialCanvas.SetActive(false);
                tutorialTextBack.SetActive(false);
                elecEffect.SetActive(true);
            }
        } else {                                   //  終了していないなら、スタート時のテキストを非表示にする       
            startText.SetActive(false);
            AudioSource[] audioSources = GetComponents<AudioSource>();
            sound1 = audioSources[0];
            sound2 = audioSources[1];
            tutorialTextBack.SetActive(true);                                                            //  チュートリアルテキストの表示
            tutorialText = tutorialTextBack.transform.GetChild(0).gameObject.GetComponent<TextMesh>();
            if (LanguageChoice.languageNum == 0) {                                                       //  言語に合わせてテキスト表示
                tutorialText.text = "【KOHAKUの冒険】へようこそ!!\n\n早速チュートリアルを始めましょう!\n画面下部に操作説明が表示されています\n\nすべての操作を終了すると\nステージ１が開放されます!";
            }
            else{
                tutorialText.text = "Welcome to\nThe Adventure of KOHAKU!!\n\nLet's start with the tutorial!\n\nUpon completion of all operations\nStage 1 will be released!";
            }
        }
	}

    void Update(){
        if (tutorialClear == 1){　　　　　　　　　　　//  チュートリアルが終了しているなら処理しない
            return;
        }
        if (tutorialClear == 0){                      //  チュートリアルをクリアしていないなら
            if (nowCount == tutorialCount){           //  チュートリアルの規定数に達したら
                tutorialClear = 1;                    //  クリアのフラグを立てて保存する
                SaveData.Save(TUTORIAL_KEY, tutorialClear);
                tutorialCanvas.SetActive(false);      //  チュートリアルキャンバスを非表示にする
                tutorialTextBack.SetActive(false);
                elecEffect.SetActive(true);
            }else{                                    //  チュートリアルがすべて終了するまで
                elecEffect.SetActive(false);          //  ステージ１の灯りを消しておく
            }
        }
    }

    /// <summary>
    /// 各チュートリアルフラグのチェック。引数で分岐。
    /// チュートリアルパネルを画面から非表示にし、チュートリアルカウントをプラスする
    /// </summary>
    /// <param name="value"></param>
    public void Checker(int value){
        var randomValue = Random.Range(0, 2);
        if(randomValue == 1){
            sound1.Play();
        }else{
            sound2.Play();
        }
        switch (value){
            case 0:  // jump
                GameObject jumpOk = jumpPanel.transform.GetChild(1).gameObject;
                jumpOk.SetActive(true);
                nowCount++;
                break;
            case 1:  // disarm
                GameObject disarmOk = disarmPanel.transform.GetChild(1).gameObject;
                disarmOk.SetActive(true);
                nowCount++;
                break;
            case 2:  //  action
                GameObject actionOk = actionPanel.transform.GetChild(1).gameObject;
                actionOk.SetActive(true);
                nowCount++;
                break;
            case 3:  //  set
                GameObject setOk = setPanel.transform.GetChild(1).gameObject;
                setOk.SetActive(true);
                nowCount++;
                break;
            case 4:  //  dash
                GameObject dashOk = dashPanel.transform.GetChild(1).gameObject;
                dashOk.SetActive(true);
                nowCount++;
                break;
            case 5:  //  move
                GameObject moveOk = movePanel.transform.GetChild(1).gameObject;
                moveOk.SetActive(true);
                nowCount++;
                break;
            case 6:  //  pause
                GameObject pauseOk = pausePanel.transform.GetChild(1).gameObject;
                pauseOk.SetActive(true);
                nowCount++;
                break;
            case 7:  //  zoom
                GameObject zoomOk = zoomPanel.transform.GetChild(1).gameObject;
                zoomOk.SetActive(true);
                nowCount++;
                break;
        }
    }
}
