﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySwitch : MonoBehaviour {

    public ObjectRotate switchObj;

    // Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        if (switchObj.isSwitch == true)
        {
            Destroy(this.gameObject);
        }
    }
}
