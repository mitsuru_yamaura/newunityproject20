﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneOnlyObject : MonoBehaviour
{
    ScrollManager scrollManager;

    //string SCROLL_GET_KEY = "scrollGetKey";     //  読み込み用のキー  

    public int itemNum;                         //　管理番号。外部で指定する
    int getItemState;                           //  獲得済の番号の状態を取得する

    HeartSpawner hSpawner;

    // Use this for initialization
    void Start()
    {
        scrollManager = GameObject.Find("ScrollManager").GetComponent<ScrollManager>();

        int i = itemNum;
        getItemState = scrollManager.scrolls[i];

        //int i = itemNum;
        //  獲得している状況を取得
        //getItemState = PlayerPrefs.GetInt("scrollGetKey", 0);
        //getItemState = PlayerPrefsX.GetIntArray("scrollGetKey", scrollManager.scrolls[i]);//.scrollNum);
        //Debug.Log(scrollManager.scrolls[itemNum].scrollNum);
        //Debug.Log(getItemState);

        //  すでに取得しているか確認。取得しているなら
        if (getItemState == itemNum)
        {
            //　アイテムを非表示にする
            gameObject.SetActive(false);
            Debug.Log(getItemState);
        }
        else
        {
            //  アイテム生成のメソッドの呼び出し
            hSpawner = GetComponent<HeartSpawner>();
            hSpawner.HeartCreate();
        }
    }

    //  オブジェクトに接触した際に呼ばれる
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            //  保存メソッドを呼び出し
            scrollManager.Save(itemNum);
            Debug.Log(itemNum);
            //SaveItem(itemNum);
        }
    }

    //void SaveItem(int itemNum)
    //{
        //PlayerPrefs.SetInt(SCROLL_GET_KEY, itemNum);
    //}
}