﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageLoading : MonoBehaviour {

    public Image [] images;

    /// <summary>
    /// Loading.csから呼ばれる。ロード画面にランダムな画像を表示する
    /// </summary>
    public void ShowImage(){
        var randomNo = Random.Range(0, images.Length);
        switch (randomNo){
            case 0:
                images[0].enabled = true;
                break;
            case 1:
                images[1].enabled = true;
                break;
            case 2:
                images[2].enabled = true;
                break;
            case 3:
                images[3].enabled = true;
                break;
            case 4:
                images[4].enabled = true;
                break;
            case 5:
                images[5].enabled = true;
                break;
            default:
                break;
        }
    }
}
