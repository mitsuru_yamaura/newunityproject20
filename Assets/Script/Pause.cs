﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;      //  LegacyAssetを使用するための宣言
using UnityStandardAssets.CrossPlatformInput;

public class Pause : MonoBehaviour {

    public bool isPause;                //  ゲームの実行状態の管理フラグ。ZoomOut.cs内より取得

    public GameObject pauseImage;　　　 //  Pause実行時に画面に表示するImageとTextを取得
    public Text skillsStateText;
    public Text forcesStateText;
 
    public SkillsStates skillsStatesScript;   //  習得しているスキルを描画するscript内のメソッド呼出用
    public ForceBonus forcesBonusScript;

    AudioSource sound0;
    AudioSource sound1;
    AudioSource sound2;
    AudioSource sound3;
    AudioSource sound4;

    Blur blurScript;
    UnitychanController uniCon;                    //  操作無効のフラグ取得用
    Tutorial tutorial;

    void Start (){
        //  ハートの描画のため、一瞬だけ描画するコルーチン
        StartCoroutine(Flash());

        //  スクリプトの取得
        blurScript = GameObject.Find("MainCamera").GetComponent<Blur>();
        uniCon = GameObject.Find("Kohaku").GetComponent<UnitychanController>();

        AudioSource[] audioSources = GetComponents<AudioSource>();
        sound0 = audioSources[0];   //  [0],[4] はスタート時の掛け声
        sound1 = audioSources[1];   //  [1]-[3] はPause時の掛け声
        sound2 = audioSources[2];
        sound3 = audioSources[3];
        sound4 = audioSources[4];

        //  SE再生
        //  ランダムで変える
        var randomValue = Random.Range(0, 2);

        if (randomValue == 0)
        {
            sound0.Play();
        }
        else
        {
            sound4.Play();
        }

        if (uniCon.isDebugFlg) {
            return;
        } else if (Tutorial.tutorialClear == 0) {                  // チュートリアルが終わっていないなら
            tutorial = GameObject.Find("TutorialCanvas").GetComponent<Tutorial>();
        }
    }
	
	void Update ()
    {
        //  ゲームの実行状態を取得。スタート時にはTrueになる。
        isPause = ZoomOut.cameraSetOn;

        if (uniCon.isEnd)             //  GameOver状態ならPauseキー操作無効化
        {
            return;
        }

        //  動作無効のフラグ(falseだとゲームが止める)
        if (isPause == false)
        {
            if (CrossPlatformInputManager.GetButtonDown("Pause") || Input.GetKeyDown("p"))
            {
                //  操作とカメラ（上下浮遊）を再度動くようにする
                ZoomOut.cameraSetOn = true;
               
                blurScript.enabled = false;

                //  PauseImage/Textの非表示
                pauseImage.SetActive(false);

                skillsStateText.enabled = false;

                forcesStateText.enabled = false;
            }
            return;
        }

        //  ゲーム実行中ならボタンでポーズをかける
        if (isPause == true) {

            if (CrossPlatformInputManager.GetButtonDown("Pause") || Input.GetKeyDown("p")) {

                //  操作とカメラ（上下浮遊）を止める
                ZoomOut.cameraSetOn = false;

                blurScript.enabled = true;

                //  PauseImage/SkillsText/ForcesTextの表示
                pauseImage.SetActive(true);

                skillsStateText.enabled = true;

                forcesStateText.enabled = true;

                //  習得しているスキルを表示するメソッドを呼び出す
                skillsStatesScript.SkillState();

                //  現在のForceの状態を表示するメソッドを呼び出す
                forcesBonusScript.GetForceBonus(UnitychanController.forceLevel,true);

                //  SE再生
                //  ランダムで変える
                var randomValue = Random.Range(0, 3);

                if (randomValue == 0) {
                    sound1.Play();
                } else if (randomValue == 1) {
                    sound2.Play();
                } else {
                    sound3.Play();
                }
            }
            if (uniCon.isDebugFlg) {
                return;
            } else if (Tutorial.tutorialClear == 0) {   // チュートリアルが終わっていないなら
                if (!Tutorial.isPause) {
                    Tutorial.isPause = true;
                    int value = 6;
                    tutorial.Checker(value);
                }
            }
        }
    }

    //  一瞬だけ描画して非表示にする
    private IEnumerator Flash(){
        yield return new WaitForSeconds(0.2f);
        pauseImage.SetActive(false);
    }
}
