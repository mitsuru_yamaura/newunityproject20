﻿using System.Collections;
using UnityEngine;
using UnityEditor;

public class AssetParameterData{
	public UnityEngine.Object obj{ get; set;}        // !<アセットのObject自体
    
    public string path{ get; set;}                   // !<アセットのパス
    
    public SerializedProperty property { get; set; } // !<プロパティ
}
